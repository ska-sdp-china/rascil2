"""Workflows for handling visibility data"""

__all__ = [
    "create_visibility_from_ms_rsexecute",
    "create_visibility_from_hdf5_rsexecute",
    "concatenate_visibility_frequency_rsexecute",
    "concatenate_visibility_time_rsexecute",
]

import logging

from rascil2.processing_components import concatenate_visibility

from rascil2.workflows.rsexecute.execution_support.rsexecute import rsexecute
from rascil2.processing_components import (
    create_visibility_from_ms,
    create_visibility_from_hdf5,
    concatenate_visibility_frequency,
)

log = logging.getLogger("rascil2-logger")


def create_visibility_from_ms_rsexecute(
    msname, nchan_per_vis, nout, dds, average_channels=False
):
    """Graph for reading from a MeasurementSet into a list of visibility

    :param msname: Name of MeasurementSet
    :param nchan_per_vis: Number of channels to be loaded into each vis
    :param nout: Number of output BVs
    :param dds: DataDescriptors to load e.g. [0, 1,2, 5, 9]
    :param average_channels: Average channels in each Visibility
    :return:
    """

    # Read the MS into RASCIL2 Visibility objects
    log.info("create_visibility_from_ms_rsexecute: Defining graph")
    log.info(
        "create_visibility_from_ms_rsexecute: will load MS data descriptors {dds} into {n} Visibility's of {nchan} channels".format(
            dds=dds, n=nout * len(dds), nchan=nchan_per_vis
        )
    )

    bvis_list = [
        [
            rsexecute.execute(create_visibility_from_ms, nout=nout)(
                msname=msname,
                selected_dds=[dd],
                start_chan=chan_block * nchan_per_vis,
                end_chan=(1 + chan_block) * nchan_per_vis - 1,
                average_channels=average_channels,
            )[0]
            for dd in dds
            for chan_block in range(nout)
        ]
    ]

    # This is a list of lists so we flatten it to a list
    bvis_list = [item for sublist in bvis_list for item in sublist]

    return bvis_list


def create_visibility_from_hdf5_rsexecute(msname, nchan_per_vis, nout, dds):
    """Graph for reading from a HDF5 into a list of visibility

    :param msname: Name of HDF5
    :param nchan_per_vis: Number of channels to be loaded into each vis
    :param nout: Number of output BVs
    :param dds: DataDescriptors to load e.g. [0, 1,2, 5, 9],just index
    :param average_channels: Average channels in each Visibility
    :return:
    """

    bvis_list = [
        [
            rsexecute.execute(create_visibility_from_hdf5)(
                msname,
                chanslice=slice(
                    chan_block * nchan_per_vis, (1 + chan_block) * nchan_per_vis
                ),
                selected_vis=[dd],
            )
            for dd in dds
        ]
        for chan_block in range(nout)
    ]

    bvis_list = [item for sublist in bvis_list for item in sublist]

    return bvis_list


def concatenate_visibility_frequency_rsexecute(bvis_list, split=2):
    """Concatenate a list of visibility's, ordered in frequency

    :param bvis_list: List of Blockvis, ordered in frequency
    :param split: Split into
    :return: BlockVis
    """
    if len(bvis_list) > split:
        centre = len(bvis_list) // split
        result = [
            concatenate_visibility_frequency_rsexecute(bvis_list[:centre]),
            concatenate_visibility_frequency_rsexecute(bvis_list[centre:]),
        ]
        return rsexecute.execute(concatenate_visibility_frequency, nout=2)(result)
    else:
        return rsexecute.execute(concatenate_visibility_frequency, nout=2)(bvis_list)


def concatenate_visibility_time_rsexecute(bvis_list, split=2):
    """Concatenate a list of visibility's, ordered in time

    :param bvis_list: List of Blockvis, ordered in frequency
    :param split: Split into
    :return: BlockVis
    """
    if len(bvis_list) > split:
        centre = len(bvis_list) // split
        result = [
            concatenate_visibility_time_rsexecute(bvis_list[:centre]),
            concatenate_visibility_time_rsexecute(bvis_list[centre:]),
        ]
        return rsexecute.execute(concatenate_visibility, nout=2)(result)
    else:
        return rsexecute.execute(concatenate_visibility, nout=2)(bvis_list)
