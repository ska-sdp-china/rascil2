""" """

__all__ = ["calibrate_list_rsexecute_workflow"]

import logging
import numpy

from rascil2.processing_components.calibration import (
    apply_calibration_chain,
    solve_calibrate_chain,
    create_calibration_controls,
)
from rascil2.processing_components.visibility import (
    integrate_visibility_by_channel,
    divide_visibility,
    concatenate_visibility_frequency,
    expand_polarizations,
)
from rascil2.processing_components.visibility import concatenate_visibility

from rascil2.workflows.rsexecute.execution_support.rsexecute import rsexecute

log = logging.getLogger("rascil2-logger")


def calibrate_list_rsexecute_workflow(
    vis_list,
    model_vislist,
    gt_list=None,
    calibration_context="TG",
    controls=None,
    global_solution=True,
    **kwargs,
):
    """Create a set of components for (optionally global) calibration of a list of visibilities

    If global solution is true then visibilities are gathered to a single visibility data set which is then
    self-calibrated. The resulting gaintable is then effectively scattered out for application to each visibility
    set. If global solution is false then the solutions are performed locally.

    :param vis_list: list of visibilities (or graph)
    :param model_vislist: list of model visibilities (or graph)
    :param calibration_context: String giving terms to be calibrated e.g. 'TGB'
    :param controls: Calibration controls dictionary
    :param global_solution: Solve for global gains
    :param kwargs: Parameters for functions in components
    :return: list of calibrated vis, list of dictionaries of gaintables
    """

    def reorder_freqs(visibility_single_channel, visibility_multiple_channels, index):
        """Copies a given frequency channel from a visibility object into a new visibility object.

        :param visibility_single_channel: single visibility (or graph)
        :param visibility_multiple_channels: visibility object with multiple frequency channels
        :param index: The index of the frequency channel to copy
        :return: visibility_single_channel
        """

        out_vis = visibility_single_channel.copy()
        out_vis.vis.data = visibility_multiple_channels.vis.data[
            :, :, index : index + 1, :
        ]
        return out_vis

    def create_parset_from_context(calibration_context, global_solution):
        """Defines input parset for DP3 based on calibration context.

        :param calibration_context: String giving terms to be calibrated e.g. 'TGB'
        :param global_solution: Solve for global gains
        :return: list of parsets for the different calibrations to run
        """

        import dp3

        parset_list = []
        controls = create_calibration_controls()
        for c in calibration_context:
            parset = dp3.parameterset.ParameterSet()
            parset.add("gaincal.parmdb", "gaincal_solutions.h5")
            parset.add("gaincal.sourcedb", "test.skymodel")
            timeslice = controls[c]["timeslice"]
            if timeslice == "auto" or timeslice is None or timeslice <= 0.0:
                parset.add("gaincal.solint", "1")
            else:
                raise NotImplementedError("The current implementation is wrong")
                nbins = max(
                    1,
                    numpy.ceil(
                        (numpy.max(vis.time.data) - numpy.min(vis.time.data))
                        / timeslice
                    ).astype("int"),
                )
                parset.add("gaincal.solint", str(nbins))
            if global_solution:
                parset.add("gaincal.nchan", "0")
            else:
                parset.add("gaincal.nchan", "1")
            parset.add("gaincal.applysolution", "true")

            if controls[c]["phase_only"]:
                if controls[c]["shape"] == "matrix":
                    parset.add("gaincal.caltype", "diagonalphase")
                else:
                    parset.add("gaincal.caltype", "scalarphase")
            else:
                if controls[c]["shape"] == "matrix":
                    parset.add("gaincal.caltype", "diagonal")
                else:
                    parset.add("gaincal.caltype", "scalar")
            parset_list.append(parset)

        return parset_list

    def dp3_gaincal(vis, calibration_context, global_solution):
        """Calibrates visibilities using the DP3 package.

        :param vis: visibilities (or graph)
        :param calibration_context: String giving terms to be calibrated e.g. 'TGB'
        :param global_solution: Solve for global gains
        :return: calibrated visibilities
        """

        import dp3.steps

        parset_list = create_parset_from_context(calibration_context, global_solution)
        calibrated_vis = vis.copy(deep=True)

        for parset in parset_list:
            gaincal_step = dp3.make_step(
                "gaincal", parset, "gaincal.", dp3.MsType.regular
            )
            queue_step = dp3.steps.QueueOutput(parset, "")
            gaincal_step.set_next_step(queue_step)

            # DP3 GainCal step assumes 4 polarization are present in the visibility data
            nr_correlations = 4
            dpinfo = dp3.DPInfo(nr_correlations)
            dpinfo.set_channels(vis.frequency.data, vis.channel_bandwidth.data)

            antenna1 = vis.antenna1.data
            antenna2 = vis.antenna2.data
            antenna_names = vis.configuration.names.data
            antenna_positions = vis.configuration.xyz.data
            antenna_diameters = vis.configuration.diameter.data
            dpinfo.set_antennas(
                antenna_names, antenna_diameters, antenna_positions, antenna1, antenna2
            )
            first_time = vis.time.data[0]
            last_time = vis.time.data[-1]
            time_interval = vis.integration_time.data[0]
            dpinfo.set_times(first_time, last_time, time_interval)
            dpinfo.phase_center = [vis.phasecentre.ra.rad, vis.phasecentre.dec.rad]
            gaincal_step.set_info(dpinfo)
            queue_step.set_info(dpinfo)
            for time, vis_per_timeslot in calibrated_vis.groupby("time"):
                # Run DP3 GainCal step over each time step
                dpbuffer = dp3.DPBuffer()
                dpbuffer.set_time(time)
                dpbuffer.set_data(
                    expand_polarizations(vis_per_timeslot.vis.data, numpy.complex64)
                )
                dpbuffer.set_uvw(-vis_per_timeslot.uvw.data)
                dpbuffer.set_flags(
                    expand_polarizations(vis_per_timeslot.flags.data, bool)
                )
                dpbuffer.set_weights(
                    expand_polarizations(vis_per_timeslot.weight.data, numpy.float32)
                )
                gaincal_step.process(dpbuffer)

            gaincal_step.finish()

            for time, vis_per_timeslot in calibrated_vis.groupby("time"):
                # Get data out of queue in QueueOutput step

                assert not queue_step.queue.empty()
                dpbuffer_from_queue = queue_step.queue.get()
                visibilities_out = numpy.array(
                    dpbuffer_from_queue.get_data(), copy=False
                )
                nr_polarizations = vis_per_timeslot.vis.data.shape[-1]
                if nr_polarizations == 4:
                    vis_per_timeslot.vis.data[:] = visibilities_out
                elif nr_polarizations == 2:
                    vis_per_timeslot.vis.data[:, :, 0] = (
                        visibilities_out[:, :, 0] + visibilities_out[:, :, 1]
                    ) / 2
                else:
                    vis_per_timeslot.vis.data[:, :, 0] = (
                        visibilities_out[:, :, 0] + visibilities_out[:, :, 3]
                    ) / 2

            assert queue_step.queue.empty()

        return calibrated_vis

    if kwargs.get("calibrate_with_dp3", False):
        log.info(
            "Using DP3 for selfcal. Please note that the gain tables are not available in a RASCIL-known format when using this technique"
        )

        # To use dp3 gaincal, all frequencies must be available in the same vis entry
        vis = rsexecute.execute(concatenate_visibility_frequency)(vis_list)
        calibrated_visibilities_all_frequencies = rsexecute.execute(
            dp3_gaincal, nout=1
        )(vis, calibration_context, global_solution)

        # The frequencies are re distributed over the vis_list for dp3/rascil compatibility
        calibrated_visibilities = [
            rsexecute.execute(reorder_freqs, nout=1)(
                vis_list[i], calibrated_visibilities_all_frequencies, i
            )
            for i, _ in enumerate(vis_list)
        ]

        return calibrated_visibilities, gt_list

    if global_solution:
        log.info("calibration_solve: Performing global solution of gains for all vis")
    else:
        log.info(
            "calibration_solve: Performing separate solution of gains for each vis"
        )

    def calibration_solve(vis, modelvis=None, gt=None):
        return solve_calibrate_chain(
            vis,
            modelvis,
            gt,
            calibration_context=calibration_context,
            controls=controls,
            **kwargs,
        )

    def calibration_apply(vis, gt):
        assert gt is not None
        new_vis = vis.copy(deep=True)
        return apply_calibration_chain(
            new_vis,
            gt,
            calibration_context=calibration_context,
            controls=controls,
            inverse=True,
            **kwargs,
        )

    if kwargs.get("calibrate_with_dp3", False):
        log.info(
            "Using DP3 for selfcal. Please note that the gain tables are not available in a RASCIL-known format when using this technique"
        )

        skymodel = kwargs.get("input_dp3_skymodel", None)
        if skymodel is None:
            skymodel = "test.skymodel"
        return (
            [
                rsexecute.execute(dp3_gaincal, nout=1)(
                    v, calibration_context, global_solution, skymodel
                )
                for v in vis_list
            ],
            None,
        )

    # Here we do a global solution over all vis and channels or
    # just solutions per vis
    if global_solution and (len(vis_list) > 1):
        # The conversion is a no op if it's actually a vis
        point_vislist = [
            rsexecute.execute(divide_visibility, nout=1)(vis_list[i], model_vislist[i])
            for i, _ in enumerate(vis_list)
        ]

        global_point_vis_list = rsexecute.execute(concatenate_visibility, nout=1)(
            point_vislist, dim="frequency"
        )
        global_point_vis_list = rsexecute.execute(
            integrate_visibility_by_channel, nout=1
        )(global_point_vis_list)
        # This is a global solution so we only compute one gain table
        if gt_list is None or len(gt_list) < 1:
            new_gt_list = [
                rsexecute.execute(calibration_solve, pure=True, nout=1)(
                    global_point_vis_list,
                )
            ]
        else:
            new_gt_list = [
                rsexecute.execute(calibration_solve, pure=True, nout=1)(
                    global_point_vis_list,
                    gt=gt_list[0],
                )
            ]

        return [
            rsexecute.execute(calibration_apply, nout=1)(v, new_gt_list[0])
            for v in vis_list
        ], new_gt_list
    else:
        if gt_list is not None and len(gt_list) > 0:
            new_gt_list = [
                rsexecute.execute(calibration_solve, pure=True, nout=1)(
                    v,
                    model_vislist[i],
                    gt_list[i],
                )
                for i, v in enumerate(vis_list)
            ]
        else:
            new_gt_list = [
                rsexecute.execute(calibration_solve, pure=True, nout=1)(
                    v,
                    model_vislist[i],
                )
                for i, v in enumerate(vis_list)
            ]
        return [
            rsexecute.execute(calibration_apply)(v, new_gt_list[i])
            for i, v in enumerate(vis_list)
        ], new_gt_list
