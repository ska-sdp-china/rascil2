"""RASCIL2 MS visualisaation or convert MS to new hdf5 (no buffer)"""

import argparse
import logging
import os
import sys

import matplotlib

matplotlib.use("Agg")

import matplotlib.pyplot as plt

from rascil2.processing_components import (
    create_visibility_from_ms,
    plot_configuration,
    plot_visibility,
    plot_uvcoverage,
)
from rascil2.processing_components.io import convert_visibility_from_ms_to_hdf5

from rascil2.apps.common import display_ms_as_image


log = logging.getLogger("rascil2-logger")
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))


def cli_parser():
    """Get a command line parser and populate it with arguments

    First a CLI argument parser is created. Each function call adds more arguments to the parser.

    :return: CLI parser argparse
    """

    parser = argparse.ArgumentParser(
        description="RASCIL2 ms visualisation and converter"
    )
    parser.add_argument(
        "--mode",
        type=str,
        default="visual",
        help="visual just visualisaation,convert will output to hdf5 file",
    )
    parser.add_argument(
        "--hdf5file", type=str, default="test.hdf5", help="hdf5 file name"
    )
    parser.add_argument(
        "--vis_time_step",
        type=int,
        default=1,
        help="size of buffer (vis_time_step,nbaseline,nchan,npol)",
    )
    parser.add_argument(
        "--weight_time_step",
        type=int,
        default=1,
        help="size of buffer (weight_time_step,nbaseline,nchan,npol)",
    )
    parser.add_argument(
        "--flags_time_step",
        type=int,
        default=1,
        help="size of buffer (flags_time_step,nbaseline,nchan,npol)",
    )
    parser.add_argument(
        "--ack",
        type=str,
        default="False",
        help="Ask casacore to acknowledge each table operation",
    )
    parser.add_argument(
        "--ifauto", type=str, default="False", help="if include autocorrelations"
    )
    parser.add_argument(
        "--datacolumn",
        type=str,
        default="DATA",
        help="MS data column to read DATA, CORRECTED_DATA, or MODEL_DATA, defaults to DATA",
    )
    parser.add_argument(
        "--ingest_msname", type=str, default=None, help="MeasurementSet to be read"
    )
    parser.add_argument(
        "--logfile",
        type=str,
        default=None,
        help="Name of logfile (default is to construct one from msname)",
    )

    return parser


def visualise(args):
    """MS Visualiser

    Performs simple visualisations of the MS

    :param args: argparse with appropriate arguments
    :return: None
    """

    # We need to tell all the Dask workers to use the same log
    cwd = os.getcwd()

    assert args.ingest_msname is not None, "Input msname must be specified"

    if args.logfile is None:
        logfile = args.ingest_msname.replace(".ms", ".log")
    else:
        logfile = args.logfile

    def init_logging():
        logging.basicConfig(
            filename=logfile,
            filemode="a",
            format="%(asctime)s.%(msecs)d %(name)s %(levelname)s %(message)s",
            datefmt="%d/%m/%Y %I:%M:%S %p",
            level=logging.INFO,
        )

    init_logging()

    log.info("\nRASCIL MS Visualiser\n")

    display_ms_as_image(msname=args.ingest_msname)

    bvis_list = create_visibility_from_ms(args.ingest_msname)

    log.info(bvis_list[0])

    plt.clf()
    plot_configuration(bvis_list[0].configuration)
    plt.savefig(args.ingest_msname.replace(".ms", "_configuration.png"))

    plt.clf()
    plot_uvcoverage(bvis_list)
    plt.savefig(args.ingest_msname.replace(".ms", "_uvcoverage.png"))
    plt.clf()

    nchan = bvis_list[0]["vis"].shape[-2]

    plt.clf()
    plot_visibility(
        bvis_list,
        plot_file=args.ingest_msname.replace(".ms", "_visibility_amp.png"),
        chan=nchan // 2,
    )

    plt.clf()
    plot_visibility(
        bvis_list,
        plot_file=args.ingest_msname.replace(".ms", "_visibility_phase.png"),
        chan=nchan // 2,
        y="phase",
    )


def main():
    # Get command line inputs
    parser = cli_parser()
    args = parser.parse_args()
    print(args)
    if args.mode == "convert":
        convert_visibility_from_ms_to_hdf5(
            msname=args.ingest_msname,
            filename=args.hdf5file,
            vis_time_step=args.vis_time_step,
            weight_time_step=args.weight_time_step,
            flags_time_step=args.flags_time_step,
            ack=args.ack == "True",
            datacolumn=args.datacolumn,
            ifauto=args.ifauto == "True",
        )
    else:
        visualise(args)


if __name__ == "__main__":
    main()
