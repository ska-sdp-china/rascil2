"""
Functions that Flagging Visibility and Visibility.

The flags of Visibility has axes [chan, pol, z, y, x] where z, y, x are spatial axes in either sky or Fourier plane. The
order in the WCS is reversed so the grid_WCS describes UU, VV, WW, STOKES, FREQ axes.


"""

__all__ = ["flagging_visibility", "flagging_aoflagger"]

import logging

import numpy
import numba
from astropy.wcs import WCS

from rascil2.data_models.memory_data_models import Visibility, FlagTable

log = logging.getLogger("rascil2-logger")


def flagging_visibility(
    bvis, baselines=None, antennas=None, channels=None, polarisations=None
):
    """Flagging Visibility

    :param bvis: Visibility
    :param baselines: The list of baseline numbers to flag
    :param antennas: The list of antenna number to flag
    :param channels: The list of Channel number to flag
    :param polarisations: The list of polarisations to flag
    :return: Visibility
    """
    if polarisations is None:
        polarisations = []
    if antennas is None:
        antennas = []
    if channels is None:
        channels = []
    if baselines is None:
        baselines = []

    for baseline in baselines:
        bvis["flags"].data[:, baseline, ...] = 1
    for channel in channels:
        bvis["flags"].data[..., channel, :] = 1
    for pol in polarisations:
        bvis["flags"].data[..., pol] = 1
    for ibaseline, (a1, a2) in enumerate(bvis.baselines.data):
        if a1 in antennas or a2 in antennas:
            bvis["flags"].data[:, ibaseline, ...] = 1

    return bvis


def flagging_aoflagger(vis, strategy_name):
    """Flagging Visibility using the AOFlagger package
    The flagging strategy can be telescope name (AARTFAAC, ARECIBO, ARECIBO 305M, BIGHORNS, EVLA, JVLA, LOFAR, MWA, PARKES, PKS, ATPKSMB, or WSRT) or a LUA file like the ones in https://gitlab.com/aroffringa/aoflagger/-/tree/master/data/strategies .
    You can define a new strategy interactively using the AOFlagger rfigui (https://aoflagger.readthedocs.io/en/latest/using_rfigui.html)

    :param vis: Visibility object
    :param strategy_name: Strategy to use: can be a LUA file or a telescope name. If the strategy for the selected telescope is not available, a generic strategy is used.
    :return: Visibility where the flags field has been updated
    """
    try:
        import aoflagger
    except ImportError:
        log.warning("AOFlagger is not installed. Not executing flagging.")
        return vis

    flagger = aoflagger.AOFlagger()
    if strategy_name.endswith(".lua"):
        # Load specific LUA strategy
        strategy = flagger.load_strategy_file(path)
    else:
        # Use the LUA strategy corresponding to the desired telescope
        name_uppercase = strategy_name.upper()
        if name_uppercase == "AARTFAAC":
            path = flagger.find_strategy_file(aoflagger.TelescopeId.AARTFAAC)
        elif name_uppercase == "ARECIBO" or name_uppercase == "ARECIBO 305M":
            path = flagger.find_strategy_file(aoflagger.TelescopeId.Arecibo)
        elif name_uppercase == "BIGHORNS":
            path = flagger.find_strategy_file(aoflagger.TelescopeId.Bighorns)
        elif name_uppercase == "EVLA" or name_uppercase == "JVLA":
            path = flagger.find_strategy_file(aoflagger.TelescopeId.JVLA)
        elif name_uppercase == "LOFAR":
            path = flagger.find_strategy_file(aoflagger.TelescopeId.LOFAR)
        elif name_uppercase == "MWA":
            path = flagger.find_strategy_file(aoflagger.TelescopeId.MWA)
        elif (
            name_uppercase == "PARKES"
            or name_uppercase == "PKS"
            or name_uppercase == "ATPKSMB"
        ):
            path = flagger.find_strategy_file(aoflagger.TelescopeId.Parkes)
        elif name_uppercase == "WSRT":
            path = flagger.find_strategy_file(aoflagger.TelescopeId.WSRT)
        else:
            path = flagger.find_strategy_file(aoflagger.TelescopeId.Generic)

        strategy = flagger.load_strategy_file(path)

    ntimes = vis.vis.data.shape[0]
    nbaselines = vis.vis.data.shape[1]
    nch = vis.vis.data.shape[2]
    npol = vis.vis.data.shape[3]

    for bl in range(nbaselines):
        data = flagger.make_image_set(ntimes, nch, npol * 2)
        for imgindex in range(npol):
            # AOFlagger uses the axis in a different order than Rascil
            values = numpy.swapaxes(vis.vis.data[:, bl, :, imgindex], 0, 1)
            data.set_image_buffer(2 * imgindex, numpy.real(values))
            data.set_image_buffer(2 * imgindex + 1, numpy.imag(values))

        flags = strategy.run(data)

        # A flag denotes that the values at that time-frequency position should be ignored for all polarizations
        for pol in range(npol):
            vis.flags.data[:, bl, :, pol] = numpy.swapaxes(
                flags.get_buffer().astype(numpy.int32), 0, 1
            )

    return vis


def check_params(vis, thresholds, flags, max_sequence_length):
    """Validate input parameters."""
    if not flags.flags.writeable:
        raise ValueError("Flags array must be writable")
    if not (
        vis.flags.c_contiguous
        and thresholds.flags.c_contiguous
        and flags.flags.c_contiguous
    ):
        raise ValueError("All arrays must be C contiguous")
    if vis.ndim != 4 or flags.ndim != 4:
        raise ValueError("Visibility and flags must be 4D arrays")
    if not numpy.iscomplexobj(vis):
        raise ValueError("Visibilities must be complex type")
    if not numpy.issubdtype(flags.dtype, numpy.integer):
        raise ValueError("Flags must be integer type")

    # Validate sequence lengths
    seq_lens = []
    current = 1
    while current <= max_sequence_length:
        seq_lens.append(current)
        current *= 2
    if len(thresholds) != len(seq_lens):
        raise ValueError("Thresholds length doesn't match required sequence lengths")

    # Check dtype consistency
    if vis.dtype == numpy.complex64:
        if thresholds.dtype != numpy.float32:
            raise ValueError("Thresholds must be float32 for complex64 vis")
    elif vis.dtype == numpy.complex128:
        if thresholds.dtype != numpy.float64:
            raise ValueError("Thresholds must be float64 for complex128 vis")
    else:
        raise ValueError("Visibilities must be complex64 or complex128")


@numba.njit
def process_block(thresholds, seq_lens, block, flags_block):
    """Core processing for a single baseline/polarization block."""
    n_times, n_channels = block.shape

    for k in range(len(seq_lens)):
        seq_len = seq_lens[k]
        threshold = thresholds[k] * seq_len

        for ch in range(n_channels):
            for t in range(n_times - seq_len + 1):
                # Update block values with existing flags
                for m in range(seq_len):
                    if flags_block[t + m, ch] == 1:
                        block[t + m, ch] = thresholds[k]

                # Calculate sum
                total = 0.0
                for m in range(seq_len):
                    total += block[t + m, ch]

                # Flag if exceeds threshold
                if total > threshold:
                    for m in range(seq_len):
                        flags_block[t + m, ch] = 1


@numba.njit(parallel=True)
def process_all_blocks(vis_mag, thresholds, seq_lens, flags):
    """Process all baselines and polarizations in parallel."""
    n_times, n_baselines, n_channels, n_pols = vis_mag.shape

    for bl in numba.prange(n_baselines):
        for pol in range(n_pols):
            block = numpy.empty((n_times, n_channels), dtype=vis_mag.dtype)
            flags_block = numpy.zeros((n_times, n_channels), dtype=numpy.int32)

            # Extract magnitude data for current block
            for t in range(n_times):
                for ch in range(n_channels):
                    block[t, ch] = vis_mag[t, bl, ch, pol]

            # Process the block
            process_block(thresholds, seq_lens, block, flags_block)

            # Update flags
            for t in range(n_times):
                for ch in range(n_channels):
                    flags[t, bl, ch, pol] = flags_block[t, ch]


def sum_threshold_rfi_flagger(vis, thresholds, max_sequence_length):
    """
    Python implementation of sum threshold RFI flagging.

    from https://gitlab.com/ska-telescope/sdp/ska-sdp-func/-/raw/1.2.1/
            src/ska-sdp-func/rfi/sdp_rfi_flagger.cpp?ref_type=tags&inline=false

    The code provides a sequence and derives the best experimented thresholds for flagging.
    For a longer sequence, the flagging threshold should be lower.
    Details see line 158, Offringa et al. 2010MNRAS.405..155O

    :param vis: Complex visibility array (time, baseline, channel, pol)
    :param thresholds: 1D array of thresholds for different window sizes
    :param max_sequence_length: Maximum window size to consider
    :return: Flag array with same shape as vis
    """
    # Initialize flags array
    flags = numpy.zeros(vis.shape, dtype=numpy.int32)

    # Generate sequence lengths
    seq_lens = []
    current = 1
    while current <= max_sequence_length:
        seq_lens.append(current)
        current *= 2
    seq_lens = numpy.array(seq_lens, dtype=numpy.int32)

    # Validate parameters
    check_params(vis, thresholds, flags, max_sequence_length)

    # Compute magnitudes
    vis_mag = numpy.abs(vis)

    # Ensure thresholds are correct type
    thresholds = numpy.asarray(thresholds, dtype=vis_mag.dtype)

    # Process data
    process_all_blocks(vis_mag, thresholds, seq_lens, flags)

    return flags
