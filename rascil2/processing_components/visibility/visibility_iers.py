"""
create_visibility_eop
"""

__all__ = [
    "create_visibility_iers",
    "calculate_azel",
    "calculate_hadec",
    "calculate_ENU2UVW_matrix",
    "calculate_ECEF2UVW_matrix",
    "times_to_mjd",
    "calculate_MATRIX_uvw",
    "calculate_uvw",
    "build_basic_object",
    "calculate_ENU_AZEL_uvw",
    "calculate_ECEF_HADEC_uvw",
]


import logging
import datetime
import urllib

import copy

import astropy.time
import numpy
import pandas
import astropy
from astropy.coordinates import SkyCoord
from skyfield.starlib import Star

from rascil2.data_models.memory_data_models import Visibility, Configuration
from rascil2.data_models.parameters import rascil2_data_path
from rascil2.data_models.polarisation import PolarisationFrame
from rascil2.processing_components.util import (
    ecef_to_enu,
    enu_to_ecef,
    azel_to_enu,
    hadec_to_azel,
)
from rascil2.processing_components.visibility.base import generate_baselines


log = logging.getLogger("rascil2-logger")

tau = 6.283185307179586476925287


def _update_finals2000A(path, url, max_days):
    """
    Update the finals2000A.all file if it is older than max_days
    :param path: path to the finals2000A.all file
    :param url: url to the finals2000A.all file
    :param max_days: maximum days to update the file
    """
    with open(path, "r") as f:
        lines = f.readlines()

    last_date = None
    for l in lines:
        flag = l[16]

        year = int(l[0:2])
        if 73 <= year <= 99:
            year += 1900
        else:
            year += 2000
        month = int(l[2:4])
        day = int(l[4:6])
        if flag == "P":
            break
        last_date = datetime.datetime(year, month, day)
    if datetime.datetime.now() - last_date > datetime.timedelta(days=max_days):
        urllib.request.urlretrieve(url, path)


def _rotate_z(theta):
    """
    Return a rotation matrix for a rotation about the z-axis.
    """
    c = numpy.cos(theta)
    s = numpy.sin(theta)
    return numpy.array([[c, -s, 0.0], [s, c, 0.0], [0.0, 0.0, 1.0]])


def _rqu2ruvw(rqu, rnp=numpy.array([0.0, 0.0, 1.0]), offset_sign=1.0):
    """
    Return a rotation matrix for a rotation from rqu to ruvw.

    :param rqu: unit vector in direction of source
    :param rnp: unit vector in direction of north pole
    :param offset_sign: sign of offset
    :return: rotation matrix
    """
    rw = rqu
    ru = numpy.cross(rnp, rw)
    ru = ru / _norm(ru) * offset_sign
    rv = numpy.cross(rw, ru)
    rv = rv / _norm(rv)
    return numpy.array((ru, rv, rw))


def _norm(v):
    """
    Return the norm of a vector.
    """
    return numpy.sqrt(numpy.dot(v, v))


def _crs2rqu(ra, dec):
    """
    Convert right ascension and declination to a unit vector.
    """
    rq = numpy.array(
        [numpy.cos(dec) * numpy.cos(ra), numpy.cos(dec) * numpy.sin(ra), numpy.sin(dec)]
    )
    return rq / _norm(rq)


def _julian_date(utc_datetime):
    """
    Convert a UTC datetime to a Julian date.
    """
    year = utc_datetime.year
    month = utc_datetime.month
    day = (
        utc_datetime.day
        + utc_datetime.hour / 24.0
        + utc_datetime.minute / 1440.0
        + utc_datetime.second / 86400.0
    )

    if month <= 2:
        year -= 1
        month += 12

    A = numpy.floor(year / 100)
    B = 2 - A + numpy.floor(A / 4)

    JD = (
        numpy.floor(365.25 * (year + 4716))
        + numpy.floor(30.6001 * (month + 1))
        + day
        + B
        - 1524.5
    )
    return JD


def _earth_rotation_angle(jd):
    """
    Calculate the Earth rotation angle.

    :param jd: Julian date
    :return: Earth rotation angle in radians
    """
    t = jd - 2451545.0
    frac = jd % 1.0

    era = (numpy.pi * 2 * (0.7790572732640 + 0.00273781191135448 * t + frac)) % (
        numpy.pi * 2
    )
    if era < 0:
        era += numpy.pi * 2
    return era


def _gmst(jd):
    """
    Calculate the Greenwich Mean Sidereal Time.

    :param jd: Julian date
    :return: GMST in radians
    """
    T = (jd - 2451545.0) / 36525.0
    era = _earth_rotation_angle(jd)
    GMST = (
        era
        + (
            0.014506
            + 4612.15739966 * T
            + 1.39667721 * T * T
            + -0.00009344 * T * T * T
            + 0.00001882 * T * T * T * T
        )
        / 60
        / 60
        * numpy.pi
        / 180
    ) % (numpy.pi * 2)
    if GMST < 0:
        GMST += numpy.pi * 2

    return numpy.rad2deg(GMST)


def _lst(gst, longitude_degrees):
    """
    Calculate the Local Sidereal Time.

    :param gst: Greenwich Sidereal Time in degrees
    :param longitude_degrees: Longitude in degrees
    :return: lst degrees
    """
    lst = gst + longitude_degrees
    if lst < 0:
        lst += 360.0
    elif lst >= 360.0:
        lst -= 360.0
    return lst


def _hour_angle(ra, utc_time, longitude):
    """
    Calculate the hour angle.

    :param ra: Right ascension in hours
    :param utc_time: UTC time
    :param longitude: Longitude in degrees
    :return: Hour angle in degrees
    """
    jd = _julian_date(utc_time)
    gst_angle = _gmst(jd)
    lst_angle = _lst(gst_angle, longitude)
    ra_angle = ra * 15.0
    ha = lst_angle - ra_angle
    if ha >= 360.0:
        ha -= 360.0
    return ha


def _skyfield_star_and_offset(star_object, dec_offset):
    from skyfield.units import Angle

    star_offset = copy.deepcopy(star_object)
    star_offset.dec = Angle(degrees=star_object.dec.degrees + dec_offset)
    return star_object, star_offset


def get_star_and_offset(star_object, dec_offset, pos_pkg):
    if isinstance(star_object, Star) and pos_pkg == "skyfield":
        return _skyfield_star_and_offset(star_object, dec_offset)
    elif isinstance(star_object, SkyCoord) and pos_pkg == "astropy":
        raise NotImplementedError("SkyCoord to Star is not implemented")
    elif isinstance(star_object, Star) and pos_pkg == "astropy":
        raise NotImplementedError("Star to SkyCoord is not implemented")
    elif isinstance(star_object, SkyCoord) and pos_pkg == "skyfield":
        raise NotImplementedError("SkyCoord to Star is not implemented")
    else:
        raise ValueError("Star object and pos_pkg do not match")


def build_basic_object(
    pos_pkg,
    latitude_degrees,
    longitude_degrees,
    elevation_m,
    ra_hours,
    dec_degrees,
    dec_offset,
    utc_times,
    source=None,
):
    """
    Build basic objects for different position packages

    :param pos_pkg: Position package to be used, (native,skyfield,ephem,astropy)
    :param latitude_degrees: Latitude in degrees
    :param longitude_degrees: Longitude in degrees
    :param elevation_m: Elevation in meters
    :param ra_hours: Right ascension in hours
    :param dec_degrees: Declination in degrees
    :param dec_offset: Declination offset in degrees
    :param utc_times: UTC times
    :return: obser, star, star_offset, obs_time_list
    """
    if pos_pkg in ["skyfield", "astropy"]:
        _update_finals2000A(
            rascil2_data_path("ephem/finals2000A.all"),
            "https://datacenter.iers.org/products/eop/rapid/standard/finals2000A.all",
            30,
        )

    if pos_pkg == "skyfield":
        from skyfield import api
        from skyfield.api import wgs84
        from skyfield.data import iers

        # Get ephemris data from rascil2 data
        __skyfield_data__ = rascil2_data_path("ephem")
        load = api.Loader(__skyfield_data__, verbose=False)

        timescale = load.timescale(builtin=False)

        # Load IERS data
        url = load.build_url("finals2000A.all")
        with load.open(url) as f:
            finals_data = iers.parse_x_y_dut1_from_finals_all(f)
        iers.install_polar_motion_table(timescale, finals_data)

        # construct the obsterver
        obser = load("de440s.bsp")["earth"] + wgs84.latlon(
            latitude_degrees, longitude_degrees, elevation_m
        )

        # construct star and start_offset object, the start epoch is J2000
        if source is None:
            star = Star(
                ra_hours=ra_hours, dec_degrees=dec_degrees, epoch=timescale.J2000
            )

            star_offset = Star(
                ra_hours=ra_hours,
                dec_degrees=dec_degrees + dec_offset,
                epoch=timescale.J2000,
            )
        else:
            star, star_offset = get_star_and_offset(source, dec_offset, "skyfield")

        # NOTE: we do not use NCP in skyfield for MATRIX model
        # NCP = Star(
        #     ra_hours=ra_hours,
        #     dec_degrees=(90, 0,0),epoch=timescale.J2000
        # )
        obs_time_list = []
        for date in utc_times:
            obs_time_list.append(timescale.utc(date))
        obs_time_list = numpy.array(obs_time_list)

    elif pos_pkg == "ephem":
        import ephem

        obser = ephem.Observer()
        obser.lat = numpy.deg2rad(latitude_degrees)
        obser.long = numpy.deg2rad(longitude_degrees)
        obser.elevation = elevation_m
        obser.epoch = ephem.J2000
        obser.pressure = 0.0
        obser.temp = 0.0

        star = ephem.FixedBody()
        star._ra = numpy.deg2rad(ephem.hours(ra_hours * 15.0))
        star._dec = numpy.deg2rad(ephem.degrees(dec_degrees))
        star._epoch = ephem.J2000

        star_offset = ephem.FixedBody()
        star_offset._ra = numpy.deg2rad(ephem.hours(ra_hours * 15.0))
        star_offset._dec = numpy.deg2rad(ephem.degrees(dec_degrees + dec_offset))
        star_offset._epoch = ephem.J2000

        obs_time_list = []
        for date in utc_times:
            obs_time_list.append(ephem.date(date))
        obs_time_list = numpy.array(obs_time_list)
    elif pos_pkg == "astropy":
        from astropy.coordinates import EarthLocation, solar_system_ephemeris
        from astropy.time import Time
        from astropy.utils import iers

        # TODO: this set is Global, need to find a better way to set this
        iers.earth_orientation_table.set(
            iers.IERS_A.open(rascil2_data_path("ephem/finals2000A.all"))
        )
        solar_system_ephemeris.set(rascil2_data_path("ephem/de440s.bsp"))

        obser = EarthLocation.from_geodetic(
            longitude_degrees, latitude_degrees, elevation_m
        )
        star = SkyCoord(ra_hours, dec_degrees, unit=("hour", "deg"), frame="icrs")
        star_offset = SkyCoord(
            ra_hours, dec_degrees + dec_offset, unit=("hour", "deg"), frame="icrs"
        )
        obs_time_list = []
        for date in utc_times:
            obs_time_list.append(Time(date))
        obs_time_list = numpy.array(obs_time_list)

    elif pos_pkg == "native":
        obser = (latitude_degrees, longitude_degrees, elevation_m)
        star = (ra_hours, dec_degrees)
        star_offset = (ra_hours, dec_degrees + dec_offset)
        obs_time_list = utc_times
    else:
        raise ValueError("pos_pkg must be one of 'skyfield' or 'ephem'")

    return obser, star, star_offset, obs_time_list


def calculate_azel(
    obser, star, obs_time, pos_pkg="skyfield", temperature=None, pressure=None
):
    """
    Calculate the azimuth and elevation of a star.

    :param obser: Observer object
    :param star: Star object
    :param obs_time: Observation time
    :param temperature: Temperature
    :param pressure: Pressure
    :return: Azimuth and elevation in radians
    """
    if pos_pkg == "skyfield":
        astrometric = obser.at(obs_time).observe(star)
        apparent = astrometric.apparent()
        alt, az, _ = apparent.altaz(temperature_C=temperature, pressure_mbar=pressure)
        az = numpy.deg2rad(az.degrees)
        el = numpy.deg2rad(alt.degrees)
    elif pos_pkg == "ephem":
        obser.date = obs_time
        if temperature is not None:
            obser.temp = temperature
        else:
            obser.temp = 0.0
        if pressure is not None:
            obser.pressure = pressure
        else:
            obser.pressure = 0.0
        star.compute(obser)
        az = star.az
        el = star.alt
    elif pos_pkg == "astropy":
        from astropy.coordinates import AltAz
        from astropy import units as u

        altaz = star.transform_to(
            AltAz(
                obstime=obs_time,
                location=obser,
                temperature=temperature * u.deg_C if temperature is not None else None,
                pressure=pressure * u.bar if pressure is not None else 0,
            )
        )
        az = altaz.az.radian
        el = altaz.alt.radian
    elif pos_pkg == "native":
        ha_rad, dec_rad = calculate_hadec(obser, star, obs_time, pos_pkg)
        latitude = obser[0]  # in degrees
        az, el = hadec_to_azel(ha_rad, dec_rad, numpy.deg2rad(latitude))
    else:
        raise ValueError("pos_pkg must be one of 'skyfield', 'ephem','native'")
    return az, el


def calculate_hadec(obser, star, obs_time, pos_pkg="skyfield"):
    """
    Calculate the hour angle and declination of a star.

    :param obser: Observer object
    :param star: Star object
    :param obs_time: Observation time
    :return: Hour angle and declination in degrees
    """
    if pos_pkg == "skyfield":
        astrometric = obser.at(obs_time).observe(star)
        apparent = astrometric.apparent()
        ha, dec, _ = apparent.hadec()
        ha = numpy.deg2rad(ha._degrees)
        dec = numpy.deg2rad(dec.degrees)
    elif pos_pkg == "ephem":
        obser.date = obs_time
        obser.pressure = 0.0
        star.compute(obser)
        ha = star.ha
        dec = star.dec
    elif pos_pkg == "astropy":
        from astropy.coordinates import HADec

        hadec = star.transform_to(HADec(obstime=obs_time, location=obser))
        ha = hadec.ha.radian
        dec = hadec.dec.radian
    elif pos_pkg == "native":
        # in native package, we use utc jd to calculate the ha and dec
        # and the GMST is calculated
        star_ra_hours = star[0]  # in hours
        longitude_degrees = obser[1]
        ha = numpy.deg2rad(_hour_angle(star_ra_hours, obs_time, longitude_degrees))
        dec = numpy.deg2rad(star[1])
    else:
        raise ValueError("pos_pkg must be one of 'skyfield', 'ephem','native'")
    return ha, dec


def calculate_ENU2UVW_matrix(az, el, az_offset, el_offset, offset_sign):
    """
    Calculate the rotation matrix from ENU to UVW.

    :param az: Azimuth in radians
    :param el: Elevation in radians
    :param az_offset: Azimuth offset in radians
    :param el_offset: Elevation offset in radians
    :return: Rotation matrix
    """
    w = azel_to_enu(az, el)
    w_offset = azel_to_enu(az_offset, el_offset)
    u = numpy.cross(w_offset, w)
    u /= _norm(u) * offset_sign
    v = numpy.cross(w, u, axis=0)
    return numpy.vstack([u, v, w])


def calculate_ECEF2UVW_matrix(
    longitude_rad_minus_ha, dec, longitude_rad_minus_ha_offset, dec_offset, offset_sign
):
    """
    Calculate the rotation matrix from ECEF to UVW.

    :param longitude_degrees_minus_ha: Longitude minus hour angle in degrees
    :param dec: Declination in degrees
    :param longitude_degrees_minus_ha_offset: Longitude minus hour angle offset in degrees
    :param dec_offset: Declination offset in degrees
    :return: Rotation matrix
    """
    Y0 = _crs2rqu(longitude_rad_minus_ha, dec)

    Y0_offset = _crs2rqu(longitude_rad_minus_ha_offset, dec_offset)

    X0 = numpy.cross(Y0_offset, Y0)
    X0 /= _norm(X0) * offset_sign

    Z0 = numpy.cross(Y0, X0)

    return numpy.vstack([X0, Z0, Y0])


def times_to_mjd(obs_time_list, pos_pkg="skyfield"):
    """
    Convert a list of UTC times to Modified Julian Dates.

    :param obs_time_list: List of UTC times
    :param pos_pkg: Position package to be used, (skyfield,ephem,astropy,native)
    """
    if pos_pkg == "skyfield":
        from skyfield.timelib import julian_date

        mjd = numpy.array(
            [
                julian_date(
                    obs_time.utc.year,
                    obs_time.utc.month,
                    obs_time.utc.day,
                    obs_time.utc.hour,
                    obs_time.utc.minute,
                    obs_time.utc.second,
                )
                - 2400000.5
                for obs_time in obs_time_list
            ]
        )
    elif pos_pkg == "ephem":
        import ephem

        mjd = numpy.array(
            [ephem.julian_date(obs_time) - 2400000.5 for obs_time in obs_time_list]
        )
    elif pos_pkg == "astropy" or pos_pkg == "native":
        mjd = numpy.array(
            [astropy.time.Time(obs_time).mjd for obs_time in obs_time_list]
        )
    else:
        raise ValueError("pos_pkg must be one of 'skyfield' or 'ephem'")
    return mjd * 86400  # convert to seconds


def calculate_ENU_AZEL_uvw(
    obser,
    star,
    obs_time,
    pos_pkg,
    star_offset,
    ants_xyz,
    config,
    temperature,
    pressure,
    offset_sign,
):
    """
    Calculate the UVW coordinates of antennas in the ENU frame.

    :param obser: Observer object
    :param star: Star object
    :param obs_time: Observation time
    :param pos_pkg: Position package to be used, (skyfield,ephem,astropy,native)
    :param star_offset: Star offset object
    :param ants_xyz: Antenna positions
    :param config: Configuration of antennas
    :param temperature: Temperature
    :param pressure: Pressure
    :return: UVW coordinates of antennas
    """
    az, el = calculate_azel(obser, star, obs_time, pos_pkg, temperature, pressure)
    az_offset, el_offset = calculate_azel(
        obser, star_offset, obs_time, pos_pkg, temperature, pressure
    )
    ENU2UVW_matrix = calculate_ENU2UVW_matrix(az, el, az_offset, el_offset, offset_sign)

    if config.frame == "ECEF":
        ants_enu = ecef_to_enu(config.location, ants_xyz)
    else:
        ants_enu = ants_xyz
    ants_uvw = numpy.einsum("ij,kj->ki", ENU2UVW_matrix, ants_enu)
    return ants_uvw


def calculate_ECEF_HADEC_uvw(
    obser, star, obs_time, pos_pkg, star_offset, ants_xyz, config, offset_sign
):
    """
    Calculate the UVW coordinates of antennas in the ECEF frame.

    :param obser: Observer object
    :param star: Star object
    :param obs_time: Observation time
    :param pos_pkg: Position package to be used, (skyfield,ephem,astropy,native)
    :param star_offset: Star offset object
    :param ants_xyz: Antenna positions
    :param config: Configuration of antennas
    :return: UVW coordinates of antennas
    """
    ha, dec = calculate_hadec(obser, star, obs_time, pos_pkg)
    ha_offset, dec_offset = calculate_hadec(obser, star_offset, obs_time, pos_pkg)
    ECEF2UVW_matrix = calculate_ECEF2UVW_matrix(
        config.location.geodetic[0].to("rad").value - ha,
        dec,
        config.location.geodetic[0].to("rad").value - ha_offset,
        dec_offset,
        offset_sign,
    )

    if config.frame == "ECEF":
        ants_ecef = ants_xyz
    else:
        ants_ecef = enu_to_ecef(config.location, ants_xyz)

    ants_uvw = numpy.einsum("ij,kj->ki", ECEF2UVW_matrix, ants_ecef)
    return ants_uvw


def calculate_MATRIX_uvw(
    obser, star, obs_time, app_pos, star_offset, ants_xyz, config, offset_sign
):
    """
    Calculate the UVW coordinates of antennas using a rotation matrix.

    :param obser: Observer object
    :param star: Star object
    :param obs_time: Observation time
    :param app_pos: Whether to use apparent position
    :param star_offset: star_offset is better than NCP
    :param ants_xyz: Antenna positions
    :param config: Configuration of antennas
    """
    from skyfield.framelib import ICRS_to_J2000 as B

    # NOTE: step-1: calculate the ICRS to ITRF matrix,
    # this is different from omniuv,
    # we use the standard ICRS to ITRF matrix rather than ITFR to ICRS matrix
    P = obs_time.precession_matrix()
    N = obs_time.nutation_matrix()
    W = obs_time.polar_motion_matrix()
    R = _rotate_z(-obs_time.gast * tau / 24.0)

    # same as :
    # from skyfield.framelib import itrs
    # ICRS_to_ITRF_matrix = itrs.rotation_at(obs_time)
    ICRS_to_ITRF_matrix = W @ R @ N @ P @ B

    if app_pos:
        # NOTE: step-2: calculate the ICRS to UVW matrix
        # this is different from omniuv,
        # In skyfield, we use the apparent position to calculate the ICRS to UVW matrix
        # this apparent position is corrected the station's position in ICRS barycentric frame
        # and aberation and light deflection are considered
        apparent = obser.at(obs_time).observe(star).apparent()
        # this is standard ICRF same as apparent.radec() without parameter
        rqu = apparent.position.au / _norm(apparent.position.au)
        # same as :
        # radec = apparent.radec()
        # When called without a parameter,
        # this returns standard ICRF right ascension and declination
        # rqu = _crs2rqu(radec[0].radians, radec[1].radians)
        NCP_apparent = obser.at(obs_time).observe(star_offset).apparent()
        rnp = NCP_apparent.position.au / _norm(NCP_apparent.position.au)

    else:
        # NOTE: step-2: calculate the ICRS to UVW matrix
        # In omniuv, the rqu use J2000 radec and the rnp is [0,0,1]
        # this is different from the skyfield apparent.position
        rqu = _crs2rqu(star.ra.radians, star.dec.radians)
        rnp = numpy.array([0.0, 0.0, 1.0])
        offset_sign = 1.0

    # NOTE: step-3: calculate the ICRS to UVW rotation matrix using rqu and rnp
    ICRS_to_UVW_rotation = _rqu2ruvw(rqu, rnp, offset_sign)

    # NOTE: step-4: we use the ICRS to ITRF matrix to calculate the ITRF to UVW matrix
    # by multiply the ICRS to ITRF matrix to the base vector of U,V,W splitly
    ITRF_to_UVW_rotation = numpy.einsum(
        "ij...,jk...->ki...", ICRS_to_ITRF_matrix, ICRS_to_UVW_rotation.T
    )

    if config.frame == "ECEF":
        ants_ecef = ants_xyz
    else:
        ants_ecef = enu_to_ecef(config.location, ants_xyz)

    # NOTE: step-5: multiply the ITRF to UVW matrix to the antenna ITRF position
    ant_uvw = numpy.einsum("ij,kj->ki", ITRF_to_UVW_rotation, ants_ecef)
    return ant_uvw


def calculate_uvw(
    obser,
    star,
    obs_time,
    star_offset,
    ants_xyz,
    config,
    uvw_model,
    pos_pkg,
    temperature,
    pressure,
    offset_sign,
):
    """
    Calculate the UVW coordinates of antennas.

    :param obser: Observer object
    :param star: Star object
    :param obs_time: Observation time
    :param star_offset: Star offset object
    :param ants_xyz: Antenna positions
    :param config: Configuration of antennas
    :param uvw_model: UVW rotation model to be used, (ENU+AZEL,ECEF+HADEC,MATRIX,MATRIX-APPPOS)
    :param pos_pkg: Position package to be used, (skyfield,ephem,astropy,native)
    :param temperature: Temperature
    :param pressure: Pressure
    :return: UVW coordinates of antennas
    """
    if uvw_model == "ENU+AZEL":
        return calculate_ENU_AZEL_uvw(
            obser,
            star,
            obs_time,
            pos_pkg,
            star_offset,
            ants_xyz,
            config,
            temperature,
            pressure,
            offset_sign,
        )
    elif uvw_model == "ECEF+HADEC":
        return calculate_ECEF_HADEC_uvw(
            obser, star, obs_time, pos_pkg, star_offset, ants_xyz, config, offset_sign
        )
    elif uvw_model in ["MATRIX", "MATRIX-APPPOS"]:
        assert pos_pkg == "skyfield"
        app_pos = uvw_model == "MATRIX-APPPOS"
        return calculate_MATRIX_uvw(
            obser, star, obs_time, app_pos, star_offset, ants_xyz, config, offset_sign
        )
    else:
        raise ValueError("uvw_model must be one of 'ENU+AZEL','ECEF+HADEC','MATRIX'")


def create_visibility_iers(
    config: Configuration,
    times: numpy.array,
    frequency: numpy.array,
    phasecentre: SkyCoord,
    weight: float = 1.0,
    polarisation_frame: PolarisationFrame = None,
    integration_time=1.0,
    channel_bandwidth=1e6,
    source="unknown",
    meta=None,
    uvw_model="ENU+AZEL",
    pos_pkg="skyfield",
    temperature=None,
    pressure=None,
    offset_value=0.03,
) -> Visibility:
    """
    Create a Visibility from Configuration, utc times, and direction of source

    :param config: Configuration of antennas
    :param times: time numpy array containing datetime.datetime objects
    :param frequency: frequencies (Hz] [nchan]
    :param phasecentre: phasecentre of observation (SkyCoord)
    :param weight: weight of a single sample
    :param polarisation_frame: PolarisationFrame('stokesI')
    :param integration_time: Integration time ('auto' or value in s)
    :param channel_bandwidth: channel bandwidths: (Hz] [nchan]
    :param zerow: bool - set w to zero
    :param elevation_limit: in degrees
    :param source: Source name
    :param meta: Meta data as a dictionary
    :param uvw_model: UVW rotation model to be used, (ENU+AZEL,ECEF+HADEC,MATRIX,MATRIX-APPPOS)
    :param pos_pkg: Position package to be used, (native,skyfield,ephem,astropy),
                    ephem and native only supports ENU+AZEL and ECEF+HADEC
    :param temperature: Temperature, the value will drectly passed to the position package
        in pos_pkg = "skyfield" if temperature="standard" Skyfield uses 10 degree Celsius,
        in pos_pkg = "ephem" the value unit is degree Celsius
        in pos_pkg = "astropy" the value unit is degree Celsius
        only used in ENU+AZEL model and pos_pkg can not be "native"
    :param pressure: Pressure, the value will drectly passed to the position package,
        in pos_pkg = "skyfield" if pressure="standard" Skyfield uses the observer's elevation
                                above sea level to estimate the atmospheric pressure,
                                otherwise the value will be used for pressure_mbar
        in pos_pkg = "ephem" the value unit is mbar
        in pos_pkg = "astropy" the value unit is bar
        only used in ENU+AZEL model and pos_pkg can not be "native"
    :param offset_value: DEC Offset value in degrees, The apparent RA is not a straight line,
        so if the number is too large, the u,v will be inaccurate, but w is always accurate,
        but if the number is too small, the u,v,w will large difference with different uvw_model
        the default value is 0.03 degrees according to the katpoint, there may not be a best value
    :return: Visibility
    """

    if not isinstance(source, str):
        if pos_pkg not in ["skyfield"]:
            raise ValueError("Only skyfield support source object")

    if pos_pkg == "native":
        if temperature is not None:
            raise ValueError("Temperature is not supported in this package")
        if pressure is not None:
            raise ValueError("Pressure is not supported in this package")

    if pos_pkg in ["ephem", "native", "astropy"]:
        if uvw_model not in ["ENU+AZEL", "ECEF+HADEC"]:
            raise ValueError(
                "Ephem package or native only supports ENU+AZEL and ECEF+HADEC"
            )

    if uvw_model != "ENU+AZEL":
        if temperature is not None:
            raise ValueError(
                "Temperature is not support for ECEF+HADEC and MATRIX model"
            )
        if pressure is not None:
            raise ValueError("Pressure is not support for ECEF+HADEC and MATRIX model")

    ants_xyz = config["xyz"].data
    nants = len(config["names"].data)
    ntimes = len(times)

    baselines = pandas.MultiIndex.from_tuples(
        generate_baselines(nants), names=("antenna1", "antenna2")
    )
    nbaselines = len(baselines)

    diameter = config["diameter"].data
    max_diameter_square = numpy.max(diameter) ** 2

    npol = polarisation_frame.npol
    nchan = len(frequency)
    visshape = [ntimes, nbaselines, nchan, npol]

    ruvw = numpy.zeros([ntimes, nbaselines, 3])

    rvis = numpy.zeros(visshape, dtype="complex")
    rflags = numpy.ones(visshape, dtype="int")
    rweight = numpy.ones(visshape)
    rimaging_weight = numpy.ones(visshape)
    rintegrationtime = numpy.zeros([ntimes])

    # ra_hours = phasecentre.ra.hour
    # dec_degrees = phasecentre.dec.deg

    if isinstance(source, Star):
        ra_hours = source.ra.hours
        dec_degrees = source.dec.degrees
        phasecentre = SkyCoord(
            ra_hours, dec_degrees, unit=("hour", "deg"), frame="icrs"
        )
    elif isinstance(source, str):
        ra_hours = phasecentre.ra.hour
        dec_degrees = phasecentre.dec.deg
    else:
        raise ValueError("source must be a skyfield Star object or string")

    # offset in degrees, TODO: need to find a better way to set this
    dec_offset = offset_value if dec_degrees < 0.0 else -offset_value
    offset_sign = 1.0 if dec_degrees < 0.0 else -1.0
    # if uvw_model in ["MATRIX","MATRIX-APPPOS"]:
    #     dec_offset = 90.0 - phasecentre.dec.deg # we need dec_offset to NCP
    #     offset_sign = 1.0

    obser, star, star_offset, obs_time_list = build_basic_object(
        pos_pkg,
        config.location.geodetic[1].to("deg").value,
        config.location.geodetic[0].to("deg").value,
        config.location.geodetic[2].to("meter").value,
        ra_hours,
        dec_degrees,
        dec_offset,
        times,
        None if isinstance(source, str) else source,
    )
    rtimes = times_to_mjd(obs_time_list, pos_pkg)

    for itime, obs_time in enumerate(obs_time_list):
        ants_uvw = calculate_uvw(
            obser,
            star,
            obs_time,
            star_offset,
            ants_xyz,
            config,
            uvw_model,
            pos_pkg,
            temperature,
            pressure,
            offset_sign,
        )

        for ibaseline, (a1, a2) in enumerate(baselines):
            if a1 != a2:
                # rweight[itime, ibaseline, ...] = weight
                # rimaging_weight[itime, ibaseline, ...] = weight

                # The size of the different antennas needs to be differentiated
                # (as is done in CASA) and is calculated here as a relative value
                rweight[itime, ibaseline, ...] = weight / (
                    (max_diameter_square / (diameter[a1] * diameter[a2])) ** 2
                )
                rimaging_weight[itime, ibaseline, ...] = weight / (
                    (max_diameter_square / (diameter[a1] * diameter[a2])) ** 2
                )
                rflags[itime, ibaseline, ...] = 0
            else:
                rweight[itime, ibaseline, ...] = 0.0
                rimaging_weight[itime, ibaseline, ...] = 0.0
                rflags[itime, ibaseline, ...] = 1

            ruvw[itime, ibaseline, :] = ants_uvw[a2, :] - ants_uvw[a1, :]

        if itime > 0:
            rintegrationtime[itime] = rtimes[itime] - rtimes[itime - 1]
        itime += 1

    if itime > 1:
        rintegrationtime[0] = rintegrationtime[1]
    else:
        rintegrationtime[0] = integration_time

    rchannel_bandwidth = channel_bandwidth

    source_name = str(source)

    vis = Visibility.constructor(
        uvw=ruvw,
        time=rtimes,
        frequency=frequency,
        vis=rvis,
        weight=rweight,
        baselines=baselines,
        imaging_weight=rimaging_weight,
        flags=rflags,
        integration_time=rintegrationtime,
        channel_bandwidth=rchannel_bandwidth,
        polarisation_frame=polarisation_frame,
        source=source_name,
        meta=meta,
        phasecentre=phasecentre,
        configuration=config,
    )

    return vis
