"""
Base simple visibility operations, placed here to avoid circular dependencies
"""

__all__ = [
    "vis_summary",
    "copy_visibility",
    "create_visibility",
    "create_visibility_from_source",
    "phaserotate_visibility",
    "generate_baselines",
    "generate_baselines_no_auto",
    "get_baseline",
]

import copy
import logging
import re

import astropy.constants as const
import numpy
import pandas
import xarray
from astropy import units as u, constants as constants
from astropy.coordinates import SkyCoord, EarthLocation
from astropy.io import fits
from astropy.time import Time

from astropy.units import Quantity

from rascil2.data_models.memory_data_models import Visibility, Configuration
from rascil2.data_models.parameters import rascil2_data_path, get_parameter
from rascil2.processing_components.util.catalog import CatalogEntry
from rascil2.data_models.polarisation import (
    PolarisationFrame,
    ReceptorFrame,
    correlate_polarisation,
)
from rascil2.processing_components.util import skycoord_to_lmn
from rascil2.processing_components.util import (
    xyz_to_uvw,
    uvw_to_xyz,
    hadec_to_azel,
    xyz_at_latitude,
    ecef_to_enu,
    enu_to_ecef,
    eci_to_uvw,
    enu_to_eci,
)
from rascil2.processing_components.util.geometry import (
    calculate_transit_time,
    utc_to_ms_epoch,
)
from rascil2.processing_components.visibility.visibility_geometry import (
    calculate_visibility_transit_time,
    calculate_visibility_hourangles,
    calculate_visibility_azel,
)
from rascil2 import phyconst

import skyfield

from skyfield import api
from skyfield.api import wgs84
from skyfield.vectorlib import VectorSum


log = logging.getLogger("rascil2-logger")


# This convention agrees with that in the MS reader
# Note that ant2 > ant1


def generate_baselines(nant):
    """Generate mapping from antennas to baselines

    Note that we need to include autocorrelations since some input measurement sets
    may contain autocorrelations

    :param nant:
    :return:
    """
    for ant1 in range(0, nant):
        for ant2 in range(ant1, nant):
            yield ant1, ant2


def generate_baselines_no_auto(nant):
    """Generate mapping from antennas to baselines without autocorrelations
    :param nant:
    :return:
    """
    for ant1 in range(0, nant):
        for ant2 in range(ant1, nant):
            if ant1 != ant2:
                yield ant1, ant2


def get_baseline(ant1, ant2, baselines):
    """Given the antenna numbers work out the baseline number.

    :param ant1:
    :param ant2:
    :param baselines:
    :return:
    """
    return baselines.get_loc((ant1, ant2))


def vis_summary(vis: Visibility):
    """Return string summarizing the Visibility

    :param vis: Visibility
    :return: string
    """
    return "%d rows, %.3f GB" % (
        vis.visibility_acc.nvis,
        vis.visibility_acc.size(),
    )


def copy_visibility(vis: Visibility, zero=False) -> Visibility:
    """Copy a visibility

    Performs a deepcopy of the data array
    :param vis: Visibility
    :returns: Visibility

    """
    # assert isinstance(vis, Visibility), vis

    newvis = copy.deepcopy(vis)
    if zero:
        newvis["vis"].data[...] = 0.0
    return newvis


def create_visibility(
    config: Configuration,
    times: numpy.array,
    frequency: numpy.array,
    phasecentre: SkyCoord,
    weight: float = 1.0,
    polarisation_frame: PolarisationFrame = None,
    integration_time=1.0,
    channel_bandwidth=1e6,
    zerow=False,
    elevation_limit=15.0 * numpy.pi / 180.0,
    source="unknown",
    meta=None,
    utc_time=None,
    times_are_ha=True,
    **kwargs
) -> Visibility:
    """Create a Visibility from Configuration, hour angles, and direction of source

    Note that we keep track of the integration time for BDA purposes

    The input times are hour angles in radians, these are converted to UTC MJD in seconds, using utc_time as
    the approximate time.

    :param config: Configuration of antennas
    :param times: time or hour angles in radians
    :param times_are_ha: The times are hour angles (default) instead of utc time (in radians)
    :param frequency: frequencies (Hz] [nchan]
    :param weight: weight of a single sample
    :param phasecentre: phasecentre of observation (SkyCoord)
    :param channel_bandwidth: channel bandwidths: (Hz] [nchan]
    :param integration_time: Integration time ('auto' or value in s)
    :param polarisation_frame: PolarisationFrame('stokesI')
    :param integration_time: in seconds
    :param zerow: bool - set w to zero
    :param elevation_limit: in degrees
    :param source: Source name
    :param meta: Meta data as a dictionary
    :param utc_time: Time of ha definition
                     None: Time("2000-01-01T00:00:00", format='isot', scale='utc')
                     Time: Transit time
                     List/Numpy.Array: Observational time with UTC format ()

    :return: Visibility
    """
    assert phasecentre is not None, "Must specify phase centre"

    transit_time = None
    obs_time = None
    if utc_time is None:
        utc_time_zero = Time("2000-01-01T00:00:00", format="isot", scale="utc")
    elif isinstance(utc_time, Time):
        utc_time_zero = Time(
            utc_time.strftime("%Y-%m-%dT00:00:00"),
            format="isot",
            scale="utc",
        )
        transit_time = utc_time
        utc_time = None
    elif isinstance(utc_time, list) or isinstance(utc_time, numpy.ndarray):
        utc_time_zero = Time(
            utc_time[0].datetime.strftime("%Y-%m-%dT00:00:00"),
            format="isot",
            scale="utc",
        )
        obs_time = copy.deepcopy(utc_time)
        utc_time = None
    else:
        raise "utc_time must be one of None, Time, and list/numpy.ndarray"
    if polarisation_frame is None:
        polarisation_frame = correlate_polarisation(config.receptor_frame)

    latitude = config.location.geodetic[1].to("rad").value
    ants_xyz = config["xyz"].data
    ants_xyz = xyz_at_latitude(ants_xyz, latitude)
    nants = len(config["names"].data)
    diameter = config["diameter"].data
    max_diameter_square = numpy.max(diameter) ** 2

    baselines = pandas.MultiIndex.from_tuples(
        generate_baselines(nants), names=("antenna1", "antenna2")
    )
    nbaselines = len(baselines)

    # Find the number of integrations ab
    ntimes = 0
    n_flagged = 0

    for itime, time in enumerate(times):
        # Calculate the positions of the antennas as seen for this hour angle
        # and declination
        if times_are_ha:
            ha = time
        else:
            ha = time * (phyconst.sidereal_day_seconds / 86400.0)

        _, elevation = hadec_to_azel(ha, phasecentre.dec.rad, latitude)
        if elevation_limit is None or (elevation > elevation_limit):
            ntimes += 1
        else:
            n_flagged += 1

    assert ntimes > 0, "No unflagged points"

    if elevation_limit is not None and n_flagged > 0:
        log.info(
            "create_visibility: flagged %d/%d times below elevation limit %f (rad)"
            % (n_flagged, ntimes, 180.0 * elevation_limit / numpy.pi)
        )
    else:
        log.debug("create_visibility: created %d times" % (ntimes))

    npol = polarisation_frame.npol
    nchan = len(frequency)
    visshape = [ntimes, nbaselines, nchan, npol]
    rvis = numpy.zeros(visshape, dtype="complex")
    rflags = numpy.ones(visshape, dtype="int")
    rweight = numpy.ones(visshape)
    rimaging_weight = numpy.ones(visshape)
    rtimes = numpy.zeros([ntimes])
    rintegrationtime = numpy.zeros([ntimes])
    ruvw = numpy.zeros([ntimes, nbaselines, 3])

    if transit_time is None:
        stime = calculate_transit_time(config.location, utc_time_zero, phasecentre)
        if stime.masked:
            stime = utc_time_zero
    else:
        stime = transit_time

    # Do each time filling in the actual values
    itime = 0
    for time_index, time in enumerate(times):
        if times_are_ha:
            ha = time
        else:
            ha = time * (phyconst.sidereal_day_seconds / 86400.0)

        # Calculate the positions of the antennas as seen for this hour angle
        # and declination
        _, elevation = hadec_to_azel(ha, phasecentre.dec.rad, latitude)
        if elevation_limit is None or (elevation > elevation_limit):
            if obs_time is not None:
                rtimes[itime] = obs_time[time_index].mjd * 86400
            else:
                rtimes[itime] = stime.mjd * 86400.0 + time * 86400.0 / (2.0 * numpy.pi)
            rweight[itime, ...] = 1.0
            rflags[itime, ...] = 1

            # Loop over all pairs of antennas. Note that a2>a1
            ant_pos = xyz_to_uvw(ants_xyz, ha, phasecentre.dec.rad)

            for ibaseline, (a1, a2) in enumerate(baselines):
                if a1 != a2:
                    # rweight[itime, ibaseline, ...] = weight
                    # rimaging_weight[itime, ibaseline, ...] = weight

                    # The size of the different antennas needs to be differentiated
                    # (as is done in CASA) and is calculated here as a relative value
                    rweight[itime, ibaseline, ...] = weight / (
                        (max_diameter_square / (diameter[a1] * diameter[a2])) ** 2
                    )
                    rimaging_weight[itime, ibaseline, ...] = weight / (
                        (max_diameter_square / (diameter[a1] * diameter[a2])) ** 2
                    )
                    rflags[itime, ibaseline, ...] = 0
                else:
                    rweight[itime, ibaseline, ...] = 0.0
                    rimaging_weight[itime, ibaseline, ...] = 0.0
                    rflags[itime, ibaseline, ...] = 1

                ruvw[itime, ibaseline, :] = ant_pos[a2, :] - ant_pos[a1, :]
                # TODO: The flags here should not all be 0
                # rflags[itime, ibaseline, ...] = 0

            if itime > 0:
                rintegrationtime[itime] = rtimes[itime] - rtimes[itime - 1]
            itime += 1

    if itime > 1:
        rintegrationtime[0] = rintegrationtime[1]
    else:
        rintegrationtime[0] = integration_time
    rchannel_bandwidth = channel_bandwidth
    if zerow:
        ruvw[..., 2] = 0.0

    vis = Visibility.constructor(
        uvw=ruvw,
        time=rtimes,
        frequency=frequency,
        vis=rvis,
        weight=rweight,
        baselines=baselines,
        imaging_weight=rimaging_weight,
        flags=rflags,
        integration_time=rintegrationtime,
        channel_bandwidth=rchannel_bandwidth,
        polarisation_frame=polarisation_frame,
        source=source,
        meta=meta,
        phasecentre=phasecentre,
        configuration=config,
    )

    log.debug("create_visibility: %s" % (vis_summary(vis)))

    return vis


def create_visibility_from_source(
    config: Configuration,
    times: numpy.array,
    frequency: numpy.array,
    weight: float = 1.0,
    polarisation_frame: PolarisationFrame = None,
    integration_time=1.0,
    channel_bandwidth=1e6,
    zerow=False,
    elevation_limit=15.0 * numpy.pi / 180.0,
    source=None,
    meta=None,
    utc_time=None,
    **kwargs
) -> Visibility:
    """Create a Visibility from Configuration, hour angles, and direction of source

    Note that we keep track of the integration time for BDA purposes

    The input times are hour angles in radians, these are converted to UTC MJD in seconds, using utc_time as
    the approximate time.

    :param config: Configuration of antennas
    :param times: time or hour angles in radians
    :param frequency: frequencies (Hz] [nchan]
    :param weight: weight of a single sample
    :param channel_bandwidth: channel bandwidths: (Hz] [nchan]
    :param integration_time: Integration time ('auto' or value in s)
    :param polarisation_frame: PolarisationFrame('stokesI')
    :param integration_time: in seconds
    :param zerow: bool - set w to zero
    :param elevation_limit: in degrees
    :param source: CatalogEntry
    :param meta: Meta data as a dictionary
    :param utc_time: Time of ha definition default is Time("2000-01-01T00:00:00", format='isot', scale='utc')
    :return: Visibility
    """
    assert source is not None
    assert times is not None
    assert utc_time is not None
    assert source is not None

    if utc_time is None:
        utc_time_zero = Time("2000-01-01T00:00:00", format="isot", scale="utc")
    elif isinstance(utc_time, Time):
        utc_time_zero = utc_time
        utc_time = None

    if not isinstance(source, CatalogEntry):
        raise ValueError("source must be an instance of CatalogEntry")

    # Get ephemris data from rascil2 data
    __skyfield_data__ = rascil2_data_path("ephem")
    load = api.Loader(__skyfield_data__, verbose=False)

    if load.days_old("finals2000A.all") > 30.0:
        load.download("finals2000A.all")

    ts = load.timescale(builtin=False)

    planets = load("de421.bsp")
    earth: VectorSum = planets["earth"]

    obser: VectorSum = earth + wgs84.latlon(
        latitude_degrees=config.location.geodetic[1].to("deg").value,
        longitude_degrees=config.location.geodetic[0].to("deg").value,
        elevation_m=config.location.geodetic[2].to("meter").value,
    )

    source_obj = source.source

    t = ts.from_astropy(utc_time_zero)

    # Calculate Object position
    astrometric = obser.at(t).observe(source_obj)
    apparent = astrometric.apparent()

    ra, dec, d = apparent.radec("date")
    phasecentre = SkyCoord(
        ra=ra._degrees * u.deg, dec=dec.degrees * u.deg, frame="icrs"
    )

    ha_times = []
    for atime in times:
        t = ts.from_astropy(utc_time_zero + atime / 86400.0)

        # Calculate Object position
        astrometric = obser.at(t).observe(source_obj)
        apparent = astrometric.apparent()
        ha, dec = apparent.hadec()[0]._degrees, apparent.hadec()[1].degrees

        if ha > 180.0:
            ha = ha - 360.0
        ha_times.append((ha * u.deg).to("rad").value)

    vis = create_visibility(
        config,
        ha_times,
        frequency,
        phasecentre=phasecentre,
        weight=weight,
        channel_bandwidth=channel_bandwidth,
        integration_time=integration_time,
        source=source.name,
        elevation_limit=elevation_limit,
        utc_time=utc_time_zero,
        polarisation_frame=polarisation_frame,
        zerow=zerow,
        meta=meta,
    )

    log.debug("create_visibility_from_source: %s" % (vis_summary(vis)))

    return vis


def phaserotate_visibility(
    vis: Visibility, newphasecentre: SkyCoord, tangent=True, inverse=False
) -> Visibility:
    """Phase rotate from the current phase centre to a new phase centre

    If tangent is False the uvw are recomputed and the visibility phasecentre is updated.
    Otherwise only the visibility phases are adjusted

    :param vis: Visibility to be rotated
    :param newphasecentre: SkyCoord of new phasecentre
    :param tangent: Stay on the same tangent plane? (True)
    :param inverse: Actually do the opposite
    :return: Visibility or Visibility
    """
    l, m, n = skycoord_to_lmn(newphasecentre, vis.phasecentre)

    # No significant change?
    if numpy.abs(n) < 1e-15:
        return vis

    # Make a new copy
    newvis = copy_visibility(vis)

    phasor = calculate_visibility_phasor(newphasecentre, newvis)
    assert vis["vis"].data.shape == phasor.shape
    if inverse:
        newvis["vis"].data *= phasor
    else:
        newvis["vis"].data *= numpy.conj(phasor)
    # To rotate UVW, rotate into the global XYZ coordinate system and back. We have the option of
    # staying on the tangent plane or not. If we stay on the tangent then the raster will
    # join smoothly at the edges. If we change the tangent then we will have to reproject to get
    # the results on the same image, in which case overlaps or gaps are difficult to deal with.
    if not tangent:
        # The rotation can be done on the uvw (metres) values but we also have to update
        # The wavelength dependent values
        nrows, nbl, _ = vis.uvw.shape
        if inverse:
            uvw_linear = vis.uvw.data.reshape([nrows * nbl, 3])
            xyz = uvw_to_xyz(
                uvw_linear,
                ha=-newvis.phasecentre.ra.rad,
                dec=newvis.phasecentre.dec.rad,
            )
            uvw_linear = xyz_to_uvw(
                xyz, ha=-newphasecentre.ra.rad, dec=newphasecentre.dec.rad
            )[...]
        else:
            # This is the original (non-inverse) code
            uvw_linear = newvis.uvw.data.reshape([nrows * nbl, 3])
            xyz = uvw_to_xyz(
                uvw_linear,
                ha=-newvis.phasecentre.ra.rad,
                dec=newvis.phasecentre.dec.rad,
            )
            uvw_linear = xyz_to_uvw(
                xyz, ha=-newphasecentre.ra.rad, dec=newphasecentre.dec.rad
            )[...]
        newvis.attrs["phasecentre"] = newphasecentre
        newvis["uvw"].data[...] = uvw_linear.reshape([nrows, nbl, 3])
        newvis = calculate_visibility_uvw_lambda(newvis)
    return newvis


def calculate_visibility_phasor(direction, vis):
    """Calculate the phasor for a component for a Visibility

    :param comp:
    :param vis:
    :return:
    """
    # assert isinstance(vis, Visibility)
    ntimes, nbaseline, nchan, npol = vis["vis"].data.shape
    l, m, n = skycoord_to_lmn(direction, vis.phasecentre)
    s = numpy.array([l, m, numpy.sqrt(1 - l**2 - m**2) - 1.0])

    phasor = numpy.ones([ntimes, nbaseline, nchan, npol], dtype="complex")
    phasor[...] = numpy.exp(
        -2j * numpy.pi * numpy.einsum("tbfs,s->tbf", vis.uvw_lambda.data, s)
    )[..., numpy.newaxis]
    return phasor


def calculate_visibility_uvw_lambda(vis):
    """Recalculate the uvw_lambda values

    :param vis:
    :return:
    """
    k = vis.frequency.data / phyconst.c_m_s
    vis.uvw_lambda.data = numpy.einsum("tbs,k->tbks", vis.uvw.data, k)
    return vis
