"""
UVFITS Function of RASCIL2

Module for outputing visibilities to a UVFITS file.  The classes and
functions defined in this module are based heavily off the lwda_fits library.

note::
    For arrays with between 256 and 2048 antennas, the baseline packing
    follows the MIRIAD convention.

Thanks for the opensource codes from LSL project.
Copyright belongs to their respectful owner.
"""

import os
import gc
import logging
import numpy
from astropy.utils import iers
from astropy.io import fits as astrofits
from rascil2.processing_components.io.basedata import UVData, BaseData
from rascil2.processing_components.io.operations import (
    STOKES_CODES,
    geo_to_ecef,
    get_eci_transform,
)
from astropy.time import Time

log = logging.getLogger("rascil2-logger")

__all__ = [
    "Uv",
]


def merge_baseline(ant1, ant2, shift=None):
    """
    Merge two stand ID numbers into a single baseline.
    :param ant1: Antenna 1
    :param ant2: Antenna2
    :param shift: If needs shift
    :return: Baseline ID

    """

    if ant1 > 255 or ant2 > 255:
        baseline = ant1 * 2048 + ant2 + 65536
    else:
        baseline = ant1 * 256 + ant2

    return baseline


def split_baseline(baseline, shift=None):
    """
    Given a baseline, split it into it consistent stand ID numbers.
    :param baseline: Baseline ID
    :param shift: Needs shift or not
    :return: Two seperated antenna IDs
    """

    if baseline >= 65536:
        ant1 = int((baseline - 65536) // 2048)
        ant2 = int((baseline - 65536) % 2048)
    else:
        ant1 = int(baseline // 256)
        ant2 = int(baseline % 256)

    return ant1, ant2


class Uv(BaseData):
    """
    Class for storing visibility data and writing the data, along with array
    geometry, frequency setup, etc., to a UVFITS file that can be read into
    AIPS via the UVLOD task.
    """

    _STOKES_CODES = STOKES_CODES

    def __init__(
        self,
        filename,
        ref_time=0.0,
        source_name=None,
        frame="ITRF",
        verbose=False,
        memmap=None,
        overwrite=True,
    ):
        """
        Initialize a new UVFITS object using a filename and a reference time
        given in seconds since the UNIX 1970 ephem, a python datetime object, or a
        string in the format of 'YYYY-MM-DDTHH:MM:SS'.

        :param filename: Filename of UVFITS
        :param ref_time: Reference time (UTC0)
        :param source_name:  Observational source
        :param frame:  The number of Frames
        :param verbose: Verbose mode
        :param memmap: Memory map mode
        :param overwrite: Overwrite existent files.
        """

        # File-specific information
        BaseData.__init__(
            self,
            filename,
            ref_time=ref_time,
            source_name=source_name,
            frame=frame,
            verbose=verbose,
        )

        # Open the file and get going
        if os.path.exists(filename):
            if overwrite:
                os.unlink(filename)
            else:
                raise IOError("File '%s' already exists" % filename)
        self.FITS = astrofits.open(filename, mode="append", memmap=memmap)
        self.project = "RASCIL2"
        self.observer = "FENGWANG"

    def set_geometry(self, site_config, antennas, bits=8):
        """
        Given a station and an array of stands, set the relevant common observation
        parameters and add entries to the self.array list.

        :param site_config: Configuration of observational site
        :param antennas: Antenna lists
        :param bits: default is 8
        """

        # Make sure that we have been passed 2047 or fewer stands
        if len(antennas) > 2047:
            raise RuntimeError(
                "UVFITS supports up to 2047 antennas only, given %i" % len(antennas)
            )

        # Update the observatory-specific information
        self.site_config = site_config
        self.siteName = self.site_config.name

        stands = []
        for ant in antennas:
            stands.append(ant.stand.id)
        stands = numpy.array(stands)

        if site_config.location is not None:
            longitude = site_config.location.geodetic[0].to("rad").value
            latitude = site_config.location.geodetic[1].to("rad").value
            altitude = site_config.location.height.to("meter").value

            arrayX, arrayY, arrayZ = geo_to_ecef(latitude, longitude, altitude)
        else:
            arrayX, arrayY, arrayZ = 0, 0, 0

        # Create the stand mapper
        mapper = {}
        enableMapper = False
        if len(stands) > 2047:
            enableMapper = True

        xyz = site_config.xyz.data[:]

        # Create the stand mapper
        # No use if don't need to consider antenna name
        mapper = []
        ants = []
        if (self.frame).upper() not in ["WGS84", "ITRF"]:
            topo2eci = get_eci_transform(latitude, longitude)
            for i in range(len(stands)):
                eci = numpy.dot(topo2eci, xyz[i, :])
                ants.append(
                    self._Antenna(
                        stands[i],
                        eci[0] + arrayX,
                        eci[1] + arrayY,
                        eci[2] + arrayZ,
                        bits=bits,
                    )
                )
                mapper.append(stands[i])
        else:
            for i, ant_name in enumerate(stands):
                ants.append(
                    self._Antenna(
                        stands[i],
                        xyz[i, 0],
                        xyz[i, 1],
                        xyz[i, 2],
                        bits=bits,
                    )
                )
                mapper.append(stands[i])

        self.nant = len(ants)
        self.array.append(
            {
                "center": [arrayX, arrayY, arrayZ],
                "ants": ants,
                "mapper": mapper,
                "enableMapper": enableMapper,
                "inputAnts": antennas,
            }
        )

    def add_data_set(
        self,
        obstime,
        inttime,
        baselines,
        visibilities,
        weights=None,
        flags=None,
        pol="XX",
        source=None,
        phasecentre=None,
        uvw=None,
    ):
        """
        Create a UVData object to store a collection of visibilities.

        :param obstime: List of Observation time
        :param inttime: List of Integration Time
        :param baselines: Baselines
        :param visibilities: Visibility data
        :param weights: Weights
        :param flags: Flags
        :param pol: Polizations
        :param source: Observational source
        :param phasecentre: Phase center
        :param uvw:  UVW

        """

        if type(pol) == str:
            numericPol = self._STOKES_CODES[pol.upper()]
        else:
            numericPol = pol

        if flags is None:
            flags = numpy.zeros_like(visibilities, dtype="bool")
        self.data.append(
            UVData(
                obstime,
                inttime,
                baselines,
                visibilities,
                flags,
                weights=weights,
                pol=numericPol,
                source=source,
                phasecentre=phasecentre,
                uvw=uvw,
            )
        )

    def add_comment(self, comment):
        """
        Add a comment to data.
        :param comment: Comment string

        """

        try:
            self._comments.append(comment)
        except AttributeError:
            self._comments = [
                comment,
            ]

    def add_history(self, history):
        """
        Add a history entry to the data.

        :param history: History
        """

        try:
            self._history.append(history)
        except AttributeError:
            self._history = [
                history,
            ]

    def write(self):
        """
        Fill in the UVFITS file will all of the tables in the
        correct order.
        """

        # Validate
        if self.nstokes == 0:
            raise RuntimeError("No polarization setups defined")
        if len(self.freq) == 0:
            raise RuntimeError("No frequency setups defined")
        if len(self.freq) > 1:
            raise RuntimeError("Too many frequency setups defined")
        if self.nant == 0:
            raise RuntimeError("No array geometry defined")
        if len(self.data) == 0:
            raise RuntimeError("No visibility data defined")

        # Sort the data set
        self.data.sort()

        # self._write_aipssu_hdu(dummy=True)
        self._write_primary_hdu()
        self._write_aipsan_hdu()
        self._write_aipsfq_hdu()
        self._write_aipssu_hdu()
        # self._write_aipsbp_hdu()

        # Clear out the data section
        del self.data[:]
        gc.collect()

    def close(self):
        """
        Close out the file.
        """

        self.FITS.flush()
        self.FITS.close()

    def _add_common_keywords(self, hdr, name, revision):
        """
        Added keywords common to all table headers.

        :param hdr: Header
        :param name: Name
        :param revision: revision number
        """

        hdr["EXTNAME"] = (name, "UVFITS table name")
        hdr["EXTVER"] = (1, "table instance number")
        hdr["TABREV"] = (revision, "table format revision number")
        hdr["NO_IF"] = (len(self.freq), "number of frequency bands")

        # ref_time_tmp = self.ref_time.replace("T", "-").replace(":", "-")
        date = self.ref_time.split("T")
        # name = "UT%s%s%s" % (date[0][2:], date[1], date[2])
        # hdr["OBSCODE"] = (name, "zenith all-sky image")
        hdr["ARRNAM"] = self.siteName
        hdr["RDATE"] = (date[0], "file data reference date")

    def _write_primary_hdu(self):
        """
        Write the primary HDU to file.
        """

        # self._write_aipsan_hdu(dummy=True)
        # (arrPos, ag) = self.read_array_geometry(dummy=True)
        # (mapper, inverseMapper) = self.read_array_mapper(dummy=True)
        # ids = ag.keys()

        if self.site_config.location is not None:
            longitude = self.site_config.location.geodetic[0].to("deg").value
            latitude = self.site_config.location.geodetic[1].to("deg").value
            altitude = self.site_config.location.height.to("meter").value

        mapper = self.array[0]["mapper"]

        first = True
        mList = []
        uList = []
        vList = []
        wList = []
        timeList = []
        dateList = []
        intTimeList = []
        blineList = []
        nameList = []
        sourceList = []
        for dataSet in self.data:
            # Sort the data by packed baseline
            try:
                order
            except (NameError, UnboundLocalError):
                order = dataSet.argsort(mapper=mapper, shift=16)

            # Deal with defininig the values of the new data set
            if dataSet.pol == self.stokes[0]:
                ## Figure out the new date/time for the observation
                utc = Time(dataSet.obstime, format="unix", scale="utc")
                utc0 = Time(
                    utc.datetime.strftime("%Y-%m-%dT00:00:00"),
                    format="isot",
                    scale="utc",
                ).jd
                utc = utc.jd
                try:
                    utcR
                except NameError:
                    utcR = utc0 * 1.0
                ## Update the observer so we can figure out where the source is
                if dataSet.source is None:
                    ### Zenith pointings
                    sidereal = Time(
                        dataSet.obstime,
                        format="unix",
                        scale="utc",
                        location=self.site_config.location,
                    )
                    sidereal_time = sidereal.sidereal_time("apparent").value
                    # equ = astro.equ_posn(obs.sidereal_time() * 180 / numpy.pi, obs.lat * 180 / numpy.pi)
                    from astropy.coordinates import Angle

                    raHms = Angle(sidereal_time, "hour")
                    d, m, s = raHms.dms
                    ### format 'source' name based on local sidereal time
                    name = "T%02d%02d%02d%01d" % (
                        d,
                        m,
                        int(s),
                        int((s - int(s)) * 10.0),
                    )
                    ra = raHms.degree
                else:
                    ### Real-live sources (ephem.Body instances)
                    ra = dataSet.phasecentre.ra.value
                    dec = dataSet.phasecentre.dec.value
                    name = dataSet.source

                ## Update the source ID
                sourceID = 0
                # self._sourceTable.index(name) + 1

                ## Compute the uvw coordinates of all baselines
                if dataSet.source is None:
                    HA = 0.0
                    dec = latitude
                else:
                    HA = dataSet.phasecentre.ra.value / 15
                    dec = dataSet.phasecentre.dec.value

                if first is True:
                    sourceRA, sourceDec = ra, dec
                    first = False

                if dataSet.uvw is None:
                    uvwCoords = dataSet.get_uvw(HA, dec, self.site_config)
                else:
                    uvwCoords = dataSet.uvw

                # Populate the metadata
                # Add in the new baselines
                try:
                    blineList.extend(baselineMapped)
                except NameError:
                    baselineMapped = []
                    for o in order:
                        antenna1, antenna2 = dataSet.baselines[o]
                        if mapper is None:
                            stand1, stand2 = antenna1.stand.id, antenna2.stand.id
                        else:
                            stand1, stand2 = (
                                mapper.index(antenna1.stand.id) + 1,
                                mapper.index(antenna2.stand.id) + 1,
                            )
                        baselineMapped.append(merge_baseline(stand1, stand2))
                    blineList.extend(baselineMapped)

                ### Add in the new u, v, and w coordinates
                uList.extend(uvwCoords[order, 0])
                vList.extend(uvwCoords[order, 1])
                wList.extend(uvwCoords[order, 2])

                ### Add in the new date/time
                dateList.extend(
                    [
                        utc0,
                    ]
                    * len(dataSet.baselines)
                )
                timeList.extend(
                    [
                        float(utc - utc0),
                    ]
                    * len(dataSet.baselines)
                )
                intTimeList.extend(
                    [
                        dataSet.inttime,
                    ]
                    * len(dataSet.baselines)
                )

                ### Add in the new new source ID and name
                sourceList.extend(
                    [
                        sourceID,
                    ]
                    * len(dataSet.baselines)
                )
                nameList.extend(
                    [
                        name,
                    ]
                    * len(dataSet.baselines)
                )

                ### Zero out the visibility data
                try:
                    if matrix.shape[0] != len(order):
                        raise NameError
                    matrix *= 0.0
                except NameError:
                    matrix = numpy.zeros(
                        (len(order), 1, 1, self.nchan, self.nstokes, 2),
                        dtype=numpy.float32,
                    )

            # Save the visibility data in the right order
            matrix[:, 0, 0, :, self.stokes.index(dataSet.pol), 0] = (
                dataSet.visibilities[order, :].real
            )
            matrix[:, 0, 0, :, self.stokes.index(dataSet.pol), 1] = (
                dataSet.visibilities[order, :].imag
            )

            # Deal with saving the data once all of the polarizations have been added to 'matrix'
            if dataSet.pol == self.stokes[-1]:
                mList.append(matrix * 1.0)

        nbaseline = len(blineList)

        # Create the UV Data table and update its header
        uv = astrofits.GroupData(
            numpy.concatenate(mList),
            parnames=["UU", "VV", "WW", "DATE", "DATE", "BASELINE", "SOURCE", "INTTIM"],
            pardata=[
                numpy.array(uList, dtype=numpy.float32),
                numpy.array(vList, dtype=numpy.float32),
                numpy.array(wList, dtype=numpy.float32),
                numpy.array(dateList),
                numpy.array(timeList),
                numpy.array(blineList),
                numpy.array(sourceList),
                numpy.array(intTimeList),
            ],
            parbscales=[1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
            parbzeros=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
            bitpix=-32,
        )
        primary = astrofits.GroupsHDU(uv)

        primary.header["EXTEND"] = (True, "indicates UVFITS file")
        primary.header["GROUPS"] = (True, "indicates UVFITS file")
        primary.header["OBJECT"] = "BINARYTB"
        primary.header["TELESCOP"] = self.siteName
        primary.header["INSTRUME"] = self.siteName
        primary.header["OBSERVER"] = (self.observer, "Observer name(s)")
        primary.header["PROJECT"] = (self.project, "Project name")
        primary.header["ORIGIN"] = "MUSER"
        primary.header["DATE-OBS"] = (self.ref_time, "UVFITS file data collection date")
        primary.header["EPOCH"] = 2000.0
        # ts = str(astro.get_date_from_sys())
        # primary.header["DATE-MAP"] = (ts.split()[0], "UVFITS file creation date")

        primary.header["CTYPE2"] = ("COMPLEX", "axis 2 is COMPLEX axis")
        primary.header["CDELT2"] = 1.0
        primary.header["CRPIX2"] = 1.0
        primary.header["CRVAL2"] = 1.0

        primary.header["CTYPE3"] = ("STOKES", "axis 3 is STOKES axis (polarization)")
        if self.stokes[0] < 0:
            primary.header["CDELT3"] = -1.0
        else:
            primary.header["CDELT3"] = 1.0
        primary.header["CRPIX3"] = 1.0
        primary.header["CRVAL3"] = float(self.stokes[0])

        primary.header["CTYPE4"] = ("FREQ", "axis 4 is FREQ axis (frequency)")
        primary.header["CDELT4"] = self.freq[0].chWidth
        primary.header["CRPIX4"] = self.refPix
        primary.header["CRVAL4"] = self.refVal

        primary.header["CTYPE5"] = (
            "RA",
            "axis 5 is RA axis (position of phase center)",
        )
        primary.header["CDELT5"] = 0.0
        primary.header["CRPIX5"] = 1.0
        primary.header["CRVAL5"] = ra

        primary.header["CTYPE6"] = (
            "DEC",
            "axis 6 is DEC axis (position of phase center)",
        )
        primary.header["CDELT6"] = 0.0
        primary.header["CRPIX6"] = 1.0
        primary.header["CRVAL6"] = dec

        primary.header["TELESCOP"] = self.siteName
        primary.header["OBSERVER"] = self.observer
        primary.header["SORT"] = ("TB", "data is sorted in [time,baseline] order")

        primary.header["VISSCALE"] = (1.0, "UV data scale factor")

        # Write extra header values
        # for name in self.extra_keywords:
        #     primary.header[name] = self.extra_keywords[name]

        # Write the comments and history
        try:
            for comment in self._comments:
                primary.header["COMMENT"] = comment
            del self._comments
        except AttributeError:
            pass
        primary.header["COMMENT"] = (
            " FITS (Flexible Image Transport System) format is defined in 'Astronomy and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H"
        )
        try:
            for hist in self._history:
                primary.header["HISTORY"] = hist
            del self._history
        except AttributeError:
            pass

        self.FITS.append(primary)
        self.FITS.flush()

    def _write_aipsan_hdu(self):
        """
        Define the 'AIPS AN' table .
        """

        i = 0
        names = []
        xyz = numpy.zeros((self.nant, 3), dtype=numpy.float64)
        for ant in self.array[0]["ants"]:
            xyz[i, :] = numpy.array([ant.x, ant.y, ant.z])
            names.append(ant.getName())
            i = i + 1

        # Antenna name
        c1 = astrofits.Column(
            name="ANNAME",
            format="A8",
            array=numpy.array([ant.getName() for ant in self.array[0]["ants"]]),
        )
        # Station coordinates in meters
        c2 = astrofits.Column(name="STABXYZ", unit="METERS", format="3D", array=xyz)
        # Station number
        c3 = astrofits.Column(
            name="NOSTA",
            format="1J",
            array=numpy.array(
                [
                    self.array[0]["mapper"].index(ant.id) + 1
                    for ant in self.array[0]["ants"]
                ]
            ),
        )
        # Mount type (0 == alt-azimuth)
        c4 = astrofits.Column(
            name="MNTSTA",
            format="1J",
            array=numpy.ones((self.nant,), dtype=numpy.int32) * 6,
        )
        # Axis offset in meters
        c5 = astrofits.Column(
            name="STAXOF",
            unit="METERS",
            format="1E",
            array=numpy.zeros((self.nant,), dtype=numpy.float32),
        )

        # Diameter
        c6 = astrofits.Column(
            name="DIAMETER",
            unit="METERS",
            format="1E",
            array=numpy.ones((self.nant,), dtype=numpy.float32) * 4.5,
        )
        # Beam FWHM
        c7 = astrofits.Column(
            name="BEAMFWHM",
            unit="DEGR/M",
            format="1E",
            array=numpy.ones(self.nant, dtype=numpy.float32) * 360.0,
        )

        # Feed A polarization label
        c8 = astrofits.Column(
            name="POLTYA",
            format="A1",
            array=numpy.array(["R" for ant in self.array[0]["ants"]]),
        )
        # Feed A orientation in degrees
        c9 = astrofits.Column(
            name="POLAA",
            format="1E",
            unit="DEGREES",
            array=numpy.array(
                [0.0 for ant in self.array[0]["ants"]],
                dtype=numpy.float32,
            ),
        )
        # Feed A polarization parameters
        c10 = astrofits.Column(
            name="POLCALA",
            format="2E",
            array=numpy.array(
                [0.0 for ant in self.array[0]["ants"]], dtype=numpy.float32
            ),
        )
        # Feed B polarization label
        c11 = astrofits.Column(
            name="POLTYB",
            format="A1",
            array=numpy.array(["L" for ant in self.array[0]["ants"]]),
        )
        # Feed B orientation in degrees
        c12 = astrofits.Column(
            name="POLAB",
            format="1E",
            unit="DEGREES",
            array=numpy.array(
                [0.0 for ant in self.array[0]["ants"]],
                dtype=numpy.float32,
            ),
        )
        # Feed B polarization parameters
        c13 = astrofits.Column(
            name="POLCALB",
            format="2E",
            array=numpy.array(
                [0.0 for ant in self.array[0]["ants"]], dtype=numpy.float32
            ),
        )

        # Define the collection of columns
        colDefs = astrofits.ColDefs(
            [c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13]
        )

        # Create the table and fill in the header
        an = astrofits.BinTableHDU.from_columns(colDefs)
        self._add_common_keywords(an.header, "AIPS AN", 1)

        an.header["EXTVER"] = (1, "array ID")
        an.header["ARRNAM"] = self.siteName
        an.header["FRAME"] = ("GEOCENTRIC", "coordinate system")
        an.header["NUMORB"] = (0, "number of orbital parameters")
        an.header["FREQ"] = (self.refVal, "reference frequency (Hz)")
        an.header["TIMSYS"] = ("UTC", "time coordinate system")
        an.header["XYZHAND"] = "RIGHT"
        sidereal = Time(
            self.ref_time,
            scale="utc",
            location=self.site_config.location,
        )
        sidereal_time = sidereal.sidereal_time("apparent").value
        an.header["GSTIA0"] = (sidereal_time, "GAST (deg) at RDATE 0 hours")
        an.header["DEGPDY"] = (360.0, "rotation rate of the earth (deg/day)")
        an.header["UT1UTC"] = (0.0, "difference UT1 - UTC for reference date")
        an.header["IATUTC"] = (0.0, "TAI - UTC for reference date")
        an.header["POLARX"] = 0.0
        an.header["POLARY"] = 0.0

        an.header["ARRAYX"] = (self.array[0]["center"][0], "array ECI X coordinate (m)")
        an.header["ARRAYY"] = (self.array[0]["center"][1], "array ECI Y coordinate (m)")
        an.header["ARRAYZ"] = (self.array[0]["center"][2], "array ECI Z coordinate (m)")

        an.header["NOSTAMAP"] = (
            int(self.array[0]["enableMapper"]),
            "Mapping enabled for stand numbers",
        )

        an.name = "AIPS AN"
        self.FITS.append(an)
        self.FITS.flush()

        if self.array[0]["enableMapper"]:
            self._write_mapper_hdu()

    def _write_aipsfq_hdu(self):
        """
        Define the 'AIPS FQ' table .
        """

        # Frequency setup number
        c1 = astrofits.Column(
            name="FRQSEL",
            format="1J",
            array=numpy.array([self.freq[0].id], dtype=numpy.int32),
        )
        # Frequency offsets in Hz
        c2 = astrofits.Column(
            name="IF FREQ",
            format="1D",
            unit="HZ",
            array=numpy.array([self.freq[0].bandFreq], dtype=numpy.float64),
        )
        # Channel width in Hz
        c3 = astrofits.Column(
            name="CH WIDTH",
            format="1E",
            unit="HZ",
            array=numpy.array([self.freq[0].chWidth], dtype=numpy.float32),
        )
        # Total bandwidths of bands
        c4 = astrofits.Column(
            name="TOTAL BANDWIDTH",
            format="1E",
            unit="HZ",
            array=numpy.array([self.freq[0].totalBW], dtype=numpy.float32),
        )
        # Sideband flag
        c5 = astrofits.Column(
            name="SIDEBAND",
            format="1J",
            array=numpy.array([self.freq[0].sideBand], dtype=numpy.int32),
        )

        # Define the collection of columns
        colDefs = astrofits.ColDefs([c1, c2, c3, c4, c5])

        # Create the table and fill in the header
        fq = astrofits.BinTableHDU.from_columns(colDefs)
        self._add_common_keywords(fq.header, "AIPS FQ", 1)

        self.FITS.append(fq)
        self.FITS.flush()

    def _write_aipsbp_hdu(self):
        """
        Define the 'AIPS BP' table.
        """

        # Central time of period covered by record in days
        c1 = astrofits.Column(
            name="TIME",
            unit="DAYS",
            format="1D",
            array=numpy.zeros((self.nant,), dtype=numpy.float64),
        )
        # Duration of period covered by record in days
        c2 = astrofits.Column(
            name="INTERVAL",
            unit="DAYS",
            format="1E",
            array=(2 * numpy.ones((self.nant,), dtype=numpy.float32)),
        )
        # Source ID
        c3 = astrofits.Column(
            name="SOURCE ID",
            format="1J",
            array=numpy.zeros((self.nant,), dtype=numpy.int32),
        )
        # Sub-array number
        c4 = astrofits.Column(
            name="SUBARRAY",
            format="1J",
            array=numpy.ones((self.nant,), dtype=numpy.int32),
        )
        # Frequency setup number
        c5 = astrofits.Column(
            name="FREQ ID",
            format="1J",
            array=(numpy.zeros((self.nant,), dtype=numpy.int32) + self.freq[0].id),
        )
        # Antenna number
        c6 = astrofits.Column(
            name="ANTENNA", format="1J", array=self.FITS["AIPS AN"].data.field("NOSTA")
        )
        # Bandwidth in Hz
        c7 = astrofits.Column(
            name="BANDWIDTH",
            unit="HZ",
            format="1E",
            array=(
                numpy.zeros((self.nant,), dtype=numpy.float32) + self.freq[0].totalBW
            ),
        )
        # Band frequency in Hz
        c8 = astrofits.Column(
            name="CHN_SHIFT",
            format="1D",
            array=(
                numpy.zeros((self.nant,), dtype=numpy.float64) + self.freq[0].bandFreq
            ),
        )
        # Reference antenna number (pol. 1)
        c9 = astrofits.Column(
            name="REFANT 1",
            format="1J",
            array=numpy.ones((self.nant,), dtype=numpy.int32),
        )
        # Solution weight (pol. 1)
        c10 = astrofits.Column(
            name="WEIGHT 1",
            format="%dE" % self.nchan,
            array=numpy.ones((self.nant, self.nchan), dtype=numpy.float32),
        )
        # Real part of the bandpass (pol. 1)
        c11 = astrofits.Column(
            name="REAL 1",
            format="%dE" % self.nchan,
            array=numpy.ones((self.nant, self.nchan), dtype=numpy.float32),
        )
        # Imaginary part of the bandpass (pol. 1)
        c12 = astrofits.Column(
            name="IMAG 1",
            format="%dE" % self.nchan,
            array=numpy.zeros((self.nant, self.nchan), dtype=numpy.float32),
        )
        # Reference antenna number (pol. 2)
        c13 = astrofits.Column(
            name="REFANT 2",
            format="1J",
            array=numpy.ones((self.nant,), dtype=numpy.int32),
        )
        # Solution weight (pol. 2)
        c14 = astrofits.Column(
            name="WEIGHT 2",
            format="%dE" % self.nchan,
            array=numpy.ones((self.nant, self.nchan), dtype=numpy.float32),
        )
        # Real part of the bandpass (pol. 2)
        c15 = astrofits.Column(
            name="REAL 2",
            format="%dE" % self.nchan,
            array=numpy.ones((self.nant, self.nchan), dtype=numpy.float32),
        )
        # Imaginary part of the bandpass (pol. 2)
        c16 = astrofits.Column(
            name="IMAG 2",
            format="%dE" % self.nchan,
            array=numpy.zeros((self.nant, self.nchan), dtype=numpy.float32),
        )

        colDefs = astrofits.ColDefs(
            [c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16]
        )

        # Create the Bandpass table and update its header
        bp = astrofits.BinTableHDU.from_columns(colDefs)
        self._add_common_keywords(bp.header, "AIPS BP", 1)

        bp.header["NO_ANT"] = self.nant
        bp.header["NO_POL"] = 2
        bp.header["NO_CHAN"] = self.nchan
        bp.header["STRT_CHN"] = self.refPix
        bp.header["NO_SHFTS"] = 1
        bp.header["BP_TYPE"] = " "

        self.FITS.append(bp)
        self.FITS.flush()

    def _write_aipssu_hdu(self):
        """
        Define the 'AIPS SU' table.
        """

        if self.site_config.location is not None:
            longitude = self.site_config.location.geodetic[0].to("deg").value
            latitude = self.site_config.location.geodetic[1].to("deg").value
            altitude = self.site_config.location.height.to("meter").value

        nameList = []
        raList = []
        decList = []
        raPoList = []
        decPoList = []
        sourceID = 0
        for dataSet in self.data:
            if dataSet.pol == self.stokes[0]:
                utc0 = Time(dataSet.obstime, format="mjd", scale="utc")
                utc = utc0.jd

                try:
                    currSourceName = dataSet.source.name
                except AttributeError:
                    currSourceName = dataSet.source

                if currSourceName not in nameList:
                    sourceID += 1

                    if dataSet.source is None:
                        ### Zenith pointings
                        sidereal = Time(
                            dataSet.obstime,
                            format="unix",
                            scale="utc",
                            location=self.site_config.location,
                        )
                        sidereal_time = sidereal.sidereal_time("apparent").value
                        ra = sidereal_time * 15
                        dec = latitude
                        # equ = astro.equ_posn(obs.sidereal_time() * 180 / numpy.pi, obs.lat * 180 / numpy.pi)
                        from astropy.coordinates import Angle

                        raHms = Angle(sidereal_time, "hour")
                        d, m, s = raHms.dms
                        # format 'source' name based on local sidereal time
                        name = "T%02d%02d%02d%01d" % (
                            d,
                            m,
                            int(s),
                            int((s - int(s)) * 10.0),
                        )
                    else:
                        ## Real-live sources (ephem.Body instances)
                        ra = dataSet.phasecentre.ra.value
                        dec = dataSet.phasecentre.dec.value
                        name = dataSet.source

                    # current apparent zenith equatorial coordinates
                    raList.append(ra)
                    decList.append(dec)

                    # J2000 zenith equatorial coordinates
                    raPoList.append(ra)
                    decPoList.append(dec)

                    # name
                    nameList.append(name)

        nSource = len(nameList)

        # Save these for later since we might need them
        self._sourceTable = nameList

        # Source ID number
        c1 = astrofits.Column(
            name="ID. NO.",
            format="1J",
            array=numpy.arange(1, nSource + 1, dtype=numpy.int32),
        )
        # Source name
        c2 = astrofits.Column(name="SOURCE", format="A16", array=numpy.array(nameList))
        # Source qualifier
        c3 = astrofits.Column(
            name="QUAL", format="1J", array=numpy.zeros((nSource,), dtype=numpy.int32)
        )
        # Calibrator code
        c4 = astrofits.Column(
            name="CALCODE", format="A4", array=numpy.array(("   ",)).repeat(nSource)
        )
        # Stokes I flux density in Jy
        c5 = astrofits.Column(
            name="IFLUX",
            format="1E",
            unit="JY",
            array=numpy.zeros((nSource,), dtype=numpy.float32),
        )
        # Stokes I flux density in Jy
        c6 = astrofits.Column(
            name="QFLUX",
            format="1E",
            unit="JY",
            array=numpy.zeros((nSource,), dtype=numpy.float32),
        )
        # Stokes I flux density in Jy
        c7 = astrofits.Column(
            name="UFLUX",
            format="1E",
            unit="JY",
            array=numpy.zeros((nSource,), dtype=numpy.float32),
        )
        # Stokes I flux density in Jy
        c8 = astrofits.Column(
            name="VFLUX",
            format="1E",
            unit="JY",
            array=numpy.zeros((nSource,), dtype=numpy.float32),
        )
        # Frequency offset in Hz
        c9 = astrofits.Column(
            name="FREQOFF",
            format="1D",
            unit="HZ",
            array=numpy.zeros((nSource,), dtype=numpy.float64),
        )
        # Bandwidth
        c10 = astrofits.Column(
            name="BANDWIDTH",
            format="1D",
            unit="HZ",
            array=numpy.zeros((nSource,), dtype=numpy.float64),
        )
        # Right ascension at mean equinox in degrees
        c11 = astrofits.Column(
            name="RAEPO", format="1D", unit="DEGREES", array=numpy.array(raPoList)
        )
        # Declination at mean equinox in degrees
        c12 = astrofits.Column(
            name="DECEPO", format="1D", unit="DEGREES", array=numpy.array(decPoList)
        )
        # Epoch
        c13 = astrofits.Column(
            name="EPOCH",
            format="1D",
            unit="YEARS",
            array=numpy.zeros((nSource,), dtype=numpy.float64) + 2000.0,
        )
        # Apparent right ascension in degrees
        c14 = astrofits.Column(
            name="RAAPP", format="1D", unit="DEGREES", array=numpy.array(raList)
        )
        # Apparent declination in degrees
        c15 = astrofits.Column(
            name="DECAPP", format="1D", unit="DEGREES", array=numpy.array(decList)
        )
        # LSR frame systemic velocity in m/s
        c16 = astrofits.Column(
            name="LSRVEL",
            format="1D",
            unit="M/SEC",
            array=numpy.zeros((nSource,), dtype=numpy.float64),
        )
        # Line rest frequency in Hz
        c17 = astrofits.Column(
            name="RESTFREQ",
            format="1D",
            unit="HZ",
            array=(numpy.zeros((nSource,), dtype=numpy.float64) + self.refVal),
        )
        # Proper motion in RA in degrees/day
        c18 = astrofits.Column(
            name="PMRA",
            format="1D",
            unit="DEG/DAY",
            array=numpy.zeros((nSource,), dtype=numpy.float64),
        )
        # Proper motion in Dec in degrees/day
        c19 = astrofits.Column(
            name="PMDEC",
            format="1D",
            unit="DEG/DAY",
            array=numpy.zeros((nSource,), dtype=numpy.float64),
        )

        # Define the collection of columns
        colDefs = astrofits.ColDefs(
            [
                c1,
                c2,
                c3,
                c4,
                c5,
                c6,
                c7,
                c8,
                c9,
                c10,
                c11,
                c12,
                c13,
                c14,
                c15,
                c16,
                c17,
                c18,
                c19,
            ]
        )

        # Create the table and fill in the header
        su = astrofits.BinTableHDU.from_columns(colDefs)
        self._add_common_keywords(su.header, "AIPS SU", 1)
        su.header["FREQID"] = 1
        su.header["VELDEF"] = ""
        su.header["VECTYP"] = ""

        self.FITS.append(su)
        self.FITS.flush()

    def _write_mapper_hdu(self):
        """
        Write a fits table that contains information about mapping stations
        numbers to actual antenna numbers.  This information can be backed out of
        the names, but this makes the extraction more programmatic.
        """

        c1 = astrofits.Column(
            name="ANNAME",
            format="A8",
            array=numpy.array([ant.get_name() for ant in self.array[0]["ants"]]),
        )
        c2 = astrofits.Column(
            name="NOSTA",
            format="1J",
            array=numpy.array(
                [self.array[0]["mapper"][ant.id] for ant in self.array[0]["ants"]]
            ),
        )
        c3 = astrofits.Column(
            name="NOACT",
            format="1J",
            array=numpy.array([ant.id for ant in self.array[0]["ants"]]),
        )

        colDefs = astrofits.ColDefs([c1, c2, c3])

        # Create the ID mapping table and update its header
        nsm = astrofits.BinTableHDU.from_columns(colDefs)
        self._add_common_keywords(nsm.header, "NOSTA_MAPPER", 1)

        nsm.name = "NOSTA_MAPPER"
        self.FITS.append(nsm)
        self.FITS.flush()
