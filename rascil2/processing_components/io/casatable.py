"""
Basic data exchange module with casa table, Synchronization from ska-sdp-datamodels
https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/merge_requests/26
"""

__all__ = [
    "import_gaintable_from_casa_cal_table",
]

import numpy
import logging
from astropy import units as u
from astropy.units import Quantity
from astropy.coordinates import SkyCoord, EarthLocation

from rascil2.data_models.memory_data_models import GainTable, Configuration
from rascil2.data_models.polarisation import ReceptorFrame

log = logging.getLogger("rascil2-logger")


# Below are helper functions for import_gaintable_from_casa_cal_table
def _load_casa_tables(msname):
    # pylint: disable=import-error,import-outside-toplevel
    from casacore.tables import table

    base_table = table(tablename=msname)
    # spw --> spectral window
    spw = table(tablename=f"{msname}/SPECTRAL_WINDOW")
    obs = table(tablename=f"{msname}/OBSERVATION")
    anttab = table(f"{msname}/ANTENNA", ack=False)
    fieldtab = table(f"{msname}/FIELD", ack=False)
    return anttab, base_table, fieldtab, obs, spw


def _get_phase_centre_from_cal_table(field_table):
    phase_dir = field_table.getcol(columnname="PHASE_DIR")
    phase_centre = SkyCoord(
        ra=phase_dir[0][0][0] * u.rad,
        dec=phase_dir[0][0][1] * u.rad,
        frame="icrs",
        equinox="J2000",
    )
    return phase_centre


def _generate_configuration_from_cal_table(
    antenna_table, telescope_name, receptor_frame
):
    names = numpy.array(antenna_table.getcol("NAME"))
    mount = numpy.array(antenna_table.getcol("MOUNT"))[names != ""]
    diameter = numpy.array(antenna_table.getcol("DISH_DIAMETER"))[names != ""]
    xyz = numpy.array(antenna_table.getcol("POSITION"))[names != ""]
    offset = numpy.array(antenna_table.getcol("OFFSET"))[names != ""]
    stations = numpy.array(antenna_table.getcol("STATION"))[names != ""]

    location = EarthLocation(
        x=Quantity(xyz[0][0], "m"),
        y=Quantity(xyz[0][1], "m"),
        z=Quantity(xyz[0][2], "m"),
    )

    configuration = Configuration.constructor(
        name=telescope_name,
        location=location,
        names=names,
        xyz=xyz,
        mount=mount,
        frame="ITRF",
        receptor_frame=receptor_frame,
        diameter=diameter,
        offset=offset,
        stations=stations,
    )
    return configuration


def _set_jones_type(base_table, jones_type):
    """
    Obtain the calibration solution type from the table and use this in
    preference to any user-defined value. If the table does not have this
    information, use the user-defined value.

    :param base_table: main CASA table
    :param jones_type: user-defined jones_type string
    :return: reformatted numpy arrays gains, gain_time, gain_interval, antenna
    """
    table_jones_type = ""
    try:
        table_jones_type = base_table.getkeyword("VisCal")
        log.info("Using table Jones type %s", table_jones_type)
    except RuntimeError:
        log.warning("No main-table keyword VisCal")

    if table_jones_type == "":
        log.info("Using user-defined Jones type %s", jones_type)
        table_jones_type = jones_type
    else:
        # update jones_type based on the VisCal keyword
        # this should just be the first character
        #  - "G Jones" -> "G"
        #  - "B Jones" -> "B"
        #  - "Df Jones" -> "D"
        #  - "K Jones" -> "K"
        #  - "Kcross Jones" -> "K"
        jones_type = table_jones_type[0]

    # interpret the VisCal string so we know how to read and stored the data
    # "G Jones", "B Jones", "Df Jones", "K Jones", "Kcross Jones"
    is_delay = False
    if table_jones_type[0] == "K":
        # "K Jones" or "Kcross Jones" delay terms
        is_delay = True
    is_leakage = False
    if (
        # "Df Jones" or "Kcross Jones" leakage terms
        table_jones_type.find("D") == 0
        or table_jones_type.find("Kcross") == 0
    ):
        is_leakage = True

    return jones_type, is_leakage, is_delay


def _reshape_3d_gain_tables(gains, gain_time, gain_interval, antenna):
    """
    reformat casa gain tables with shape [ntimes*nants, nfrequency, nrec] to
    have shape [ntimes, nants, nfrequency, nrec]. Initial rows are assumed to
    cycle through antennas for each time step. Time and antenana arrays are
    also reduced to one row per time or antenna respectively.

    :param gains: numpy array with shape [ntimes*nants, nfrequency, nrec]
    :param gain_time: numpy array with shape [ntimes*nants]
    :param gain_interval: numpy array with shape [ntimes*nants]
    :param antenna: numpy array with shape [ntimes*nants]
    :return: reformatted numpy arrays gains, gain_time, gain_interval, antenna

    """

    if gains.ndim != 3:
        raise ValueError(f"Expect 3d gains array, have {gains.ndim}")

    input_shape = numpy.shape(gains)

    nrow = input_shape[0]
    nfrequency = input_shape[1]
    nrec = input_shape[2]

    # Antenna rows in the same solution interval may have different times,
    # so cannot set ntimes based on unique time tags. Use the fact that we
    # require each antenna to have one row per solution interval to define
    # ntimes
    antenna = numpy.unique(antenna)
    nants = len(antenna)
    if nrow % nants != 0:
        raise ValueError("Require each antenna in each solution interval")
    ntimes = nrow // nants

    gains = numpy.reshape(gains, (ntimes, nants, nfrequency, nrec))

    # GainTable wants time and increment vectors with one value per
    # solution interval, however the main CASA cal table columns have
    # one row for each solution interval and antenna. Need to remove
    # duplicate values. Take the average time value per solution interval
    gain_time = numpy.mean(numpy.reshape(gain_time, (ntimes, nants)), axis=1)

    # check that the times are increasing
    if numpy.any(numpy.diff(gain_time) <= 0):
        raise ValueError(f"Time error {gain_time-gain_time[0]}")

    # take a single soln interval value per time (scan_id)
    if len(gain_interval) == nants * ntimes:
        gain_interval = gain_interval[::nants, ...]
    else:
        raise ValueError(f"interval length error: {len(gain_interval)}")

    return gains, gain_time, gain_interval, antenna


def _gain_tables_to_jones(table, frequency, is_leakage, is_delay):
    """
    Add the two table polarisations into Jones matrices in an appropriate way

    :param table: numpy gains array with shape [ntimes*nants,nfrequency,nrec]
    :param frequency: list of frequencies for converting time delay to phase
    :param is_leakage: list of frequencies for converting time delay to phase
    :return: numpy gains array with shape [ntimes*nants,nfrequency,nrec,nrec]
    """
    table_shape = table.shape
    ntimes = table_shape[0]
    nants = table_shape[1]
    nfrequency = table_shape[2]
    nrec = table_shape[3]

    gain = numpy.ones((ntimes, nants, nfrequency, nrec, nrec), dtype="complex")

    if nrec == 1:
        gain[..., 0, 0] = table[..., 0]
    elif nrec == 2 and not is_delay:
        if not is_leakage:
            # standard table Jones: G or B = [[gx,0],[0,gy]]
            gain[..., 0, 0] = table[..., 0]
            gain[..., 0, 1] = 0.0
            gain[..., 1, 0] = 0.0
            gain[..., 1, 1] = table[..., 1]
        else:
            # standard leakages Jones: D = [[1,dxy],[-dyx,1]]
            # dyx is not defined with a -1 coeff in CASA
            gain[..., 0, 0] = 1.0
            gain[..., 0, 1] = table[..., 0]
            gain[..., 1, 0] = table[..., 1]
            gain[..., 1, 1] = 1.0
    elif nrec == 2 and is_delay:
        # convert ns time delays to phase at the reference frequency
        if frequency is None:
            raise ValueError("Require frequency list for delay conversion")
        ns2rad = 2 * numpy.pi * frequency[0] * 1e-9
        if nfrequency != 1:
            raise ValueError("expect a single channel for delay fits")
        if not is_leakage:
            # standard table Jones: G = [[gx,0],[0,gy]]
            phase = ns2rad * table
            gain[..., 0, 0] = numpy.exp(1j * phase[..., 0])
            gain[..., 0, 1] = 0.0
            gain[..., 1, 0] = 0.0
            gain[..., 1, 1] = numpy.exp(1j * phase[..., 1])
        else:
            # standard leakages Jones: D = [[1,dxy],[-dyx,1]]
            phase = ns2rad * (table[..., 0] - table[..., 1])
            dxy = numpy.exp(1j * phase)
            gain[..., 0, 0] = 1.0
            gain[..., 0, 1] = dxy
            gain[..., 1, 0] = numpy.conj(dxy)
            gain[..., 1, 1] = 1.0
    else:
        raise ValueError(f"Unsure how to import {nrec} polarisations")

    return gain


def import_gaintable_from_casa_cal_table(
    table_name,
    jones_type="B",
    rec_frame=ReceptorFrame("linear"),
) -> GainTable:
    """
    Create gain table from Calibration table of CASA.
    This import gain table form calibration table of CASA.

    :param table_name: Name of CASA table file
    :param jones_type: Type of calibration matrix T or G or B.
        Overwritten if table keyword "VisCal" is set
    :param rec_frame: Receptor Frame for the GainTable
    :return: GainTable object

    """
    anttab, base_table, fieldtab, obs, spw = _load_casa_tables(table_name)

    # Get times, interval, bandpass solutions
    gain_time = base_table.getcol(columnname="TIME")
    gain_interval = base_table.getcol(columnname="INTERVAL")
    antenna = base_table.getcol(columnname="ANTENNA1")
    spec_wind_id = base_table.getcol(columnname="SPECTRAL_WINDOW_ID")[0]

    # Obtain the calibration solution type from the table
    jones_type, is_leakage, is_delay = _set_jones_type(base_table, jones_type)

    # gains and leakages are stored in CPARAM[nrec,nfrequency]
    # delays are stored in FPARAM[nrec,1]
    if is_delay:
        gains = base_table.getcol(columnname="FPARAM")
    else:
        gains = base_table.getcol(columnname="CPARAM")

    # Get the frequency sampling information
    gain_frequency = spw.getcol(columnname="CHAN_FREQ")[spec_wind_id]
    nfrequency = spw.getcol(columnname="NUM_CHAN")[spec_wind_id]

    # Get receptor frame from Measurement set input
    # Currently we use the same for ideal/model and measured
    receptor_frame = rec_frame
    nrec = receptor_frame.nrec

    if gains.ndim == 3:
        gains, gain_time, gain_interval, antenna = _reshape_3d_gain_tables(
            gains, gain_time, gain_interval, antenna
        )

    if gains.ndim != 4:
        raise ValueError(f"Tables have unexpected shape: {gains.ndim}")

    ntimes = len(gain_time)
    nants = len(antenna)

    # final check of the main table shape
    input_shape = numpy.shape(gains)
    if ntimes != input_shape[0]:
        raise ValueError("gain and time columns are inconsistent")
    if nants != input_shape[1]:
        raise ValueError("gain and antenna columns are inconsistent")
    if nfrequency != input_shape[2]:
        raise ValueError(f"tables have wrong number of channels: {nfrequency}")
    if nrec != input_shape[3]:
        raise ValueError(f"Tables have wrong number of receptors: {nrec}")

    gain = _gain_tables_to_jones(gains, gain_frequency, is_leakage, is_delay)

    # Set the gain weight to one and residual to zero
    # This is temporary since in current tables they are not provided.
    gain_weight = numpy.ones(gain.shape)
    gain_residual = numpy.zeros([ntimes, nfrequency, nrec, nrec])

    # Get configuration
    ts_name = obs.getcol(columnname="TELESCOPE_NAME")[0]
    configuration = _generate_configuration_from_cal_table(
        anttab, ts_name, receptor_frame
    )

    # Get phase_centres
    phase_centre = _get_phase_centre_from_cal_table(fieldtab)

    # pylint: disable=duplicate-code
    gain_table = GainTable.constructor(
        gain=gain,
        time=gain_time,
        interval=gain_interval,
        weight=gain_weight,
        residual=gain_residual,
        frequency=gain_frequency,
        receptor_frame=receptor_frame,
        phasecentre=phase_centre,
        configuration=configuration,
        jones_type=jones_type,
    )

    return gain_table
