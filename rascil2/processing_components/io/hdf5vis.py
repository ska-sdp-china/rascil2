"""
Add new visibility hdf5 pattern support
"""

__all__ = [
    "save_visibility_to_hdf5",
    "create_visibility_from_hdf5",
    "convert_visibility_from_ms_to_hdf5",
]

import ast
import os
import shutil
import numpy
import logging
import h5py
import uuid
from rascil2.data_models.data_model_helpers import (
    convert_configuration_to_hdf,
    convert_configuration_from_hdf,
)
import pandas
import astropy.units as u
from astropy.units import Quantity

from astropy.coordinates import SkyCoord, EarthLocation

from rascil2.data_models.polarisation import PolarisationFrame, ReceptorFrame

from rascil2.data_models.memory_data_models import (
    Configuration,
    Visibility,
)
from rascil2.processing_components.visibility import (
    generate_baselines,
    generate_baselines_no_auto,
)
from rascil2.processing_components.io.regular_ms import (
    get_regularized_table,
)

from rascil2.processing_components.io.base import _get_polarisation_info

log = logging.getLogger("rascil2-logger")


def _save_vis_to_hdf5(vis, f, timeslice=None, blslice=None, chanslice=None):
    """save visibility to hdf5 group

    :param vis: vis
    :param f: hdf5 file object or group
    :param timeslice: slice times,it can be a numpy array, defaults to slice(None,None)
    :param blslice: baseline slice,alse can be numpy array, defaults to slice(None,None)
    :param chanslice: channel slice, defaults to slice(None,None)
    """
    # We only need to keep the things we need to reconstruct the data_model
    f.attrs["rascil_data_model"] = "Visibility"
    f.attrs["nants"] = numpy.max([b[1] for b in vis.baselines.data]) + 1
    f.attrs["nvis"] = vis.visibility_acc.nvis
    f.attrs["npol"] = vis.visibility_acc.npol
    f.attrs["phasecentre_coords"] = vis.phasecentre.to_string()
    f.attrs["phasecentre_frame"] = vis.phasecentre.frame.name
    f.attrs["polarisation_frame"] = vis.visibility_acc.polarisation_frame.type
    f.attrs["source"] = vis.source
    f.attrs["meta"] = str(vis.meta)
    f = convert_configuration_to_hdf(vis.configuration, f)

    f["data_frequency"] = vis["frequency"].data[chanslice]
    f["data_channel_bandwidth"] = vis["channel_bandwidth"].data[chanslice]
    f["data_uvw"] = vis["uvw"].data[timeslice, blslice, ...]
    f["data_time"] = vis["time"].data[timeslice]
    f["data_integration_time"] = vis["integration_time"].data[timeslice]
    f["data_flags"] = (
        vis["flags"].data[timeslice, blslice, chanslice, ...].astype("bool")
    )
    f["data_weight"] = vis["weight"].data[timeslice, blslice, slice(0, 1), ...]

    ntimes = f["data_uvw"].shape[0]
    nbaselines = f["data_uvw"].shape[1]
    nchan = f["data_frequency"].shape[0]
    npol = vis.visibility_acc.npol

    tmpvis = vis["vis"].data[timeslice, blslice, chanslice, ...]
    for ipol in range(npol):
        this_pol_group = f.create_group(f"data_vis_pol-{ipol}")

        for ichan in range(nchan):
            this_chan_pol_group = this_pol_group.create_group(f"data_vis_chan-{ichan}")

            this_chan_pol_group.create_dataset(
                "data_vis_real",
                (ntimes, nbaselines),
                data=tmpvis[:, :, ichan, ipol].real,
                dtype="float64",
            )

            this_chan_pol_group.create_dataset(
                "data_vis_imag",
                (ntimes, nbaselines),
                data=tmpvis[:, :, ichan, ipol].imag,
                dtype="float64",
            )

    return f


def save_visibility_to_hdf5(
    vis,
    filename,
    timeslice=slice(None, None),
    blslice=slice(None, None),
    chanslice=slice(None, None),
):
    """save visibility to hdf5 with a new format,
    it's different from export_visibility_from_hdf5

    :param vis: vis or vis list
    :param filename: hdf5 filename
    :param timeslice: slice times,it can be a numpy array, defaults to slice(None,None)
    :param blslice: baseline slice,alse can be numpy array, defaults to slice(None,None)
    :param chanslice: channel slice, defaults to slice(None,None)
    """
    f = h5py.File(filename, "w")
    if isinstance(vis, list):
        for i, v in enumerate(vis):
            vf = f.create_group("Visibility%d" % i)
            _save_vis_to_hdf5(v, vf, timeslice, blslice, chanslice)
    else:
        vf = f.create_group("Visibility%d" % 0)
        _save_vis_to_hdf5(vis, vf, timeslice, blslice, chanslice)
    f.flush()
    f.close()


def _slice_to_range(aslice, allen):
    return range(aslice.start or 0, aslice.stop or allen, aslice.step or 1)


def _create_vis_from_hdf5(f, timeslice=None, blslice=None, chanslice=None):
    """create visibility from hdf5 group

    :param f: hdf5 file object or group
    :param timeslice: slice times,it can be a numpy array, defaults to slice(None,None)
    :param blslice: baseline slice,alse can be numpy array, defaults to slice(None,None)
    :param chanslice: channel slice, defaults to slice(None,None)
    :raises ValueError: only supper autocorrelations or crosscorrelations
    :return: vis
    """

    s = f.attrs["phasecentre_coords"].split()
    ss = [float(s[0]), float(s[1])] * u.deg
    phasecentre = SkyCoord(ra=ss[0], dec=ss[1], frame=f.attrs["phasecentre_frame"])
    polarisation_frame = PolarisationFrame(f.attrs["polarisation_frame"])
    source = f.attrs["source"]
    nants = f.attrs["nants"]
    meta = ast.literal_eval(f.attrs["meta"])

    nbaseline = f["data_flags"].shape[1]
    assert f.attrs["rascil_data_model"] == "Visibility", "Not a Visibility"

    if nbaseline == int(nants * (nants - 1) / 2):
        ifauto = False
    elif nbaseline == int(nants * (nants + 1) / 2):
        ifauto = True
    else:
        raise ValueError(f"unsupport format,nant={nants},baseline={nbaseline}")

    time = f["data_time"][timeslice]
    integration_time = f["data_integration_time"][timeslice]
    frequency = f["data_frequency"][chanslice]
    channel_bandwidth = f["data_channel_bandwidth"][chanslice]

    uvw = f["data_uvw"][timeslice, blslice, :]

    flags = f["data_flags"][timeslice, blslice, chanslice, ...].astype("int")

    weight = f["data_weight"][timeslice, blslice, ...]
    # TODO add weight spectrum support
    assert weight.shape[2] == 1
    weight = numpy.repeat(weight, repeats=channel_bandwidth.shape[0], axis=2)

    if ifauto:
        baselines = pandas.MultiIndex.from_tuples(
            generate_baselines(nants), names=("antenna1", "antenna2")
        )
    else:
        baselines = pandas.MultiIndex.from_tuples(
            generate_baselines_no_auto(nants), names=("antenna1", "antenna2")
        )
    baselines = baselines[blslice]

    _, _, _, npol = flags.shape
    vis_real = numpy.empty(shape=flags.shape, dtype="float")
    vis_imag = numpy.empty(shape=flags.shape, dtype="float")
    for ipol in range(npol):
        for visich, gpichan in enumerate(
            _slice_to_range(chanslice, f["data_frequency"].shape[0])
        ):
            this_pol_chan_gp = f[f"data_vis_pol-{ipol}/data_vis_chan-{gpichan}"]
            vis_real[:, blslice, visich, ipol] = this_pol_chan_gp["data_vis_real"][
                timeslice, blslice
            ]
            vis_imag[:, blslice, visich, ipol] = this_pol_chan_gp["data_vis_imag"][
                timeslice, blslice
            ]

    vis = vis_real + 1j * vis_imag

    vis = Visibility.constructor(
        vis=vis,
        time=time,
        uvw=uvw,
        integration_time=integration_time,
        frequency=frequency,
        weight=weight,
        imaging_weight=weight,
        flags=flags,
        baselines=baselines,
        polarisation_frame=polarisation_frame,
        phasecentre=phasecentre,
        channel_bandwidth=channel_bandwidth,
        source=source,
        meta=meta,
        configuration=convert_configuration_from_hdf(f),
    )
    return vis


def create_visibility_from_hdf5(
    filename,
    selected_vis=slice(None, None),
    timeslice=slice(None, None),
    blslice=slice(None, None),
    chanslice=slice(None, None),
):
    """create visibility from hdf5 file with a new format
     it's different from import_visibility_from_hdf5

    :param filename: hdf5 filename
    :param selected_vis: selected visibility group, defaults to slice(None,None)
    :param timeslice: slice times,it can be a numpy array, defaults to slice(None,None)
    :param blslice: baseline slice,alse can be numpy array, defaults to slice(None,None)
    :param chanslice: channel slice, defaults to slice(None,None)
    :return: vis or vis list
    """
    f = h5py.File(filename, "r")
    vis_group_list = list(f.keys())
    vis_group_list.sort(key=lambda x: int(x.replace("Visibility", "")))
    vis_group_list = numpy.array(vis_group_list)

    vislist = [
        _create_vis_from_hdf5(f[vis_group], timeslice, blslice, chanslice)
        for vis_group in vis_group_list[selected_vis]
    ]
    f.close()
    if len(vislist) == 1:
        return vislist[0]
    else:
        return vislist


def convert_visibility_from_ms_to_hdf5(
    msname,
    filename="test.hdf5",
    vis_time_step=1,
    weight_time_step=1,
    flags_time_step=1,
    ack=False,
    datacolumn="DATA",
    selected_sources=None,
    selected_dds=None,
    ifauto=False,
    ifregularize_table=False,
    filled_tmp_table_prefix=None,
):
    """this method can faster convert MS to new hdf5 pattern than row loop

    TODO:this scipt needs to be optimized to adapt to NOT reguar MS file

    :param msname: MS file
    :param filename: hdf5 file, defaults to "test.hdf5"
    :param vis_time_step: size of buffer (vis_time_step,nbaseline,nchan,npol), defaults to 1
    :param weight_time_step: size of buffer (weight_time_step,nbaseline,nchan,npol), defaults to 1
    :param flags_time_step: size of buffer (flags_time_step,nbaseline,nchan,npol), defaults to 1
    :param ack: Ask casacore to acknowledge each table operation, defaults to False
    :param datacolumn: MS data column to read DATA, CORRECTED_DATA, or MODEL_DATA, defaults to "DATA"
    :param selected_sources: Sources to select, defaults to None
    :param selected_dds: Data descriptors to select, defaults to None
    :param ifauto: if include autocorrelations , defaults to False
    :param ifregularize_table: if include regularized table, defaults to False
    :param filled_tmp_table_file: filled_tmp_table_file, defaults to None
    :return: None
    """

    if ifregularize_table:
        assert filled_tmp_table_prefix is not None, "filled_tmp_table_file is required"
        assert filled_tmp_table_prefix != "", "filled_tmp_table_file is required"
        uuid_path = str(uuid.uuid4())
        filled_tmp_table_prefix = filled_tmp_table_prefix + "/" + uuid_path
        if os.path.exists(filled_tmp_table_prefix):
            shutil.rmtree(filled_tmp_table_prefix)
        os.makedirs(filled_tmp_table_prefix)

    import time as pytime

    betime = pytime.time()
    try:
        from casacore.tables import table  # pylint: disable=import-error
    except ModuleNotFoundError:
        raise ModuleNotFoundError("casacore is not installed")
    try:
        from rascil2.processing_components.io import msv2
    except ModuleNotFoundError:
        raise ModuleNotFoundError("cannot import msv2")

    tab = table(msname, ack=ack)

    if selected_sources is None:
        fields = numpy.unique(tab.getcol("FIELD_ID"))
    else:
        fieldtab = table("%s/FIELD" % msname, ack=False)
        sources = fieldtab.getcol("NAME")
        fields = list()
        for field, source in enumerate(sources):
            if source in selected_sources:
                fields.append(field)
        assert len(fields) > 0, "No sources selected"

    if selected_dds is None:
        dds = numpy.unique(tab.getcol("DATA_DESC_ID"))
    else:
        dds = selected_dds

    full_hdf_file = h5py.File(filename, "w")

    visid = 0
    for field in fields:
        ftab = table(msname, ack=ack).query("FIELD_ID==%d" % field, style="")
        assert ftab.nrows() > 0, "Empty selection for FIELD_ID=%d" % (field)

        for dd in dds:
            hdf_file_group = full_hdf_file.create_group(f"Visibility{visid}")

            # Now get info from the subtables
            ddtab = table("%s/DATA_DESCRIPTION" % msname, ack=False)
            spwid = ddtab.getcol("SPECTRAL_WINDOW_ID")[dd]
            polid = ddtab.getcol("POLARIZATION_ID")[dd]
            ddtab.close()

            meta = {"MSV2": {"FIELD_ID": field, "DATA_DESC_ID": dd}}

            if ifregularize_table:
                anttab = table("%s/ANTENNA" % msname, ack=False)
                names = numpy.array(anttab.getcol("NAME"))
                names = numpy.array(anttab.getcol("NAME"))[names != ""]
                nants = len(names)
                ms = get_regularized_table(
                    tab,
                    field,
                    dd,
                    ifauto,
                    nants,
                    f"{filled_tmp_table_prefix}/ms-field-{field}-dd-{dd}.tmp",
                )
            else:
                ms = ftab.query("DATA_DESC_ID==%d" % dd, style="")
            assert (
                ms.nrows() > 0
            ), "Empty selection for FIELD_ID=%d and DATA_DESC_ID=%d" % (field, dd)

            otime = ms.getcol("TIME")
            log.info("ms.getcol TIME: %s", str(pytime.time() - betime))
            datacol = ms.getcol(datacolumn, nrow=1)
            channels = datacol.shape[-2]

            channum = range(channels)

            uvw = -1 * ms.getcol("UVW")
            integration_time = ms.getcol("INTERVAL")
            log.info("ms.getcol UVW and INTERVAL: %s", str(pytime.time() - betime))
            time = otime - integration_time / 2.0

            timeline = numpy.unique(time)
            ntimes = len(timeline)
            log.info("otime unique: %s", str(pytime.time() - betime))

            spwtab = table("%s/SPECTRAL_WINDOW" % msname, ack=False)
            cfrequency = numpy.array(spwtab.getcol("CHAN_FREQ")[spwid][channum])
            cchannel_bandwidth = numpy.array(
                spwtab.getcol("CHAN_WIDTH")[spwid][channum]
            )
            nchan = cfrequency.shape[0]

            npol, polarisation_frame = _get_polarisation_info(msname, polid)

            # Get configuration
            anttab = table("%s/ANTENNA" % msname, ack=False)
            names = numpy.array(anttab.getcol("NAME"))
            mount = numpy.array(anttab.getcol("MOUNT"))[names != ""]
            diameter = numpy.array(anttab.getcol("DISH_DIAMETER"))[names != ""]
            xyz = numpy.array(anttab.getcol("POSITION"))[names != ""]
            offset = numpy.array(anttab.getcol("OFFSET"))[names != ""]
            stations = numpy.array(anttab.getcol("STATION"))[names != ""]
            names = numpy.array(anttab.getcol("NAME"))[names != ""]
            nants = len(names)

            nants_in_main_table = numpy.unique(
                [ms.getcol("ANTENNA1"), ms.getcol("ANTENNA2")]
            ).shape[0]
            assert nants_in_main_table == nants, "nants_in_main_table!= nants in anttab"
            ntime_in_main_table = ntimes
            nrow_main_table = ms.nrows()

            if (
                nrow_main_table
                == ntime_in_main_table
                * nants_in_main_table
                * (nants_in_main_table - 1)
                / 2
            ):
                nbaselines = int(nants * (nants - 1) / 2)
                assert ifauto is False
            elif (
                nrow_main_table
                == ntime_in_main_table
                * nants_in_main_table
                * (nants_in_main_table + 1)
                / 2
            ):
                nbaselines = int(nants * (nants + 1) / 2)
                assert ifauto is True
            else:
                raise ValueError(
                    "unsupported shape: nrows: %d, ntimes: %d, nants: %d"
                    % (nrow_main_table, ntime_in_main_table, nants_in_main_table)
                )

            assert (time.reshape(ntimes, nbaselines)[:, 0] == timeline).all()
            log.info("assert time.reshape == time.unique")

            log.info(
                "nbaselines,nants,ntimes,npol: %d,%d,%d,%d",
                nbaselines,
                nants,
                ntimes,
                npol,
            )

            location = EarthLocation(
                x=Quantity(xyz[0][0], "m"),
                y=Quantity(xyz[0][1], "m"),
                z=Quantity(xyz[0][2], "m"),
            )

            configuration = Configuration.constructor(
                name="",
                location=location,
                names=names,
                xyz=xyz,
                mount=mount,
                frame="ITRF",
                receptor_frame=ReceptorFrame("linear"),
                diameter=diameter,
                offset=offset,
                stations=stations,
            )
            hdf_file_group = convert_configuration_to_hdf(configuration, hdf_file_group)
            full_hdf_file.flush()

            # Get phasecentres
            fieldtab = table("%s/FIELD" % msname, ack=False)
            pc = fieldtab.getcol("PHASE_DIR")[field, 0, :]
            source = fieldtab.getcol("NAME")[field]
            phasecentre = SkyCoord(
                ra=pc[0] * u.rad, dec=pc[1] * u.rad, frame="icrs", equinox="J2000"
            )

            # attrs
            hdf_file_group.attrs["rascil_data_model"] = "Visibility"
            hdf_file_group.attrs["nants"] = nants
            hdf_file_group.attrs["nvis"] = numpy.prod([ntimes, nbaselines, nchan, npol])
            hdf_file_group.attrs["npol"] = npol
            hdf_file_group.attrs["phasecentre_coords"] = phasecentre.to_string()
            hdf_file_group.attrs["phasecentre_frame"] = phasecentre.frame.name
            hdf_file_group.attrs["polarisation_frame"] = polarisation_frame.type
            hdf_file_group.attrs["source"] = source
            hdf_file_group.attrs["meta"] = str(meta)

            # freq,bw
            hdf_file_group["data_frequency"] = cfrequency
            hdf_file_group["data_channel_bandwidth"] = cchannel_bandwidth

            # uvw
            hdf_file_group["data_uvw"] = uvw.reshape(ntimes, nbaselines, 3)

            # time,intergration_time
            hdf_file_group["data_time"] = timeline
            hdf_file_group["data_integration_time"] = integration_time.reshape(
                ntimes, nbaselines
            )[:, 0]

            log.info(
                "start writing vis flag weight..... %s", str(pytime.time() - betime)
            )

            # ----------------vis--------------------
            for ipol in range(npol):
                this_pol_group = hdf_file_group.create_group(f"data_vis_pol-{ipol}")

                for ichan in range(nchan):
                    this_chan_pol_group = this_pol_group.create_group(
                        f"data_vis_chan-{ichan}"
                    )

                    this_chan_pol_group.create_dataset(
                        "data_vis_real",
                        (ntimes, nbaselines),
                        dtype="float64",
                    )

                    this_chan_pol_group.create_dataset(
                        "data_vis_imag",
                        (ntimes, nbaselines),
                        dtype="float64",
                    )

            step = vis_time_step
            for idx, itime in enumerate(range(0, ntimes, step)):
                sttt = pytime.time()
                buffer_size = step * nbaselines
                buffer_start = idx * buffer_size
                buffer_vis = ms.getcol(datacolumn, buffer_start, nrow=buffer_size)[
                    :, channum, :
                ].reshape(step, nbaselines, nchan, npol)
                real = buffer_vis.real
                imag = buffer_vis.imag
                msreadttt = pytime.time()

                ttsl = slice(itime, itime + step)

                for ipol in range(npol):
                    for ichan in range(nchan):
                        this_chan_pol_group = hdf_file_group[
                            f"data_vis_pol-{ipol}/data_vis_chan-{ichan}"
                        ]
                        this_chan_pol_group["data_vis_real"][ttsl, :] = real[
                            :, :, ichan, ipol
                        ]
                        this_chan_pol_group["data_vis_imag"][ttsl, :] = imag[
                            :, :, ichan, ipol
                        ]

                log.info(
                    "writing VIS: timeid: %d, buffershape: %s, costms: %.2f, costloop: %.2f",
                    itime,
                    str(buffer_vis.shape),
                    msreadttt - sttt,
                    pytime.time() - sttt,
                )
            log.info("END VIS: cost: %s", str(pytime.time() - betime))

            # ------------------vis-end-----------------------

            hdf_file_group.create_dataset(
                "data_flags", (ntimes, nbaselines, nchan, npol), dtype="bool"
            )
            step = flags_time_step
            for idx, itime in enumerate(range(0, ntimes, step)):
                sttt = pytime.time()
                buffer_size = step * nbaselines
                buffer_start = idx * buffer_size
                buffer_flags = (
                    ms.getcol("FLAG", buffer_start, nrow=buffer_size)[:, channum, :]
                    .astype("bool")
                    .reshape(step, nbaselines, nchan, npol)
                )
                msreadttt = pytime.time()

                ttsl = slice(itime, itime + step)
                hdf_file_group["data_flags"][ttsl, :, :, :] = buffer_flags

                log.info(
                    "writing FLAGS: timeid: %d, buffershape: %s, costms: %.2f, costloop: %.2f",
                    itime,
                    str(buffer_flags.shape),
                    msreadttt - sttt,
                    pytime.time() - sttt,
                )
            log.info("END FLAGS: cost: %s", str(pytime.time() - betime))

            hdf_file_group.create_dataset(
                "data_weight", (ntimes, nbaselines, 1, npol), dtype="float64"
            )
            step = weight_time_step
            for idx, itime in enumerate(range(0, ntimes, step)):
                sttt = pytime.time()
                buffer_size = step * nbaselines
                buffer_start = idx * buffer_size
                buffer_weight = ms.getcol(
                    "WEIGHT", buffer_start, nrow=buffer_size
                ).reshape(step, nbaselines, 1, npol)
                msreadttt = pytime.time()

                ttsl = slice(itime, itime + step)
                hdf_file_group["data_weight"][ttsl, :, :, :] = buffer_weight

                log.info(
                    "writing WEIGHT: timeid: %d, buffershape: %s, costms: %.2f, costloop: %.2f",
                    itime,
                    str(buffer_weight.shape),
                    msreadttt - sttt,
                    pytime.time() - sttt,
                )
            log.info("END WEIGHT: cost: %s", str(pytime.time() - betime))

            full_hdf_file.flush()
            visid += 1

    tab.close()
    full_hdf_file.close()
    log.info("ALL DONE! cost: %.2f", pytime.time() - betime)
