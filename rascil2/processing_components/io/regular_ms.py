"""
regularisation visibility io module based on python-casacore table
"""

from casacore.tables import table, tableutil
import numpy
import pandas
import os
import shutil
import glob
import uuid
from astropy import units as u, constants as constants
from astropy.coordinates import SkyCoord, EarthLocation
from astropy.units import Quantity
from rascil2.processing_components.io.operations import STOKES_CODES
from rascil2.processing_components.io.base import _get_polarisation_info
from rascil2.data_models.memory_data_models import Visibility, Configuration
from rascil2.data_models.polarisation import (
    ReceptorFrame,
)
from rascil2.processing_components.visibility.base import (
    generate_baselines,
    generate_baselines_no_auto,
)

__all__ = [
    "export_regularisation_visibility_to_ms",
    "get_regularized_table",
    "create_visibility_from_ms_regularisation",
]


def get_main_desc(nchan, ncorr, notiled=True):
    """
    Get the main description of the table.

    :param nchan: Number of channels
    :param ncorr: Number of correlations
    :return: Main description
    """
    col1 = tableutil.makearrcoldesc(
        "UVW",
        0.0,
        1,
        shape=[3],
        options=0,
        comment="Vector with uvw coordinates (in meters)",
        datamanagergroup="UVW" if notiled else "TiledUVW",
        datamanagertype="TiledColumnStMan",
        keywords={
            "QuantumUnits": ["m", "m", "m"],
            "MEASINFO": {"type": "uvw", "Ref": "ITRF"},
        },
    )
    col2 = tableutil.makearrcoldesc(
        "FLAG",
        False,
        2,
        shape=[nchan, ncorr] if notiled else [],
        options=4 if notiled else 0,
        datamanagertype="TiledColumnStMan" if notiled else "TiledShapeStMan",
        datamanagergroup="Data" if notiled else "TiledFlag",
        comment="The data flags, array of bools with same shape as data",
    )
    col3 = tableutil.makearrcoldesc(
        "FLAG_CATEGORY",
        False,
        3,
        shape=[1, nchan, ncorr] if notiled else [],
        options=4 if notiled else 0,
        datamanagertype="TiledColumnStMan" if notiled else "TiledShapeStMan",
        datamanagergroup="FlagCategory" if notiled else "TiledFlagCategory",
        comment="The flag category, NUM_CAT flags for each datum",
        keywords={
            "CATEGORY": [
                "",
            ]
        },
    )
    col4 = tableutil.makearrcoldesc(
        "WEIGHT",
        1.0,
        1,
        shape=[ncorr] if notiled else [],
        datamanagertype="TiledColumnStMan" if notiled else "TiledShapeStMan",
        datamanagergroup="Weight" if notiled else "TiledWgt",
        valuetype="float",
        comment="Weight for each polarization spectrum",
    )
    col_4_5 = tableutil.makearrcoldesc(
        "WEIGHT_SPECTRUM",
        1.0,
        2,
        shape=[nchan, ncorr] if notiled else [],
        options=4 if notiled else 0,
        datamanagertype="TiledColumnStMan" if notiled else "TiledShapeStMan",
        datamanagergroup="WeightSpectrum" if notiled else "TiledWgtSpectrum",
        valuetype="float",
        comment="Weight for each polarization and channel spectrum",
    )
    col5 = tableutil.makearrcoldesc(
        "SIGMA",
        9999.0,
        1,
        shape=[ncorr] if notiled else [],
        options=4 if notiled else 0,
        datamanagertype="TiledColumnStMan" if notiled else "TiledShapeStMan",
        datamanagergroup="Sigma" if notiled else "TiledSigma",
        valuetype="float",
        comment="Estimated rms noise for channel with unity bandpass response",
    )
    col6 = tableutil.makescacoldesc(
        "ANTENNA1", 0, comment="ID of first antenna in interferometer"
    )
    col7 = tableutil.makescacoldesc(
        "ANTENNA2", 0, comment="ID of second antenna in interferometer"
    )
    col8 = tableutil.makescacoldesc(
        "ARRAY_ID",
        0,
        datamanagertype="IncrementalStMan",
        comment="ID of array or subarray",
    )
    col9 = tableutil.makescacoldesc(
        "DATA_DESC_ID",
        0,
        datamanagertype="IncrementalStMan",
        comment="The data description table index",
    )
    col10 = tableutil.makescacoldesc(
        "EXPOSURE",
        0.0,
        comment="The effective integration time",
        keywords={
            "QuantumUnits": [
                "s",
            ]
        },
    )
    col11 = tableutil.makescacoldesc(
        "FEED1",
        0,
        datamanagertype="IncrementalStMan",
        comment="The feed index for ANTENNA1",
    )
    col12 = tableutil.makescacoldesc(
        "FEED2",
        0,
        datamanagertype="IncrementalStMan",
        comment="The feed index for ANTENNA2",
    )
    col13 = tableutil.makescacoldesc(
        "FIELD_ID",
        0,
        datamanagertype="IncrementalStMan",
        comment="Unique id for this pointing",
    )
    col14 = tableutil.makescacoldesc(
        "FLAG_ROW",
        False,
        datamanagertype="IncrementalStMan",
        comment="Row flag - flag all data in this row if True",
    )
    col15 = tableutil.makescacoldesc(
        "INTERVAL",
        0.0,
        datamanagertype="IncrementalStMan",
        comment="The sampling interval",
        keywords={
            "QuantumUnits": [
                "s",
            ]
        },
    )
    col16 = tableutil.makescacoldesc(
        "OBSERVATION_ID",
        0,
        datamanagertype="IncrementalStMan",
        comment="ID for this observation, index in OBSERVATION table",
    )
    col17 = tableutil.makescacoldesc(
        "PROCESSOR_ID",
        -1,
        datamanagertype="IncrementalStMan",
        comment="Id for backend processor, index in PROCESSOR table",
    )
    col18 = tableutil.makescacoldesc(
        "SCAN_NUMBER",
        1,
        datamanagertype="IncrementalStMan",
        comment="Sequential scan number from on-line system",
    )
    col19 = tableutil.makescacoldesc(
        "STATE_ID",
        -1,
        datamanagertype="IncrementalStMan",
        comment="ID for this observing state",
    )
    col20 = tableutil.makescacoldesc(
        "TIME",
        0.0,
        comment="Modified Julian Day",
        datamanagertype="IncrementalStMan",
        keywords={
            "QuantumUnits": [
                "s",
            ],
            "MEASINFO": {"type": "epoch", "Ref": "UTC"},
        },
    )
    col21 = tableutil.makescacoldesc(
        "TIME_CENTROID",
        0.0,
        comment="Modified Julian Day",
        datamanagertype="IncrementalStMan",
        keywords={
            "QuantumUnits": [
                "s",
            ],
            "MEASINFO": {"type": "epoch", "Ref": "UTC"},
        },
    )
    col22 = tableutil.makearrcoldesc(
        "DATA",
        0j,
        2,
        shape=[nchan, ncorr] if notiled else [],
        options=4 if notiled else 0,
        valuetype="complex",
        keywords={"UNIT": "Jy"},
        datamanagertype="TiledColumnStMan" if notiled else "TiledShapeStMan",
        datamanagergroup="Data" if notiled else "TiledDATA",
        comment="The data column",
    )

    desc = tableutil.maketabdesc(
        [
            col1,
            col2,
            col3,
            col4,
            col_4_5,
            col5,
            col6,
            col7,
            col8,
            col9,
            col10,
            col11,
            col12,
            col13,
            col14,
            col15,
            col16,
            col17,
            col18,
            col19,
            col20,
            col21,
            col22,
        ]
    )
    return desc


def init_main_table(vis_list, ms_name, if_delete=True):
    """
    Initialize main table for MS
    :param vis_list: list of dictionaries
    :param ms_name: name of MS
    :param if_delete: delete existing MS if True
    """
    if os.path.exists(ms_name):
        if if_delete:
            shutil.rmtree(ms_name, ignore_errors=False)
        else:
            raise IOError("File '%s' already exists" % ms_name)

    notiled = (
        numpy.unique(
            numpy.array([vis.frequency.data.shape[0] for vis in vis_list])
        ).shape
        == 1
    )

    _, _, nchan, npol = vis_list[0]["vis"].shape
    maintable = table(
        "%s" % ms_name, get_main_desc(nchan, npol, notiled), nrow=0, ack=False
    )
    maintable.flush()
    maintable.close()


def put_main_table(vis_list, ms_name, visid_to_fieldid, visid_to_ddsid):
    """
    Put main table for MS
    :param vis_list: list of dictionaries
    :param ms_name: name of MS
    :param visid_to_fieldid: dictionary mapping visid to fieldid
    :param visid_to_ddsid: dictionary mapping visid to ddsid
    """

    # main table
    haswrite_row = 0
    for visid, vis in enumerate(vis_list):
        field_id = visid_to_fieldid[visid]
        dds_id = visid_to_ddsid[visid]

        ntimes, nbaseline, nchan, npol = vis["vis"].shape
        # maindesc = get_main_desc(nchan, npol)
        # maintable = table("%s" % ms_name, maindesc, nrow=0, ack=False)
        maintable = table("%s" % ms_name, ack=False, readonly=False)
        nrow = ntimes * nbaseline
        maintable.addrows(nrow)

        maintable.putcol("UVW", -vis["uvw"].data.reshape(-1, 3), haswrite_row, nrow)
        maintable.putcol(
            "FLAG", vis["flags"].data.reshape(-1, nchan, npol), haswrite_row, nrow
        )
        maintable.putcol(
            "FLAG_CATEGORY",
            numpy.zeros((nrow, 1, nchan, npol), dtype=bool),
            haswrite_row,
            nrow,
        )
        maintable.putcol(
            "WEIGHT",
            numpy.average(vis["weight"].data.reshape(-1, nchan, npol), axis=1),
            haswrite_row,
            nrow,
        )
        maintable.putcol(
            "WEIGHT_SPECTRUM",
            vis["weight"].data.reshape(-1, nchan, npol),
            haswrite_row,
            nrow,
        )
        maintable.putcol("SIGMA", numpy.full((nrow, npol), 9999), haswrite_row, nrow)

        baseline_array = numpy.array(vis.baselines.data.tolist())[None, :, :]
        baseline_col = numpy.repeat(baseline_array, ntimes, axis=0).reshape(-1, 2)
        maintable.putcol("ANTENNA1", baseline_col[:, 0], haswrite_row, nrow)
        maintable.putcol("ANTENNA2", baseline_col[:, 1], haswrite_row, nrow)

        maintable.putcol("ARRAY_ID", numpy.zeros((nrow,)), haswrite_row, nrow)
        maintable.putcol(
            "DATA_DESC_ID", numpy.full((nrow,), dds_id), haswrite_row, nrow
        )

        inttime_array = vis["integration_time"].data
        inttime_col = inttime_array[:, None].repeat(nbaseline, axis=1).ravel()
        maintable.putcol("EXPOSURE", inttime_col, haswrite_row, nrow)

        maintable.putcol("FEED1", numpy.zeros((nrow,)), haswrite_row, nrow)
        maintable.putcol("FEED2", numpy.zeros((nrow,)), haswrite_row, nrow)

        maintable.putcol("FIELD_ID", numpy.full((nrow,), field_id), haswrite_row, nrow)

        maintable.putcol("FLAG_ROW", numpy.full((nrow,), False), haswrite_row, nrow)

        maintable.putcol("INTERVAL", inttime_col, haswrite_row, nrow)

        maintable.putcol("OBSERVATION_ID", numpy.zeros((nrow,)), haswrite_row, nrow)
        maintable.putcol("PROCESSOR_ID", numpy.full((nrow,), -1), haswrite_row, nrow)
        maintable.putcol("SCAN_NUMBER", numpy.full((nrow,), visid), haswrite_row, nrow)
        maintable.putcol("STATE_ID", numpy.full((nrow,), -1), haswrite_row, nrow)

        time_array = vis["time"].data + inttime_array / 2.0
        time_col = time_array[:, None].repeat(nbaseline, axis=1).ravel()
        maintable.putcol("TIME", time_col, haswrite_row, nrow)
        maintable.putcol("TIME_CENTROID", time_col, haswrite_row, nrow)

        maintable.putcol(
            "DATA", vis["vis"].data.reshape(-1, nchan, npol), haswrite_row, nrow
        )
        maintable.flush()
        haswrite_row += nrow
    maintable.close()


def freq_in_list(freq_array, freq_array_list):
    """
    Check if a frequency array is in a list of frequency arrays.

    :param freq_array: numpy.ndarray
        Frequency array.
    :param freq_array_list: list
        List of frequency arrays.
    """
    isin = False
    for freq in freq_array_list:
        # if freq.shape == freq_array.shape we can use numpy.allclose
        if freq.shape == freq_array.shape:
            if numpy.allclose(freq, freq_array):
                isin = True
                break
    return isin


def radec_in_list(radec, ph_list):
    """
    Check if a given RA/Dec pair is in a list of polarizations.

    :param radec: astropy.coordinates.SkyCoord
        RA/Dec pair.
    :param ph_list: list
        List of polarizations.
    """
    isin = False
    for ph in ph_list:
        if numpy.isclose(radec.ra.value, ph.ra.value) and numpy.isclose(
            radec.dec.value, ph.dec.value
        ):
            isin = True
            break
    return isin


def pol_in_list(pol, pol_list):
    """
    Check if a polarization is in a list of polarizations.

    :param pol: str
        Name of the polarization.
    :param pol_list: list
        List of polarizations.
    :return: bool
        True if the polarization is in the list of polarizations.

    """
    isin = False
    for pol_str in pol_list:
        if pol_str == pol:
            isin = True
            break
    return isin


def source_in_list(source, source_list):
    """
    Return True if the source is in the list of sources.

    :param source: str
        Name of the source.
    :param source_list: list
        List of sources.
    :return: bool
        True if the source is in the list of sources.

    """
    isin = False
    for source_str in source_list:
        if source_str == source:
            isin = True
            break
    return isin


def index_list(thinglist, cot, mode="str"):
    """
    Return the index of a thing in a list.
    :param thinglist: list
        List of things.
    :param cot: str or astropy.coordinates.SkyCoord
        The thing to look for.
    :param mode: str
        Mode to use for the search.
    :return: int
        Index of the thing in the list.
    """
    if mode == "str":
        return thinglist.index(cot)
    elif mode == "numpy":
        for idx, thing in enumerate(thinglist):
            if thing.shape == cot.shape:
                if numpy.allclose(thing, cot):
                    return idx
    elif mode == "radec":
        for idx, thing in enumerate(thinglist):
            if numpy.isclose(thing.ra.deg, cot.ra.deg) and numpy.isclose(
                thing.dec.deg, cot.dec.deg
            ):
                return idx
    else:
        raise ValueError("mode must be'str' or 'int'")


def put_data_description(vis_list, ms_name):
    """
    Write a list of visibility objects to a MS.
    :param vis_list: list of visibility objects
    :param ms_name: name of MS to write to
    :return:
    """
    col1 = tableutil.makescacoldesc("FLAG_ROW", False, comment="Flag this row")
    col2 = tableutil.makescacoldesc(
        "POLARIZATION_ID", 0, comment="Pointer to polarization table"
    )
    col3 = tableutil.makescacoldesc(
        "SPECTRAL_WINDOW_ID", 0, comment="Pointer to spectralwindow table"
    )

    desc = tableutil.maketabdesc([col1, col2, col3])
    tb = table("%s/DATA_DESCRIPTION" % ms_name, desc, nrow=0, ack=False)

    pol_list = []
    radec_list = []
    freq_list = []
    source_list = []
    for idx, vis in enumerate(vis_list):
        ifpolinlist = pol_in_list(_polarization(vis), pol_list)
        iffreqinlist = freq_in_list(vis.frequency.data, freq_list)

        if not ifpolinlist:
            pol_list.append(_polarization(vis))

        if not radec_in_list(vis.phasecentre, radec_list):
            radec_list.append(vis.phasecentre)

        if not iffreqinlist:
            freq_list.append(vis.frequency.data)

        if not source_in_list(vis.source, source_list):
            source_list.append(vis.source)

    visid_to_polid = {}
    visid_to_fieldid = {}
    visid_to_spwid = {}
    visid_to_sourceid = {}
    visid_to_ddsid = {}
    conbine_pol_spw = []
    for idx, vis in enumerate(vis_list):
        visid_to_polid[idx] = index_list(pol_list, _polarization(vis), mode="str")
        visid_to_fieldid[idx] = index_list(radec_list, vis.phasecentre, mode="radec")
        visid_to_spwid[idx] = index_list(freq_list, vis.frequency.data, mode="numpy")
        visid_to_sourceid[idx] = index_list(source_list, vis.source, mode="str")

        pol_id = visid_to_polid[idx]
        spw_id = visid_to_spwid[idx]
        if (pol_id, spw_id) in conbine_pol_spw:
            continue
        conbine_pol_spw.append((pol_id, spw_id))

    visid_to_ddsid = {}
    for idx, _ in enumerate(vis_list):
        pol_id = visid_to_polid[idx]
        spw_id = visid_to_spwid[idx]
        visid_to_ddsid[idx] = conbine_pol_spw.index((pol_id, spw_id))

    added_dds = []
    rowid = 0
    for idx, vis in enumerate(vis_list):
        pol_id = visid_to_polid[idx]
        spw_id = visid_to_spwid[idx]
        dds_id = visid_to_ddsid[idx]

        if dds_id in added_dds:
            continue
        tb.addrows(1)
        tb.putcell("FLAG_ROW", dds_id, False)
        tb.putcell("POLARIZATION_ID", dds_id, pol_id)
        tb.putcell("SPECTRAL_WINDOW_ID", dds_id, spw_id)
        rowid += 1
        added_dds.append(dds_id)
    tb.flush()
    tb.close()

    return (
        visid_to_ddsid,
        visid_to_fieldid,
        visid_to_polid,
        visid_to_spwid,
        visid_to_sourceid,
    )


def put_antena_table(vis_list, ms_name):
    """
    Put antenna table in the table.

    :param vis_list: list of visibilities
    :param ms_name: name of the MS
    :return:
    """

    configuration = vis_list[0].configuration
    nants = configuration.configuration_acc.nants

    col1 = tableutil.makearrcoldesc(
        "OFFSET",
        0.0,
        1,
        comment="Axes offset of mount to FEED REFERENCE point",
        keywords={
            "QuantumUnits": ["m", "m", "m"],
            "MEASINFO": {"type": "position", "Ref": "ITRF"},
        },
    )
    col2 = tableutil.makearrcoldesc(
        "POSITION",
        0.0,
        1,
        comment="Antenna X,Y,Z phase reference position",
        keywords={
            "QuantumUnits": ["m", "m", "m"],
            "MEASINFO": {"type": "position", "Ref": "ITRF"},
        },
    )
    col3 = tableutil.makescacoldesc(
        "TYPE", "ground-based", comment="Antenna type (e.g. SPACE-BASED)"
    )
    col4 = tableutil.makescacoldesc(
        "DISH_DIAMETER",
        2.0,
        comment="Physical diameter of dish",
        keywords={
            "QuantumUnits": [
                "m",
            ]
        },
    )
    col5 = tableutil.makescacoldesc("FLAG_ROW", False, comment="Flag for this row")
    col6 = tableutil.makescacoldesc(
        "MOUNT", "alt-az", comment="Mount type e.g. alt-az, equatorial, etc."
    )
    col7 = tableutil.makescacoldesc(
        "NAME", "none", comment="Antenna name, e.g. VLA22, CA03"
    )
    col8 = tableutil.makescacoldesc(
        "STATION", "unkonwn", comment="Station (antenna pad) name"
    )

    desc = tableutil.maketabdesc([col1, col2, col3, col4, col5, col6, col7, col8])
    tb = table("%s/ANTENNA" % ms_name, desc, nrow=nants, ack=False)

    tb.putcol("OFFSET", configuration["offset"].data)
    tb.putcol("TYPE", ["GROUND-BASED"] * nants)
    tb.putcol("DISH_DIAMETER", configuration["diameter"].data)
    tb.putcol("FLAG_ROW", numpy.full((nants,), False))
    tb.putcol("MOUNT", configuration["mount"].data)
    tb.putcol("NAME", configuration["names"].data)
    tb.putcol("STATION", configuration["stations"].data)

    tb.putcol("POSITION", configuration["xyz"].data)
    tb.flush()
    tb.close()


def _polarization(vis):
    """
    Return the polarization of the visibility.
    :param vis:
    :return:
    """
    if vis.visibility_acc.polarisation_frame.type == "linear":
        polarization = ["XX", "XY", "YX", "YY"]
    elif vis.visibility_acc.polarisation_frame.type == "linearnp":
        polarization = ["XX", "YY"]
    elif vis.visibility_acc.polarisation_frame.type == "stokesI":
        polarization = ["I"]
    elif vis.visibility_acc.polarisation_frame.type == "circular":
        polarization = ["RR", "RL", "LR", "LL"]
    elif vis.visibility_acc.polarisation_frame.type == "circularnp":
        polarization = ["RR", "LL"]
    elif vis.visibility_acc.polarisation_frame.type == "stokesIQUV":
        polarization = ["I", "Q", "U", "V"]
    elif vis.visibility_acc.polarisation_frame.type == "stokesIQ":
        polarization = ["I", "Q"]
    elif vis.visibility_acc.polarisation_frame.type == "stokesIV":
        polarization = ["I", "V"]
    else:
        raise ValueError(
            "Unknown visibility polarisation %s"
            % (vis.visibility_acc.polarisation_frame.type)
        )
    return polarization


def put_polarization_table(vis_list, ms_name, visid_to_polid):
    """
    Put the polarization table into the MS.
    :param vis_list:
    :param ms_name:
    :param visid_to_polid:
    :return:
    """
    col1 = tableutil.makearrcoldesc(
        "CORR_TYPE",
        0,
        1,
        comment="The polarization type for each correlation product, as a Stokes enum.",
    )
    col2 = tableutil.makearrcoldesc(
        "CORR_PRODUCT",
        0,
        2,
        comment="Indices describing receptors of feed going into correlation",
    )
    col3 = tableutil.makescacoldesc("FLAG_ROW", False, comment="flag")
    col4 = tableutil.makescacoldesc(
        "NUM_CORR", -1, comment="Number of correlation products"
    )

    desc = tableutil.maketabdesc([col1, col2, col3, col4])
    tb = table("%s/POLARIZATION" % ms_name, desc, nrow=0, ack=False)

    areadys_polid_in_table = []
    rowid = 0
    for idx, vis in enumerate(vis_list):
        pol_id = visid_to_polid[idx]
        if pol_id in areadys_polid_in_table:
            continue
        tb.addrows(1)

        polarization = _polarization(vis)
        numric_pol = [STOKES_CODES[pol.upper()] for pol in polarization]
        stks = numpy.array(numric_pol)
        prds = numpy.zeros((2, len(polarization)), dtype=numpy.int32)
        for i, stk in enumerate(numric_pol):
            stks[i] = stk
            if stk > 4:
                prds[0, i] = ((stk - 1) % 4) / 2
                prds[1, i] = ((stk - 1) % 4) % 2
            else:
                prds[0, i] = 1
                prds[1, i] = 1
        tb.putcell("CORR_TYPE", pol_id, numpy.array(numric_pol))
        tb.putcell("CORR_PRODUCT", pol_id, prds.T)
        tb.putcell("FLAG_ROW", pol_id, False)
        tb.putcell("NUM_CORR", pol_id, len(polarization))
        rowid += 1
        areadys_polid_in_table.append(pol_id)
    tb.flush()
    tb.close()


def put_observation_table(vis_list, ms_name):
    """
    Puts the observation table for a list of visibilities.
    :param vis_list: list of visibilities
    :param ms_name: name of the measurement set
    :return:
    """
    configuration = vis_list[0].configuration
    configuration.name

    col1 = tableutil.makearrcoldesc(
        "TIME_RANGE",
        0.0,
        1,
        comment="Start and end of observation",
        keywords={
            "QuantumUnits": [
                "s",
            ],
            "MEASINFO": {"type": "epoch", "Ref": "UTC"},
        },
    )
    col2 = tableutil.makearrcoldesc("LOG", "none", 1, comment="Observing log")
    col3 = tableutil.makearrcoldesc("SCHEDULE", "none", 1, comment="Observing schedule")
    col4 = tableutil.makescacoldesc("FLAG_ROW", False, comment="Row flag")
    col5 = tableutil.makescacoldesc("OBSERVER", "ZASKY", comment="Name of observer(s)")
    col6 = tableutil.makescacoldesc(
        "PROJECT", "ZASKY", comment="Project identification string"
    )
    col7 = tableutil.makescacoldesc(
        "RELEASE_DATE",
        0.0,
        comment="Release date when data becomes public",
        keywords={
            "QuantumUnits": [
                "s",
            ],
            "MEASINFO": {"type": "epoch", "Ref": "UTC"},
        },
    )
    col8 = tableutil.makescacoldesc(
        "SCHEDULE_TYPE", "none", comment="Observing schedule type"
    )
    col9 = tableutil.makescacoldesc(
        "TELESCOPE_NAME",
        configuration.name,
        comment="Telescope Name (e.g. WSRT, VLBA)",
    )

    desc = tableutil.maketabdesc([col1, col2, col3, col4, col5, col6, col7, col8, col9])
    tb = table("%s/OBSERVATION" % ms_name, desc, nrow=1, ack=False)

    obs_time = []
    for vis in vis_list:
        obs_time.append(vis.time.data)
    obs_time = numpy.array(obs_time)
    tStart = numpy.max(obs_time)
    tStop = numpy.min(obs_time)

    tb.putcell("TIME_RANGE", 0, [tStart, tStop])
    tb.putcell("LOG", 0, "Not provided")
    tb.putcell("SCHEDULE", 0, "Not provided")
    tb.putcell("FLAG_ROW", 0, False)
    tb.putcell("OBSERVER", 0, "FENGWANG")
    tb.putcell("PROJECT", 0, "SKASIM")
    tb.putcell("RELEASE_DATE", 0, tStop)
    tb.putcell("SCHEDULE_TYPE", 0, "None")
    tb.putcell("TELESCOPE_NAME", 0, configuration.name)

    tb.flush()
    tb.close()


def put_source_table(vis_list, ms_name, visid_to_spwid, visid_to_sourceid):
    """
    Put the source table.
    :param vis_list: list of visibilities
    :param ms_name: name of the MS
    :param visid_to_spwid: dictionary of visid to spwid
    :param visid_to_sourceid: dictionary of visid to sourceid
    :return:
    """
    col1 = tableutil.makearrcoldesc(
        "DIRECTION",
        0.0,
        1,
        comment="Direction (e.g. RA, DEC).",
        keywords={
            "QuantumUnits": ["rad", "rad"],
            "MEASINFO": {"type": "direction", "Ref": "J2000"},
        },
    )
    col2 = tableutil.makearrcoldesc(
        "PROPER_MOTION",
        0.0,
        1,
        comment="Proper motion",
        keywords={
            "QuantumUnits": [
                "rad/s",
            ]
        },
    )
    col3 = tableutil.makescacoldesc(
        "CALIBRATION_GROUP",
        0,
        comment="Number of grouping for calibration purpose.",
    )
    col4 = tableutil.makescacoldesc(
        "CODE",
        "none",
        comment="Special characteristics of source, e.g. Bandpass calibrator",
    )
    col5 = tableutil.makescacoldesc(
        "INTERVAL",
        0.0,
        comment="Interval of time for which this set of parameters is accurate",
        keywords={
            "QuantumUnits": [
                "s",
            ]
        },
    )
    col6 = tableutil.makescacoldesc(
        "NAME", "none", comment="Name of source as given during observations"
    )
    col7 = tableutil.makescacoldesc("NUM_LINES", 0, comment="Number of spectral lines")
    col8 = tableutil.makescacoldesc("SOURCE_ID", 0, comment="Source id")
    col9 = tableutil.makescacoldesc(
        "SPECTRAL_WINDOW_ID", -1, comment="ID for this spectral window setup"
    )
    col10 = tableutil.makescacoldesc(
        "TIME",
        0.0,
        comment="Midpoint of time for which this set of parameters is accurate.",
        keywords={
            "QuantumUnits": [
                "s",
            ],
            "MEASINFO": {"type": "epoch", "Ref": "UTC"},
        },
    )
    col11 = tableutil.makearrcoldesc(
        "TRANSITION", "none", 1, comment="Line Transition name"
    )
    col12 = tableutil.makearrcoldesc(
        "REST_FREQUENCY",
        1.0,
        1,
        comment="Line rest frequency",
        keywords={
            "QuantumUnits": [
                "Hz",
            ],
            "MEASINFO": {"type": "frequency", "Ref": "LSRK"},
        },
    )
    col13 = tableutil.makearrcoldesc(
        "SYSVEL",
        1.0,
        1,
        comment="Systemic velocity at reference",
        keywords={
            "QuantumUnits": [
                "m/s",
            ],
            "MEASINFO": {"type": "radialvelocity", "Ref": "LSRK"},
        },
    )

    desc = tableutil.maketabdesc(
        [
            col1,
            col2,
            col3,
            col4,
            col5,
            col6,
            col7,
            col8,
            col9,
            col10,
            col11,
            col12,
            col13,
        ]
    )

    tb = table("%s/SOURCE" % ms_name, desc, nrow=0, ack=False)

    source_name_spwid_list = []
    source_num_id = 0
    rowid = 0
    for idx, vis in enumerate(vis_list):
        source_name = vis.source
        spw_id = visid_to_spwid[idx]
        if (source_name, spw_id) in source_name_spwid_list:
            continue
        obs_time = vis.time.data
        tStart = numpy.max(obs_time)
        tStop = numpy.min(obs_time)
        tb.addrows(1)
        phasecentre = vis.phasecentre
        ra = phasecentre.ra.rad
        dec = phasecentre.dec.rad
        tb.putcell("DIRECTION", source_num_id, [ra, dec])
        tb.putcell("PROPER_MOTION", source_num_id, [0.0, 0.0])
        tb.putcell("CALIBRATION_GROUP", source_num_id, 0)
        tb.putcell("CODE", source_num_id, "none")
        tb.putcell("INTERVAL", source_num_id, 0.0)
        tb.putcell("NAME", source_num_id, source_name)
        tb.putcell("NUM_LINES", source_num_id, 0)
        tb.putcell("SOURCE_ID", source_num_id, source_num_id)
        tb.putcell("SPECTRAL_WINDOW_ID", source_num_id, spw_id)
        tb.putcell("TIME", source_num_id, (tStart + tStop) / 2 * 86400)
        source_num_id += 1
        source_name_spwid_list.append((source_name, spw_id))
        rowid += 1
    tb.flush()
    tb.close()


def put_field_table(vis_list, ms_name, visid_to_sourceid, visid_to_fieldid):
    """
    Put the field table.
    :param vis_list: list of visibilities
    :param ms_name: name of the MS
    :param visid_to_sourceid: dictionary of visid to sourceid
    :param visid_to_fieldid: dictionary of visid to fieldid
    :return:
    """

    col1 = tableutil.makearrcoldesc(
        "DELAY_DIR",
        0.0,
        2,
        comment="Direction of delay center (e.g. RA, DEC)as polynomial in time.",
        keywords={
            "QuantumUnits": ["rad", "rad"],
            "MEASINFO": {"type": "direction", "Ref": "J2000"},
        },
    )
    col2 = tableutil.makearrcoldesc(
        "PHASE_DIR",
        0.0,
        2,
        comment="Direction of phase center (e.g. RA, DEC).",
        keywords={
            "QuantumUnits": ["rad", "rad"],
            "MEASINFO": {"type": "direction", "Ref": "J2000"},
        },
    )
    col3 = tableutil.makearrcoldesc(
        "REFERENCE_DIR",
        0.0,
        2,
        comment="Direction of REFERENCE center (e.g. RA, DEC).as polynomial in time.",
        keywords={
            "QuantumUnits": ["rad", "rad"],
            "MEASINFO": {"type": "direction", "Ref": "J2000"},
        },
    )
    col4 = tableutil.makescacoldesc(
        "CODE",
        "none",
        comment="Special characteristics of field, e.g. Bandpass calibrator",
    )
    col5 = tableutil.makescacoldesc("FLAG_ROW", False, comment="Row Flag")
    col6 = tableutil.makescacoldesc("NAME", "none", comment="Name of this field")
    col7 = tableutil.makescacoldesc(
        "NUM_POLY", 0, comment="Polynomial order of _DIR columns"
    )
    col8 = tableutil.makescacoldesc("SOURCE_ID", 0, comment="Source id")
    col9 = tableutil.makescacoldesc(
        "TIME",
        -1,
        comment="Time origin for direction and rate",
        keywords={
            "QuantumUnits": [
                "s",
            ],
            "MEASINFO": {"type": "epoch", "Ref": "UTC"},
        },
    )

    desc = tableutil.maketabdesc([col1, col2, col3, col4, col5, col6, col7, col8, col9])
    tb = table("%s/FIELD" % ms_name, desc, nrow=0, ack=False)

    added_field_list = []
    for idx, vis in enumerate(vis_list):
        source_name = vis.source
        phasecentre = vis.phasecentre
        ra = phasecentre.ra.value
        dec = phasecentre.dec.value
        field_id = visid_to_fieldid[idx]
        source_id = visid_to_sourceid[idx]
        if field_id in added_field_list:
            continue
        obs_time = vis.time.data
        tStart = numpy.max(obs_time)
        tStop = numpy.min(obs_time)
        tb.addrows(1)
        phasecentre = vis.phasecentre
        ra = phasecentre.ra.rad
        dec = phasecentre.dec.rad
        dir_array = numpy.array([[ra, dec]])
        tb.putcell("DELAY_DIR", field_id, dir_array)
        tb.putcell("PHASE_DIR", field_id, dir_array)
        tb.putcell("REFERENCE_DIR", field_id, dir_array)
        tb.putcell("CODE", field_id, "None")
        tb.putcell("FLAG_ROW", field_id, False)
        tb.putcell("NAME", field_id, source_name)
        tb.putcell("NUM_POLY", field_id, 0)
        tb.putcell("SOURCE_ID", field_id, source_id)
        tb.putcell("TIME", field_id, (tStart + tStop) / 2 * 86400)
        added_field_list.append(field_id)
    tb.flush()
    tb.close()


def put_spectralwindow_table(vis_list, ms_name, visid_to_spwid):
    """
    Put spectral window table into the table.
    :param vis_list: list of visibilities
    :param ms_name: name of the ms
    :param visid_to_spwid: dictionary of visid to spectral window id
    :return:
    """
    col1 = tableutil.makescacoldesc(
        "MEAS_FREQ_REF", 0, comment="Frequency Measure reference"
    )
    col2 = tableutil.makearrcoldesc(
        "CHAN_FREQ",
        0.0,
        1,
        comment="Center frequencies for each channel in the data matrix",
        keywords={
            "QuantumUnits": [
                "Hz",
            ],
            "MEASINFO": {
                "type": "frequency",
                "VarRefCol": "MEAS_FREQ_REF",
                "TabRefTypes": [
                    "REST",
                    "LSRK",
                    "LSRD",
                    "BARY",
                    "GEO",
                    "TOPO",
                    "GALACTO",
                    "LGROUP",
                    "CMB",
                    "Undefined",
                ],
                "TabRefCodes": [0, 1, 2, 3, 4, 5, 6, 7, 8, 64],
            },
        },
    )
    col3 = tableutil.makescacoldesc(
        "REF_FREQUENCY",
        -1,
        comment="The reference frequency",
        keywords={
            "QuantumUnits": [
                "Hz",
            ],
            "MEASINFO": {
                "type": "frequency",
                "VarRefCol": "MEAS_FREQ_REF",
                "TabRefTypes": [
                    "REST",
                    "LSRK",
                    "LSRD",
                    "BARY",
                    "GEO",
                    "TOPO",
                    "GALACTO",
                    "LGROUP",
                    "CMB",
                    "Undefined",
                ],
                "TabRefCodes": [0, 1, 2, 3, 4, 5, 6, 7, 8, 64],
            },
        },
    )
    col4 = tableutil.makearrcoldesc(
        "CHAN_WIDTH",
        0.0,
        1,
        comment="Channel width for each channel",
        keywords={
            "QuantumUnits": [
                "Hz",
            ]
        },
    )
    col5 = tableutil.makearrcoldesc(
        "EFFECTIVE_BW",
        0.0,
        1,
        comment="Effective noise bandwidth of each channel",
        keywords={
            "QuantumUnits": [
                "Hz",
            ]
        },
    )
    col6 = tableutil.makearrcoldesc(
        "RESOLUTION",
        0.0,
        1,
        comment="The effective noise bandwidth for each channel",
        keywords={
            "QuantumUnits": [
                "Hz",
            ]
        },
    )
    col7 = tableutil.makescacoldesc("FLAG_ROW", False, comment="flag")
    col8 = tableutil.makescacoldesc("FREQ_GROUP", 1, comment="Frequency group")
    col9 = tableutil.makescacoldesc(
        "FREQ_GROUP_NAME", "group1", comment="Frequency group name"
    )
    col10 = tableutil.makescacoldesc(
        "IF_CONV_CHAIN", 0, comment="The IF conversion chain number"
    )
    col11 = tableutil.makescacoldesc(
        "NAME", "%i channels" % -1, comment="Spectral window name"
    )
    col12 = tableutil.makescacoldesc("NET_SIDEBAND", 0, comment="Net sideband")
    col13 = tableutil.makescacoldesc(
        "NUM_CHAN", 0, comment="Number of spectral channels"
    )
    col14 = tableutil.makescacoldesc(
        "TOTAL_BANDWIDTH",
        0.0,
        comment="The total bandwidth for this window",
        keywords={
            "QuantumUnits": [
                "Hz",
            ]
        },
    )

    desc = tableutil.maketabdesc(
        [
            col1,
            col2,
            col3,
            col4,
            col5,
            col6,
            col7,
            col8,
            col9,
            col10,
            col11,
            col12,
            col13,
            col14,
        ]
    )
    tb = table("%s/SPECTRAL_WINDOW" % ms_name, desc, nrow=0, ack=False)
    added_spwlist = []
    for idx, vis in enumerate(vis_list):
        spwid = visid_to_spwid[idx]
        if spwid in added_spwlist:
            continue
        tb.addrows(1)
        tb.putcell(
            "MEAS_FREQ_REF", spwid, 5
        )  # https://github.com/ska-sa/pyxis/issues/27
        tb.putcell("CHAN_FREQ", spwid, vis.frequency.data)
        tb.putcell("REF_FREQUENCY", spwid, vis.frequency.data[0])
        tb.putcell("CHAN_WIDTH", spwid, vis["channel_bandwidth"].data)
        tb.putcell("EFFECTIVE_BW", spwid, vis["channel_bandwidth"].data)
        tb.putcell("RESOLUTION", spwid, vis["channel_bandwidth"].data)
        tb.putcell("FLAG_ROW", spwid, False)
        tb.putcell("FREQ_GROUP", spwid, spwid + 1)
        tb.putcell("FREQ_GROUP_NAME", spwid, "group%i" % (spwid + 1))
        tb.putcell("IF_CONV_CHAIN", spwid, spwid)
        tb.putcell(
            "NAME", spwid, "IF %i, %i channels" % (spwid + 1, len(vis.frequency.data))
        )
        tb.putcell("NET_SIDEBAND", spwid, 0)
        tb.putcell("NUM_CHAN", spwid, len(vis.frequency.data))
        tb.putcell(
            "TOTAL_BANDWIDTH",
            spwid,
            numpy.max(vis.frequency.data) - numpy.min(vis.frequency.data),
        )

        added_spwlist.append(spwid)
    tb.flush()
    tb.close()


def put_misc_table(ms_name):
    """
    Put a table of miscellaneous information into the table file.
    :param ms_name: Name of the MS
    :return:
    """

    # Flag command

    col1 = tableutil.makescacoldesc(
        "TIME",
        0.0,
        comment="Midpoint of interval for which this flag is valid",
        keywords={
            "QuantumUnits": [
                "s",
            ],
            "MEASINFO": {"type": "epoch", "Ref": "UTC"},
        },
    )
    col2 = tableutil.makescacoldesc(
        "INTERVAL",
        0.0,
        comment="Time interval for which this flag is valid",
        keywords={
            "QuantumUnits": [
                "s",
            ]
        },
    )
    col3 = tableutil.makescacoldesc(
        "TYPE", "flag", comment="Type of flag (FLAG or UNFLAG)"
    )
    col4 = tableutil.makescacoldesc("REASON", "reason", comment="Flag reason")
    col5 = tableutil.makescacoldesc("LEVEL", 0, comment="Flag level - revision level")
    col6 = tableutil.makescacoldesc("SEVERITY", 0, comment="Severity code (0-10)")
    col7 = tableutil.makescacoldesc(
        "APPLIED", False, comment="True if flag has been applied to main table"
    )
    col8 = tableutil.makescacoldesc("COMMAND", "command", comment="Flagging command")

    desc = tableutil.maketabdesc([col1, col2, col3, col4, col5, col6, col7, col8])
    tb = table("%s/FLAG_CMD" % ms_name, desc, nrow=0, ack=False)

    tb.flush()
    tb.close()

    # History

    col1 = tableutil.makescacoldesc(
        "TIME",
        0.0,
        comment="Timestamp of message",
        keywords={
            "QuantumUnits": [
                "s",
            ],
            "MEASINFO": {"type": "epoch", "Ref": "UTC"},
        },
    )
    col2 = tableutil.makescacoldesc(
        "OBSERVATION_ID",
        0,
        comment="Observation id (index in OBSERVATION table)",
    )
    col3 = tableutil.makescacoldesc("MESSAGE", "message", comment="Log message")
    col4 = tableutil.makescacoldesc("PRIORITY", "NORMAL", comment="Message priority")
    col5 = tableutil.makescacoldesc(
        "ORIGIN",
        "origin",
        comment="(Source code) origin from which message originated",
    )
    col6 = tableutil.makescacoldesc("OBJECT_ID", 0, comment="Originating ObjectID")
    col7 = tableutil.makescacoldesc(
        "APPLICATION", "application", comment="Application name"
    )
    col8 = tableutil.makearrcoldesc(
        "CLI_COMMAND", "command", 1, comment="CLI command sequence"
    )
    col9 = tableutil.makearrcoldesc(
        "APP_PARAMS", "params", 1, comment="Application parameters"
    )

    desc = tableutil.maketabdesc([col1, col2, col3, col4, col5, col6, col7, col8, col9])
    tb = table("%s/HISTORY" % ms_name, desc, nrow=0, ack=False)

    tb.flush()
    tb.close()

    # POINTING

    col1 = tableutil.makescacoldesc("ANTENNA_ID", 0, comment="Antenna Id")
    col2 = tableutil.makescacoldesc(
        "TIME",
        0.0,
        comment="Time interval midpoint",
        keywords={
            "QuantumUnits": [
                "s",
            ],
            "MEASINFO": {"type": "epoch", "Ref": "UTC"},
        },
    )
    col3 = tableutil.makescacoldesc(
        "INTERVAL",
        0.0,
        comment="Time interval",
        keywords={
            "QuantumUnits": [
                "s",
            ]
        },
    )
    col4 = tableutil.makescacoldesc("NAME", "name", comment="Pointing position name")
    col5 = tableutil.makescacoldesc("NUM_POLY", 0, comment="Series order")
    col6 = tableutil.makescacoldesc(
        "TIME_ORIGIN",
        0.0,
        comment="Time origin for direction",
        keywords={
            "QuantumUnits": [
                "s",
            ],
            "MEASINFO": {"type": "epoch", "Ref": "UTC"},
        },
    )
    col7 = tableutil.makearrcoldesc(
        "DIRECTION",
        0.0,
        2,
        comment="Antenna pointing direction as polynomial in time",
        keywords={
            "QuantumUnits": ["rad", "rad"],
            "MEASINFO": {"type": "direction", "Ref": "J2000"},
        },
    )
    col8 = tableutil.makearrcoldesc(
        "TARGET",
        0.0,
        2,
        comment="target direction as polynomial in time",
        keywords={
            "QuantumUnits": ["rad", "rad"],
            "MEASINFO": {"type": "direction", "Ref": "J2000"},
        },
    )
    col9 = tableutil.makescacoldesc(
        "TRACKING", True, comment="Tracking flag - True if on position"
    )

    desc = tableutil.maketabdesc([col1, col2, col3, col4, col5, col6, col7, col8, col9])
    tb = table("%s/POINTING" % ms_name, desc, nrow=0, ack=False)

    tb.flush()
    tb.close()

    # Processor

    col1 = tableutil.makescacoldesc("TYPE", "type", comment="Processor type")
    col2 = tableutil.makescacoldesc("SUB_TYPE", "subtype", comment="Processor sub type")
    col3 = tableutil.makescacoldesc("TYPE_ID", 0, comment="Processor type id")
    col4 = tableutil.makescacoldesc("MODE_ID", 0, comment="Processor mode id")
    col5 = tableutil.makescacoldesc("FLAG_ROW", False, comment="flag")

    desc = tableutil.maketabdesc([col1, col2, col3, col4, col5])
    tb = table("%s/PROCESSOR" % ms_name, desc, nrow=0, ack=False)

    tb.flush()
    tb.close()

    # State

    col1 = tableutil.makescacoldesc(
        "SIG", True, comment="True for a source observation"
    )
    col2 = tableutil.makescacoldesc(
        "REF", False, comment="True for a reference observation"
    )
    col3 = tableutil.makescacoldesc(
        "CAL",
        0.0,
        comment="Noise calibration temperature",
        keywords={
            "QuantumUnits": [
                "K",
            ]
        },
    )
    col4 = tableutil.makescacoldesc(
        "LOAD",
        0.0,
        comment="Load temperature",
        keywords={
            "QuantumUnits": [
                "K",
            ]
        },
    )
    col5 = tableutil.makescacoldesc(
        "SUB_SCAN", 0, comment="Sub scan number, relative to scan number"
    )
    col6 = tableutil.makescacoldesc(
        "OBS_MODE", "mode", comment="Observing mode, e.g., OFF_SPECTRUM"
    )
    col7 = tableutil.makescacoldesc("FLAG_ROW", False, comment="Row flag")

    desc = tableutil.maketabdesc([col1, col2, col3, col4, col5, col6, col7])
    tb = table("%s/STATE" % ms_name, desc, nrow=0, ack=False)

    tb.flush()
    tb.close()


def finsh_msfile(ms_name):
    """
    Finish the MS file.
    :param ms_name:
    :return:
    """

    tb = table("%s" % ms_name, readonly=False, ack=False)
    tb.putinfo(
        {
            "type": "Measurement Set",
            "readme": "This is a MeasurementSet Table holding measurements from a Telescope",
        }
    )
    tb.putkeyword("MS_VERSION", numpy.float32(2.0))
    for filename in sorted(glob.glob("%s/*" % ms_name)):
        if os.path.isdir(filename):
            tname = os.path.basename(filename)
            stb = table("%s/%s" % (ms_name, tname), ack=False)
            tb.putkeyword(tname, stb)
            stb.close()
    tb.flush()
    tb.close()


def export_regularisation_visibility_to_ms(vis_list, ms_name):
    """
    Write a list of visibility objects to a MS.
    :param vis_list: list of visibility objects
    :param ms_name: name of the MS
    :return:
    """

    init_main_table(vis_list, ms_name)

    (
        visid_to_ddsid,
        visid_to_fieldid,
        visid_to_polid,
        visid_to_spwid,
        visid_to_sourceid,
    ) = put_data_description(vis_list, ms_name)

    put_main_table(vis_list, ms_name, visid_to_fieldid, visid_to_ddsid)

    put_antena_table(vis_list, ms_name)

    put_polarization_table(vis_list, ms_name, visid_to_polid)

    put_source_table(vis_list, ms_name, visid_to_spwid, visid_to_sourceid)

    put_field_table(vis_list, ms_name, visid_to_sourceid, visid_to_fieldid)

    put_spectralwindow_table(vis_list, ms_name, visid_to_spwid)

    put_misc_table(ms_name)

    finsh_msfile(ms_name)


def get_regularized_table(mstable, fieldid, ddsid, ifauto, nant, filled_tmp_table_file):
    """
    regularized ms table, sorted by ant1,time,ant2, and remove duplicates
    and make nrows of table is ntimes * nbaselines

    :param mstable: ms table
    :param fieldid: field id
    :param ddsid: data desc id
    :param ifauto: if auto
    :param nant: nant from antenna table
    :return: regularized ms table
    """

    from casacore.tables import taql, table

    selected_sorted_table = mstable.query(
        query=f"FIELD_ID=={fieldid} AND DATA_DESC_ID=={ddsid}",
        sortlist="TIME,ANTENNA1,ANTENNA2",
    )

    noduplicate_table = taql(
        "select DISTINCT ANTENNA1,ANTENNA2,TIME,EXPOSURE,SCAN_NUMBER from $selected_sorted_table"
    )

    selected_sorted_nodup_table = mstable.selectrows(noduplicate_table.rownumbers())

    if os.path.exists(filled_tmp_table_file):
        shutil.rmtree(filled_tmp_table_file)

    filled_tmp_table = table(
        filled_tmp_table_file,
        tabledesc=selected_sorted_nodup_table.getdesc(),
        readonly=False,
    )

    timeall = numpy.unique(selected_sorted_nodup_table.getcol("TIME"))

    # check nrow number = ntimes * nbaseline and baselines
    antnumber = nant
    if ifauto:
        nbaselines = int(antnumber * (antnumber + 1) / 2)
    else:
        nbaselines = int(antnumber * (antnumber - 1) / 2)

    # if the table is full, just return the sorted and no duplication table
    if selected_sorted_nodup_table.nrows() == len(timeall) * nbaselines:
        return selected_sorted_nodup_table

    # NOW we need to fill the full shape,
    # nbaseline = nant * (nant - 1)/2 or nant * (nant + 1)/2
    # nrows = ntimes * nbaseline
    if ifauto:
        baselines = generate_baselines(antnumber)
    else:
        baselines = generate_baselines_no_auto(antnumber)

    row_to_ant_map = {}
    ant_to_row_map = {}
    for rowidx, (a1, a2) in enumerate(baselines):
        row_to_ant_map[rowidx] = (a1, a2)
        ant_to_row_map[f"{a1},{a2}"] = rowidx
        rowidx += 1

    nchan, npol = selected_sorted_nodup_table.getcell("FLAG", 0).shape
    fake_row = {
        "UVW": numpy.array([0, 0, 0]),
        "FLAG": numpy.zeros((nchan, npol), dtype=bool),
        "WEIGHT": numpy.zeros((npol)),
        "SIGMA": numpy.zeros((npol)),
        "ARRAY_ID": 0,
        "DATA_DESC_ID": ddsid,
        "FEED1": 0,
        "FEED2": 0,
        "FIELD_ID": fieldid,
        "FLAG_ROW": False,
        "OBSERVATION_ID": 0,
        "PROCESSOR_ID": -1,
        "STATE_ID": -1,
        "DATA": numpy.zeros((nchan, npol)),
        "WEIGHT_SPECTRUM": numpy.zeros((nchan, npol)),
    }

    for timevalue in timeall:
        row_couter = numpy.zeros(nbaselines)
        this_time_table = noduplicate_table.query("TIME==$timevalue")

        if this_time_table.nrows() == nbaselines:
            continue

        assert (
            this_time_table.nrows() <= nbaselines
        ), "Do you turn on auto-correlation?, args ifauto=True"

        for rowvalue in this_time_table:
            a1, a2 = rowvalue["ANTENNA1"], rowvalue["ANTENNA2"]
            row_couter[ant_to_row_map[f"{a1},{a2}"]] += 1

        for ridx in numpy.argwhere(row_couter < 1)[:, 0]:
            filled_tmp_table.addrows(1)
            lastindex = filled_tmp_table.nrows()

            for rowkey in filled_tmp_table.colnames():
                rowfill = fake_row.get(rowkey, None)
                if rowfill is None:
                    pass
                else:
                    filled_tmp_table.putcell(rowkey, lastindex - 1, rowfill)
            filled_tmp_table.putcell("TIME", lastindex - 1, timevalue)
            filled_tmp_table.putcell("TIME_CENTROID", lastindex - 1, timevalue)
            filled_tmp_table.putcell(
                "SCAN_NUMBER", lastindex - 1, this_time_table.getcell("SCAN_NUMBER", 0)
            )
            filled_tmp_table.putcell(
                "INTERVAL", lastindex - 1, this_time_table.getcell("EXPOSURE", 0)
            )
            filled_tmp_table.putcell(
                "EXPOSURE", lastindex - 1, this_time_table.getcell("EXPOSURE", 0)
            )
            filled_tmp_table.putcell("ANTENNA1", lastindex - 1, row_to_ant_map[ridx][0])
            filled_tmp_table.putcell("ANTENNA2", lastindex - 1, row_to_ant_map[ridx][1])

        filled_tmp_table.flush()
    filled_sorted_nodup_table = table([selected_sorted_nodup_table, filled_tmp_table])
    filled_sorted_nodup_table = filled_sorted_nodup_table.sort("TIME,ANTENNA1,ANTENNA2")

    # close tmp table
    selected_sorted_table.close()
    noduplicate_table.close()
    selected_sorted_nodup_table.close()
    filled_tmp_table.close()

    return filled_sorted_nodup_table


def create_visibility_from_ms_regularisation(
    msname,
    start_chan=None,
    end_chan=None,
    ack=False,
    datacolumn="DATA",
    selected_sources=None,
    selected_dds=None,
    ifauto=False,
    filled_tmp_table_prefix="/tmp/rascil_filled_tmp",
    regularized=True,
):
    """
    Create visibility table from MS regularisation.
    :param msname : MS name
    :param start_chan: start channel
    :param end_chan: end channel
    :param ack: use ack flag
    :param datacolumn: data column
    :param selected_sources: list of sources to include
    :param selected_dds: list of data descs to include
    :param ifauto: if True, use auto-selection
    :param filled_tmp_table_file: file to write filled table to
    :param regularized: if True, use regularisation
    :return: vis_list
    """
    uuid_path = str(uuid.uuid4())
    filled_tmp_table_prefix = filled_tmp_table_prefix + "/" + uuid_path
    if os.path.exists(filled_tmp_table_prefix):
        shutil.rmtree(filled_tmp_table_prefix)
    os.makedirs(filled_tmp_table_prefix)

    tab = table(msname, ack=ack)

    if selected_sources is None:
        fields = numpy.unique(tab.getcol("FIELD_ID"))
    else:
        fieldtab = table("%s/FIELD" % msname, ack=False)
        sources = fieldtab.getcol("NAME")
        fields = list()
        for field, source in enumerate(sources):
            if source in selected_sources:
                fields.append(field)
        assert len(fields) > 0, "No sources selected"

    if selected_dds is None:
        dds = numpy.unique(tab.getcol("DATA_DESC_ID"))
    else:
        dds = selected_dds

    vis_list = list()
    for field in fields:
        for dd in dds:
            anttab = table("%s/ANTENNA" % msname, ack=False)
            names = numpy.array(anttab.getcol("NAME"))
            mount = numpy.array(anttab.getcol("MOUNT"))[names != ""]
            diameter = numpy.array(anttab.getcol("DISH_DIAMETER"))[names != ""]
            xyz = numpy.array(anttab.getcol("POSITION"))[names != ""]
            offset = numpy.array(anttab.getcol("OFFSET"))[names != ""]
            stations = numpy.array(anttab.getcol("STATION"))[names != ""]
            names = numpy.array(anttab.getcol("NAME"))[names != ""]
            nants = len(names)

            location = EarthLocation(
                x=Quantity(xyz[0][0], "m"),
                y=Quantity(xyz[0][1], "m"),
                z=Quantity(xyz[0][2], "m"),
            )

            configuration = Configuration.constructor(
                name="",
                location=location,
                names=names,
                xyz=xyz,
                mount=mount,
                frame="ITRF",
                receptor_frame=ReceptorFrame("linear"),
                diameter=diameter,
                offset=offset,
                stations=stations,
            )

            # Now get info from the subtables
            ddtab = table("%s/DATA_DESCRIPTION" % msname, ack=False)
            spwid = ddtab.getcol("SPECTRAL_WINDOW_ID")[dd]
            polid = ddtab.getcol("POLARIZATION_ID")[dd]
            ddtab.close()

            meta = {"MSV2": {"FIELD_ID": field, "DATA_DESC_ID": dd}}
            if regularized:
                ms = get_regularized_table(
                    mstable=tab,
                    fieldid=field,
                    ddsid=dd,
                    ifauto=ifauto,
                    nant=nants,
                    filled_tmp_table_file=f"{filled_tmp_table_prefix}/ms-field-{field}-dd-{dd}.tmp",
                )
            else:
                ms = tab.query(
                    query=f"FIELD_ID=={field} AND DATA_DESC_ID=={dd}",
                )

            assert (
                ms.nrows() > 0
            ), "Empty selection for FIELD_ID=%d and DATA_DESC_ID=%d" % (field, dd)

            # Get phasecentres
            fieldtab = table("%s/FIELD" % msname, ack=False)
            pc = fieldtab.getcol("PHASE_DIR")[field, 0, :]
            source = fieldtab.getcol("NAME")[field]
            phasecentre = SkyCoord(
                ra=pc[0] * u.rad, dec=pc[1] * u.rad, frame="icrs", equinox="J2000"
            )

            if ifauto:
                baselines = pandas.MultiIndex.from_tuples(
                    generate_baselines(nants), names=("antenna1", "antenna2")
                )
            else:
                baselines = pandas.MultiIndex.from_tuples(
                    generate_baselines_no_auto(nants), names=("antenna1", "antenna2")
                )
            nbaselines = len(baselines)

            datacol = ms.getcol(datacolumn, nrow=1)
            datacol_shape = list(datacol.shape)
            channels = datacol.shape[-2]
            if start_chan is not None and end_chan is not None:
                blc = [start_chan, 0]
                trc = [end_chan, datacol_shape[-1] - 1]
                channum = range(start_chan, end_chan + 1)
            else:
                blc = [0, 0]
                trc = [channels - 1, datacol_shape[-1] - 1]
                channum = range(0, channels)

            spwtab = table("%s/SPECTRAL_WINDOW" % msname, ack=False)
            cfrequency = numpy.array(spwtab[spwid]["CHAN_FREQ"][channum])
            cchannel_bandwidth = numpy.array(spwtab[spwid]["CHAN_WIDTH"][channum])
            nchan = cfrequency.shape[0]

            npol, polarisation_frame = _get_polarisation_info(msname, polid)

            integration_time = ms.getcol("INTERVAL")
            time = ms.getcol("TIME") - integration_time / 2.0
            bv_times = numpy.unique(time)
            ntimes = len(bv_times)
            bv_uvw = -1 * ms.getcol("UVW").reshape(ntimes, nbaselines, 3)
            bv_vis = ms.getcolslice(datacolumn, blc=blc, trc=trc).reshape(
                ntimes, nbaselines, nchan, npol
            )
            bv_flags = ms.getcolslice("FLAG", blc=blc, trc=trc).reshape(
                ntimes, nbaselines, nchan, npol
            )
            if "WEIGHT_SPECTRUM" in ms.colnames():
                bv_weight = ms.getcolslice("WEIGHT_SPECTRUM", blc=blc, trc=trc).reshape(
                    ntimes, nbaselines, nchan, npol
                )
            else:
                bv_weight = ms.getcol("WEIGHT").reshape(ntimes, nbaselines, npol)[
                    :, :, numpy.newaxis, ...
                ]
                bv_weight = numpy.repeat(bv_weight, nchan, axis=2)

            bv_integration_time = integration_time.reshape(ntimes, nbaselines)[:, 0]
            bv_vis = bv_vis.astype("complex128")
            vis_list.append(
                Visibility.constructor(
                    uvw=bv_uvw,
                    baselines=baselines,
                    time=bv_times,
                    frequency=cfrequency,
                    channel_bandwidth=cchannel_bandwidth,
                    vis=bv_vis,
                    flags=bv_flags,
                    weight=bv_weight,
                    integration_time=bv_integration_time,
                    imaging_weight=bv_weight,
                    configuration=configuration,
                    phasecentre=phasecentre,
                    polarisation_frame=polarisation_frame,
                    source=source,
                    meta=meta,
                )
            )

    return vis_list
