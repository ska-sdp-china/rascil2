"""Function to check the installation"""

import logging

from rascil2.data_models import rascil2_data_path

log = logging.getLogger(__file__)

__all__ = ["check_data_directory"]


def check_data_directory(verbose=False, fatal=True):
    """Check the RASCIL2 data directory to see if it has been installed correctly"""
    try:
        canary = rascil2_data_path("configurations/LOWBD2.csv")
        with open(canary, "r") as f:
            first = f.read(1)
            if first == "version https://git-lfs.github.com/spec/v1":
                log.warning(
                    "The RASCIL2 data directory is not filled correctly - git lfs pull is required"
                )
            else:
                if verbose:
                    print(
                        "The RASCIL2 data directory appears to have been installed correctly"
                    )
    except FileNotFoundError:
        # FileNotFoundError raised when trying to open 'canary'
        # and also if rascil2_data_path doesn't find the data directory
        if fatal:
            raise FileNotFoundError(
                "The RASCIL2 data directory is not available - stopping"
            )
        else:
            log.warning(
                "The RASCIL2 data directory is not available - continuing but any simulations will fail"
            )


if __name__ == "__main__":
    check_data_directory()
