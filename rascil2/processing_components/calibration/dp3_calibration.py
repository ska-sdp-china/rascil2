"""
Functions to use DP3 for calibration purposes.
"""

__all__ = [
    "dp3_gaincal",
]

import logging

import numpy

from rascil2.processing_components.calibration.chain_calibration import (
    create_calibration_controls,
)
from rascil2.processing_components.visibility.operations import (
    expand_polarizations,
    copy_data_and_shrink_polarizations,
)

log = logging.getLogger("func-python-logger")


def create_dp_info(vis):
    """Creates a DPInfo object based on input visibilities.

    :param vis: Visibility object (or graph)
    :return: DPInfo to use for DP3 steps
    """

    import dp3  # noqa: E501 # pylint:disable=no-name-in-module,import-error,import-outside-toplevel

    # Some DP3 steps only work with 4 correlations (gaincal, for example)
    # hence we force the number of correlations to 4 in the DPInfo, and do a
    # mapping from 1,2 correlations to 4 when needed (in the
    # process_visibilities function)
    n_correlations = 4
    dpinfo = dp3.DPInfo(n_correlations)
    dpinfo.set_channels(vis.frequency.data, vis.channel_bandwidth.data)

    antenna1 = vis.antenna1.data
    antenna2 = vis.antenna2.data
    antenna_names = vis.configuration.names.data
    antenna_positions = vis.configuration.xyz.data
    antenna_diameters = vis.configuration.diameter.data
    dpinfo.set_antennas(
        antenna_names,
        antenna_diameters,
        antenna_positions,
        antenna1,
        antenna2,
    )
    first_time = vis.time.data[0]
    last_time = vis.time.data[-1]
    time_interval = vis.integration_time.data[0]
    dpinfo.set_times(first_time, last_time, time_interval)
    dpinfo.phase_center = [vis.phasecentre.ra.rad, vis.phasecentre.dec.rad]

    return dpinfo


def process_visibilities(
    step, vis, *, save_out_vis=True, extra_data_name=None, extra_data=None
):
    """Process visibilities with the operation defined by the input step.

    :param step: DP3 step
    :param vis: Visibility object (or graph)
    :param save_out_vis: set to False if the output visibilities are not
                        needed (for example if the step should produce
                        solutions, which are saved to disk)
    :param extra_data_name: Name of the extra visibility data the step should
                            use (for example model data)
    :param extra_data: Visibility object (or graph) containing the extra data
    :return: Processed visibilities
    """

    import dp3  # noqa: E501 # pylint:disable=no-name-in-module,import-error,import-outside-toplevel
    import dp3.steps  # noqa: E501 # pylint:disable=no-name-in-module,import-error,import-outside-toplevel
    from dp3.parameterset import (  # noqa: E501 # pylint:disable=no-name-in-module,import-error,import-outside-toplevel
        ParameterSet,
    )

    # To extract the visibilities processed by the step, we add a QueueOutput
    # step which will accumulate the output visibilities.
    if save_out_vis:
        queue_step = dp3.steps.QueueOutput(ParameterSet(), "")
        step.set_next_step(queue_step)
    else:
        step.set_next_step(
            dp3.make_step("null", ParameterSet(), "", dp3.MsType.regular)
        )

    step.set_info(create_dp_info(vis))

    # The time_index variable is needed to extract the correct time from the
    # "extra_data" visibility object, which is not indexed in the for loop
    time_index = 0
    for time, vis_per_timeslot in vis.groupby("time"):
        dpbuffer = dp3.DPBuffer()
        dpbuffer.set_data(
            expand_polarizations(vis_per_timeslot.vis.data, numpy.complex64)
        )
        dpbuffer.set_weights(
            expand_polarizations(vis_per_timeslot.weight.data, numpy.float32)
        )
        dpbuffer.set_flags(expand_polarizations(vis_per_timeslot.flags.data, bool))
        dpbuffer.set_uvw(-vis_per_timeslot.uvw.data)
        if extra_data_name:
            time_slice = extra_data.time.data[time_index]
            model_vis_per_timeslot = extra_data.sel({"time": time_slice})
            modeldata = expand_polarizations(
                model_vis_per_timeslot.vis.data, numpy.complex64
            )
            dpbuffer.add_data(extra_data_name)
            dpbuffer.set_extra_data(extra_data_name, modeldata)
        time_index += 1

        dpbuffer.set_time(time)
        step.process(dpbuffer)
    step.finish()

    # Extract the results from the QueueOutput step, if desired.
    if save_out_vis:
        time_index = 0

        while not queue_step.queue.empty():
            dpbuffer_from_queue = queue_step.queue.get()
            visibilities_out = numpy.array(dpbuffer_from_queue.get_data(), copy=False)
            flags_out = numpy.array(dpbuffer_from_queue.get_flags(), copy=False)
            weights_out = numpy.array(dpbuffer_from_queue.get_weights(), copy=False)
            uvws_out = numpy.array(dpbuffer_from_queue.get_uvw(), copy=False)

            nr_polarizations = vis.vis.data.shape[-1]
            vis.vis.data[time_index, :, :, :] = copy_data_and_shrink_polarizations(
                visibilities_out, nr_polarizations
            )
            vis.flags.data[time_index, :, :, :] = copy_data_and_shrink_polarizations(
                flags_out, nr_polarizations
            )
            vis.weight.data[time_index, :, :, :] = copy_data_and_shrink_polarizations(
                weights_out, nr_polarizations
            )

            vis.uvw.data[time_index, :, :] = uvws_out

            time_index += 1

        return vis

    return None


def create_parset_from_context(
    vis,
    calibration_context,
    global_solution,
    solutions_filename,
    *,
    skymodel_filename=None,
    apply_solutions=True,
    modeldata_name=None,
):
    """Defines input parset for DP3 based on calibration context.

    :param vis: Visibility object
    :param calibration_context: String giving terms to be calibrated e.g. 'TGB'
    :param global_solution: Find a single solution over all frequency channels
    :param solutions_filename: Filename of the calibration solutions produced
    by DP3. The easiest way to inspect the results is by using a .h5 extension.
    :param skymodel_filename: Filename of the skymodel used by DP3
    :param apply_solutions: Apply the calibration solution to the visibility
    :param modeldata_name: Name of the modeldata as specificed in the DPBuffer.
    If this is given, the skymodel_filename won't be read
    :return: list of parsets for the different calibrations to run
    """

    from dp3.parameterset import (  # noqa: E501 # pylint: disable=import-error,import-outside-toplevel
        ParameterSet,
    )

    parset_list = []
    controls = create_calibration_controls()
    for calibration_control in calibration_context:
        parset = ParameterSet()

        parset.add("gaincal.parmdb", solutions_filename)
        if modeldata_name:
            parset.add("gaincal.reusemodel", modeldata_name)
        else:
            parset.add("gaincal.sourcedb", skymodel_filename)
        timeslice = controls[calibration_control]["timeslice"]
        if timeslice == "auto" or timeslice is None or timeslice <= 0.0:
            parset.add("gaincal.solint", "1")
        else:
            nbins = max(
                1,
                numpy.ceil(
                    (numpy.max(vis.time.data) - numpy.min(vis.time.data)) / timeslice
                ).astype("int"),
            )
            parset.add("gaincal.solint", str(nbins))
        if global_solution:
            parset.add("gaincal.nchan", "0")
        else:
            parset.add("gaincal.nchan", "1")

        if apply_solutions:
            parset.add("gaincal.applysolution", "true")

        if controls[calibration_control]["phase_only"]:
            if controls[calibration_control]["shape"] == "vector":
                parset.add("gaincal.caltype", "diagonalphase")
            elif controls[calibration_control]["shape"] == "matrix":
                parset.add("gaincal.caltype", "fulljones")
            else:
                parset.add("gaincal.caltype", "scalarphase")
        else:
            if controls[calibration_control]["shape"] == "vector":
                parset.add("gaincal.caltype", "diagonal")
            elif controls[calibration_control]["shape"] == "matrix":
                parset.add("gaincal.caltype", "fulljones")
            else:
                parset.add("gaincal.caltype", "scalar")

        parset_list.append(parset)

    return parset_list


def dp3_gaincal(
    vis,
    calibration_context,
    global_solution,
    skymodel_filename="test.skymodel",
    solutions_filename="gaincal.h5",
):
    """Calibrates visibilities using the DP3 package.

    :param vis: Visibility object (or graph)
    :param calibration_context: String giving terms to be calibrated e.g. 'TGB'
    :param global_solution: Solve for global gains
    :param skymodel_filename: Filename of the skymodel used by DP3
    :return: calibrated visibilities
    """
    from dp3 import (  # noqa: E501 # pylint:disable=no-name-in-module,import-error,import-outside-toplevel
        MsType,
        make_step,
    )

    log.info("Started computing dp3_gaincal")
    calibrated_vis = vis.copy(deep=True)

    parset_list = create_parset_from_context(
        calibrated_vis,
        calibration_context,
        global_solution,
        solutions_filename,
        skymodel_filename=skymodel_filename,
    )

    for parset in parset_list:
        gaincal_step = make_step(
            "gaincal",
            parset,
            "gaincal.",
            MsType.regular,
        )

        process_visibilities(gaincal_step, calibrated_vis)

        log.info("Finished computing dp3_gaincal")

    return calibrated_vis


def dp3_gaincal_with_modeldata(
    vis,
    calibration_context,
    global_solution,
    model_vis,
    modeldata_name,
    solutions_filename="gaincal.h5",
):
    """Calibrates visibilities using the DP3 package, with model data provided
       as a separate visibility object.

    :param vis: Visibility object (or graph)
    :param calibration_context: String giving terms to be calibrated e.g. 'TGB'
    :param global_solution: Solve for global gains
    :param model_vis: Visibility object (or graph) containing the extra data
    :param modeldata_name: Name of the model visibility data the step should
                           use (for example "modeldata")
    :return: calibrated visibilities
    """

    from dp3 import (  # noqa: E501 # pylint:disable=no-name-in-module,import-error,import-outside-toplevel
        MsType,
        make_step,
    )

    log.info("Started computing dp3_gaincal_with_modeldata")
    calibrated_vis = vis.copy(deep=True)

    parset_list = create_parset_from_context(
        calibrated_vis,
        calibration_context,
        global_solution,
        solutions_filename,
        modeldata_name=modeldata_name,
    )

    for parset in parset_list:
        gaincal_step = make_step(
            "gaincal",
            parset,
            "gaincal.",
            MsType.regular,
        )
        process_visibilities(
            gaincal_step,
            calibrated_vis,
            extra_data_name=modeldata_name,
            extra_data=model_vis,
        )

    log.info("Finished computing dp3_gaincal_with_modeldata")

    return calibrated_vis
