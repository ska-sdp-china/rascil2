
Radio Astronomy Simulation, Calibration and Imaging Library 2
===========================================================

The Radio Astronomy Simulation, Calibration and Imaging Library
expresses radio interferometry calibration and imaging algorithms in
python and numpy. The interfaces all operate with familiar data structures
such as image, visibility table, gaintable, etc. The python source
code is directly accessible from these documentation pages: see the
source link in the top right corner.

Since the development of the SKA Science Data Processor (SDP), RASCIL has been simplified, optimized, and split to meet the requirements of SDP.
To maintain a stable and extensive version of RASCIL, we create RASCIL2 based on RASCIL version 0.7 for learning, researching and software development.

The copyright belongs to respectful owners who have been mentioned in CONTRIBUTORS.


The [Documentation](http://ska-sdp-china.skatelescope.cn/rascil2/) includes usage 
examples, API, and installation directions.

RASCIL2 CI/CD occurs on  [Gitlab](https://gitlab.astrolab.cn/ska-sdp-china/rascil2)