.. _documentation_master:

.. toctree::

Radio Astronomy Simulation, Calibration and Imaging Library
###########################################################

The SKA Radio Astronomy Simulation, Calibration and Imaging Library expresses radio interferometry calibration and
imaging algorithms in python and numpy. The interfaces all operate with familiar data
structures such as image, visibility table, gaintable, etc.

 - Documentation: https://ska-telescope.gitlab.io/external/rascil2/
 - Source code: https://gitlab.com/ska-sdp-china/rascil2

.. _feedback: mailto:realtimcornwell@gmail.com
