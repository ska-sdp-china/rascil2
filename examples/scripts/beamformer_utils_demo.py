"""
Here is an example of how to use these functions. In this case
there are three input calibration files. The first contains
co-polarisation gain terms from CASA task "bandpass", the second
contains cross-polarisation delay terms from CASA task "gaincal"
with gaintype='KCROSS', and the third contains cross-polarisation
leakage terms from CASA task "polcal" with poltype='Df'.

Synchronization from ska-sdp-func-python
https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/29
"""

import numpy
from rascil2.data_models import GainTable, export_gaintable_to_hdf5
from rascil2.processing_components.io.casatable import (
    import_gaintable_from_casa_cal_table,
)

from rascil2.processing_components.calibration.beamformer_utils import (
    expand_delay_phase,
    multiply_gaintable_jones,
    set_beamformer_frequencies,
    resample_bandpass,
)

# import the CASA calibration tables into GainTables
Btable = import_gaintable_from_casa_cal_table("bandpass_B.out")
Ktable = import_gaintable_from_casa_cal_table("gaincal_Kcross.out")
Dtable = import_gaintable_from_casa_cal_table("polcal_Df.out")

# expand the Kcross delay phase shifts across the band
Ktable = expand_delay_phase(Ktable, Btable.frequency.data)

# combine the cross-pol terms then matrix multiply with the bandpass gain terms
Dtable = multiply_gaintable_jones(Ktable, Dtable, elementwise=True)
Jtable = multiply_gaintable_jones(Btable, Dtable)

# set up the output frequency channels
f_out = set_beamformer_frequencies(Btable, array="LOW")

# do the interpolation
gain_out = resample_bandpass(f_out, Jtable, alg="cubicspl")

# create a new GainTable with the output
shape = gain_out.shape
gain_weight = numpy.ones(shape)
gain_residual = numpy.zeros((shape[0], shape[2], shape[3], shape[4]))

gain_table_out = GainTable.constructor(
    gain=gain_out,
    time=Btable.time,
    interval=Btable.interval,
    weight=gain_weight,
    residual=gain_residual,
    frequency=f_out,
    receptor_frame=Btable.receptor_frame1,
    phasecentre=Btable.phasecentre,
    configuration=Btable.configuration,
    jones_type=Btable.jones_type,
)

# write to a HDF5 file
export_gaintable_to_hdf5(gain_table_out, "bandpass_CBF.hdf5")
