image: python:3.10-bullseye

variables:
  MPLBACKEND: "agg"

workflow:
  rules:
    - if: $CI_COMMIT_BRANCH
    - if: $CI_COMMIT_TAG

stages:
  - linting
  - build
  - test
  - test-dask
  - publish
  - prepost

compile_requirements:
  only:
    - schedules
  variables:
    GIT_LFS_SKIP_SMUDGE: "1"  # do not download LFS related files
  before_script:
    - sed -i "s|http://deb.debian.org/debian|https://artifact.astrolab.cn/repository/apt-debian|g" /etc/apt/sources.list
    - sed -i "s|http://security.debian.org/debian-security|https://artifact.astrolab.cn/repository/apt-debian-security|g" /etc/apt/sources.list
    - apt-get -y update
    - apt-get -y install git-lfs
    - git lfs fetch --all  # without this, the job complains about not finding a file, that doesn't actually exist..
    - pip3 config set global.index-url https://mirror.sjtu.edu.cn/pypi/web/simple # use this mirror as update index
    - pip3 install --upgrade pip
    - pip install gitpython python-gitlab
    - make requirements
  script:
    - python3 create_mr.py
  after_script:
    - git remote rm new_origin


linting:
  stage: linting
  before_script:
    - pip3 config set global.index-url https://artifact.astrolab.cn/repository/pypi-proxy/simple
    - pip3 install --upgrade pip
    - pip3 install -r requirements-test.txt
  script:
    - make lint
  artifacts:
    paths:
      - linting.xml
  rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule"'
    - if: '$CI_COMMIT_TAG'

# Build the data file only if on the master
data:
  tags:
    - cnlab
  stage: build
  script:
    - tar -zcf rascil_data.tgz data
  artifacts:
    paths:
      - rascil_data.tgz
    expire_in: 1 months
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PIPELINE_SOURCE != "schedule" && ($CI_COMMIT_TAG == "" || $CI_COMMIT_TAG == null)'

# Output used for publishing the package and building the docker images
build_package_dev:
  tags:
    - cnlab
  stage: build
  script:
    - python3 setup.py egg_info -b+dev.c${CI_COMMIT_SHORT_SHA} sdist bdist_wheel # dev
  artifacts:
    paths:
      - dist
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PIPELINE_SOURCE != "schedule"'

build_package_tag:
  tags:
    - cnlab
  stage: build
  script:
    - python3 setup.py egg_info sdist bdist_wheel # tag
  artifacts:
    paths:
      - dist
  rules:
    - if: '$CI_COMMIT_TAG'

# Always try to build the docs since this catches errors via the notebook
docs:
  tags:
    - cnlab
  stage: test
  before_script:
    - sed -i "s|http://deb.debian.org/debian|https://artifact.astrolab.cn/repository/apt-debian|g" /etc/apt/sources.list
    - sed -i "s|http://security.debian.org/debian-security|https://artifact.astrolab.cn/repository/apt-debian-security|g" /etc/apt/sources.list
    - apt-get update
    - apt-get -y install pandoc rsync cmake
    - pip3 config set global.index-url https://artifact.astrolab.cn/repository/pypi-proxy/simple
    - pip3 install --upgrade pip
    - pip install -r requirements-docs.txt
    - pip install -r requirements.txt
    - mkdir -p docs/build/html
    - mkdir -p test_results
    - mkdir -p ~/.astropy/config
    - echo "[utils.iers.iers]" > ~/.astropy/config/astropy.cfg 
    - echo "iers_auto_url=https://cicdfile.astrolab.cn/finals2000A.all" >> ~/.astropy/config/astropy.cfg 
    - python3 -c "from astropy.utils import iers;print(iers.conf.iers_auto_url)"
    - python3 -c "from astropy.time import Time; Time.now().ut1"
  script:
    - PYTHONPATH=`pwd` HOME=`pwd` make docs
  artifacts:
    paths:
      - docs/build/html/
    expire_in: 3 days
  rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule" && ($CI_COMMIT_TAG == "" || $CI_COMMIT_TAG == null)'

# Always run the unittests
test:
  stage: test
  tags:
    - astrolab-runner
  before_script:
    - sed -i "s|http://deb.debian.org/debian|https://artifact.astrolab.cn/repository/apt-debian|g" /etc/apt/sources.list
    - sed -i "s|http://security.debian.org/debian-security|https://artifact.astrolab.cn/repository/apt-debian-security|g" /etc/apt/sources.list
    - apt-get update
    - apt-get -y install rsync ca-certificates cmake
    - ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
    - date
    - pip3 config set global.index-url https://artifact.astrolab.cn/repository/pypi-proxy/simple
    - pip3 install --upgrade pip
    - pip3 install  -r requirements-test.txt
    - pip3 install  -r requirements.txt
    - mkdir -p test_results
    - mkdir -p ~/.astropy/config
    - echo "[utils.iers.iers]" > ~/.astropy/config/astropy.cfg 
    - echo "iers_auto_url=https://cicdfile.astrolab.cn/finals2000A.all" >> ~/.astropy/config/astropy.cfg 
    - python3 -c "from astropy.utils import iers;print(iers.conf.iers_auto_url)"
    - python3 -c "from astropy.time import Time; Time.now().ut1"
  script:
    - PYTHONPATH=`pwd` HOME=`pwd` make test
  after_script:
    - mv .coverage coverage_test
  coverage: '/^TOTAL.+?(\d+\%)$/'
  artifacts:
    paths:
      - coverage_test
      - coverage
      - unit-tests-other.xml
    expire_in: 3 days
  rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule" && ($CI_COMMIT_TAG == "" || $CI_COMMIT_TAG == null)'

test-dask:
  stage: test-dask
  before_script:
    - sed -i "s|http://deb.debian.org/debian|https://artifact.astrolab.cn/repository/apt-debian|g" /etc/apt/sources.list
    - sed -i "s|http://security.debian.org/debian-security|https://artifact.astrolab.cn/repository/apt-debian-security|g" /etc/apt/sources.list
    - apt-get update
    - ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
    - apt-get -y install rsync ca-certificates cmake
    - pip3 config set global.index-url https://artifact.astrolab.cn/repository/pypi-proxy/simple
    - pip3 install --upgrade pip
    - pip3 install  -r requirements-test.txt
    - pip3 install  -r requirements.txt
    - pip3 uninstall -y radler # radler may crash on some old cpu so we disable test it
    - mkdir -p test_results
    - mkdir -p ~/.astropy/config
    - echo "[utils.iers.iers]" > ~/.astropy/config/astropy.cfg 
    - echo "iers_auto_url=https://cicdfile.astrolab.cn/finals2000A.all" >> ~/.astropy/config/astropy.cfg 
    - python3 -c "from astropy.utils import iers;print(iers.conf.iers_auto_url)"
    - python3 -c "from astropy.time import Time; Time.now().ut1"
  script:
    - PYTHONPATH=`pwd` HOME=`pwd` make test-dask
  after_script:
    - mv .coverage coverage_dask
  coverage: '/^TOTAL.+?(\d+\%)$/'
  artifacts:
    paths:
      - coverage_dask
      - coverage
      - unit-tests-dask.xml
    expire_in: 3 days
  rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule" && ($CI_COMMIT_TAG == "" || $CI_COMMIT_TAG == null)'

test_gpu:
  image: $CUDA_AND_PYTHON310
  stage: test
  tags: [astrolab-runner-gpu]
  before_script:
    - python3 -V
    - pip3 -V
    - pip3 config set global.index-url https://artifact.astrolab.cn/repository/pypi-proxy/simple
    - pip3 install  --upgrade pip
    - export LD_LIBRARY_PATH=/usr/local/cuda/lib:/usr/local/cuda/lib64:$LD_LIBRARY_PATH
    - pip3 install  -r requirements-test.txt
    - pip3 install  -r requirements.txt
    - git clone https://gitlab.astrolab.cn/ska-sdp-china/ska-gridder-nifty-cuda.git  # => wagg
    - cd ska-gridder-nifty-cuda
    # due to a bug in wagg, we need to use the following branch for now
    - git checkout --track origin/sim-874-python-wrapper
    - cd python
    - pip3 install .
    - cd ../..
    - mkdir -p test_results
    - nvidia-smi
    - mkdir -p ~/.astropy/config
    - echo "[utils.iers.iers]" > ~/.astropy/config/astropy.cfg 
    - echo "iers_auto_url=https://cicdfile.astrolab.cn/finals2000A.all" >> ~/.astropy/config/astropy.cfg 
    - python3 -c "from astropy.utils import iers;print(iers.conf.iers_auto_url)"
    - python3 -c "from astropy.time import Time; Time.now().ut1"
  script:
    - nvidia-smi
    - PYTHONPATH=`pwd` HOME=`pwd` make test-gpu
  after_script:
    - mv .coverage coverage_test_gpu
  coverage: '/^TOTAL.+?(\d+\%)$/'
  artifacts:
    paths:
      - coverage_test_gpu
      - coverage
      - unit-tests-gpu.xml
    expire_in: 3 days
  rules:
    - if: '$CI_PIPELINE_SOURCE == "none"'  # disable gpu test
      when: never


# Test docker images upon each merge to master
# Publish tested image in the GitLab registry, with latest tag
docker_test:
  stage: test
  tags:
    - astrolab-docker
  image: docker:20.10.7
  services:
    - name: registry.astrolab.cn/ska-sdp-china/rascil2/dockerdindcustom:0.2
      alias: docker
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_TLS_CERTDIR: ""
    GIT_VERSION: $CI_COMMIT_SHORT_SHA
  before_script:
    - sed -i 's/dl-cdn.alpinelinux.org/artifact.astrolab.cn\/repository/g' /etc/apk/repositories
    - apk add make git
    - cp dist/*whl docker/rascil2-base
    - cd docker
  script:
    - make build_all_latest_with_hosts
    - make test_all
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin registry.astrolab.cn
    - make push_all_latest
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PIPELINE_SOURCE != "schedule" && ($CI_COMMIT_TAG == "" || $CI_COMMIT_TAG == null)'


# Publish the docs, data, and coverage if this is a build of the master
pages:
  stage: publish
  dependencies:
    - docs
    - test
    - data
  script:
    - rm -rf public
    - mkdir -p public
    - mv docs/build/html/* public
    - mv coverage public
    - mv rascil_data.tgz public
    - ls -l public
  artifacts:
    paths:
      - public
    expire_in: 30 days
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PIPELINE_SOURCE != "schedule" && ($CI_COMMIT_TAG == "" || $CI_COMMIT_TAG == null)'

# Publish dev version package
publish_to_registry_dev:
  stage: publish
  before_script:
    - pip3 config set global.index-url https://artifact.astrolab.cn/repository/pypi-proxy/simple
    - pip3 install --upgrade pip
  script:
    - echo "Commit tag is ${CI_COMMIT_TAG}"
    - echo "Commit message is ${CI_COMMIT_MESSAGE}"
    - pip install twine
    - TWINE_PASSWORD=${CI_PYPI_TOKEN} TWINE_USERNAME=gitlab-ci-token python -m twine upload --repository-url https://gitlab.astrolab.cn/api/v4/projects/${CI_PROJECT_ID}/packages/pypi dist/*
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PIPELINE_SOURCE != "schedule" && ($CI_COMMIT_TAG == "" || $CI_COMMIT_TAG == null)'

# Publish tag version package
publish_to_registry:
  stage: publish
  before_script:
    - pip3 config set global.index-url https://artifact.astrolab.cn/repository/pypi-proxy/simple
    - pip3 install --upgrade pip
  script:
    - echo "Commit tag is ${CI_COMMIT_TAG}"
    - echo "Commit message is ${CI_COMMIT_MESSAGE}"
    - pip install twine
    - TWINE_PASSWORD=${CI_PYPI_TOKEN} TWINE_USERNAME=gitlab-ci-token python -m twine upload --repository-url https://gitlab.astrolab.cn/api/v4/projects/${CI_PROJECT_ID}/packages/pypi dist/*
  rules:
    - if: '$CI_COMMIT_TAG'

# Publish docker:<version> (release) to Central Artefact Repository if this is a tagged build of the master
docker_release:
  stage: publish
  tags:
    - astrolab-docker
  image: docker:20.10.7
  services:
    - name: registry.astrolab.cn/ska-sdp-china/rascil2/dockerdindcustom:0.2
      alias: docker
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_TLS_CERTDIR: ""
    GIT_VERSION: $CI_COMMIT_SHORT_SHA
  before_script:
    - sed -i 's/dl-cdn.alpinelinux.org/mirrors.tuna.tsinghua.edu.cn/g' /etc/apk/repositories
    - apk add make git
    - cp dist/*whl docker/rascil2-base
    - cd docker
  script:
    - make build_all_latest_with_hosts
    - make test_all
    - echo $CI_REGISTRY_PASSWORD | docker login --username $CI_REGISTRY_USER --password-stdin registry.astrolab.cn
    - make push_all_release
  rules:
    - if: '$CI_COMMIT_TAG'


prepare ci metrics:
  stage: prepost
  image: $SKA_TANGO_IMAGES_PYTANGO
  script:
    - mkdir -p build/reports
    - coverage combine coverage_* && coverage xml && coverage html -d coverage && coverage report
    - test -a coverage.xml && mv coverage.xml ./build/reports/code-coverage.xml
    - test -a linting.xml && mv linting.xml ./build/reports/linting.xml
    - python3 util/xmlcombine.py unit-tests-dask.xml unit-tests-other.xml > unit-tests.xml
    - test -a unit-tests.xml && mv unit-tests.xml ./build/reports/unit-tests.xml
  artifacts:
    paths:
      - coverage
      - ./build
  rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule" && ($CI_COMMIT_TAG == "" || $CI_COMMIT_TAG == null)'


create-ci-metrics:
  stage: .post
  image: $SKA_K8S_TOOLS_DEPLOY_IMAGE
  cache: {}
  when: always
  tags:
    - cnlab
  script:
    # Gitlab CI badges creation: START
    - sed -i "s|http://archive.ubuntu.com/ubuntu|https://artifact.astrolab.cn/repository/apt-ubuntu|g" /etc/apt/sources.list
    - apt-get -y update
    - apt-get install -y curl --no-install-recommends
    - "curl --retry 30 --retry-delay 3 --header \"PRIVATE-TOKEN: $CI_JOB_TOKEN\" -s https://gitlab.astrolab.cn/ska-sdp-china/ska-cicd-ci-metrics-utilities/-/raw/master/ska_ci_metrics_utilities/ci-badges-func.sh | sh"
      # Gitlab CI badges creation: END
  artifacts:
    paths:
      - ./build
  allow_failure: true
  rules:  
    - if: '$CI_PIPELINE_SOURCE != "schedule" && ($CI_COMMIT_TAG == "" || $CI_COMMIT_TAG == null)'


include:
  - local: 'docker/kubernetes/ci_test.yaml'


  