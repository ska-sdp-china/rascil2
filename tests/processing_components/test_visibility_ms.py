"""Unit tests for visibility operations"""

import sys
import unittest
import logging
import shutil

import numpy

from rascil2.data_models import rascil2_path, rascil2_data_path, Visibility
from rascil2.processing_components.io.base import (
    create_visibility_from_ms,
    export_visibility_to_ms,
)
from rascil2.processing_components.visibility.operations import (
    integrate_visibility_by_channel,
)

log = logging.getLogger("rascil2-logger")

log.setLevel(logging.WARNING)
log.addHandler(logging.StreamHandler(sys.stdout))


class TestCreateMS(unittest.TestCase):
    def setUp(self):
        try:
            from casacore.tables import table  # pylint: disable=import-error

            self.casacore_available = True
        #            except ModuleNotFoundError:
        except:
            self.casacore_available = False

    def test_create_list(self):
        if not self.casacore_available:
            return

        msfile = rascil2_data_path("vis/xcasa.ms")
        self.vis = create_visibility_from_ms(msfile)

        for v in self.vis:
            assert v.vis.data.shape[-1] == 4
            assert v.visibility_acc.polarisation_frame.type == "circular"

    def test_create_list_spectral(self):
        if not self.casacore_available:
            return

        msfile = rascil2_data_path("vis/ASKAP_example.ms")

        vis_by_channel = list()
        nchan_ave = 16
        nchan = 192
        for schan in range(0, nchan, nchan_ave):
            max_chan = min(nchan, schan + nchan_ave)
            v = create_visibility_from_ms(msfile, range(schan, max_chan))
            vis_by_channel.append(v[0])

        assert len(vis_by_channel) == 12
        for v in vis_by_channel:
            assert v.vis.data.shape[-1] == 4
            assert v.visibility_acc.polarisation_frame.type == "linear"

    def test_create_list_slice(self):
        if not self.casacore_available:
            return

        msfile = rascil2_data_path("vis/ASKAP_example.ms")

        vis_by_channel = list()
        nchan_ave = 16
        nchan = 192
        for schan in range(0, nchan, nchan_ave):
            max_chan = min(nchan, schan + nchan_ave)
            v = create_visibility_from_ms(
                msfile, start_chan=schan, end_chan=max_chan - 1
            )
            assert v[0].vis.shape[-2] == nchan_ave
            vis_by_channel.append(v[0])

        assert len(vis_by_channel) == 12
        for v in vis_by_channel:
            assert v.vis.data.shape[-1] == 4
            assert v.visibility_acc.polarisation_frame.type == "linear"

    def test_create_list_slice_visibility(self):
        if not self.casacore_available:
            return

        msfile = rascil2_data_path("vis/ASKAP_example.ms")

        vis_by_channel = list()
        nchan_ave = 16
        nchan = 192
        for schan in range(0, nchan, nchan_ave):
            max_chan = min(nchan, schan + nchan_ave)
            v = create_visibility_from_ms(
                msfile, start_chan=schan, end_chan=max_chan - 1
            )
            nchannels = len(numpy.unique(v[0].frequency))
            assert nchannels == nchan_ave
            vis_by_channel.append(v[0])

        assert len(vis_by_channel) == 12
        for v in vis_by_channel:
            assert v.vis.data.shape[-1] == 4
            assert v.visibility_acc.polarisation_frame.type == "linear"
            assert numpy.max(numpy.abs(v.vis)) > 0.0
            assert numpy.max(numpy.abs(v.visibility_acc.flagged_vis)) > 0.0
            assert numpy.sum(v.weight) > 0.0
            assert numpy.sum(v.visibility_acc.flagged_weight) > 0.0

    def test_create_list_average_slice_visibility(self):
        if not self.casacore_available:
            return

        msfile = rascil2_data_path("vis/ASKAP_example.ms")

        vis_by_channel = list()
        nchan_ave = 16
        nchan = 192
        for schan in range(0, nchan, nchan_ave):
            max_chan = min(nchan, schan + nchan_ave)
            v = create_visibility_from_ms(
                msfile, start_chan=schan, end_chan=max_chan - 1, average_channels=True
            )
            nchannels = len(numpy.unique(v[0].frequency))
            assert nchannels == 1
            vis_by_channel.append(v[0])

        assert len(vis_by_channel) == 12
        for ivis, v in enumerate(vis_by_channel):
            assert v.vis.data.shape[-1] == 4
            assert v.visibility_acc.polarisation_frame.type == "linear"
            assert numpy.max(numpy.abs(v.vis)) > 0.0, ivis
            assert numpy.max(numpy.abs(v.visibility_acc.flagged_vis)) > 0.0, ivis
            assert numpy.sum(v.weight) > 0.0, ivis
            assert numpy.sum(v.visibility_acc.flagged_weight) > 0.0, ivis

    def test_create_list_single(self):
        if not self.casacore_available:
            return

        msfile = rascil2_data_path("vis/ASKAP_example.ms")

        vis_by_channel = list()
        nchan_ave = 1
        nchan = 8
        for schan in range(0, nchan, nchan_ave):
            max_chan = min(nchan, schan + nchan_ave)
            v = create_visibility_from_ms(msfile, start_chan=schan, end_chan=schan)
            vis_by_channel.append(v[0])

        assert len(vis_by_channel) == 8, len(vis_by_channel)
        for v in vis_by_channel:
            assert v.vis.data.shape[-1] == 4
            assert v.visibility_acc.polarisation_frame.type == "linear"

    def test_create_list_spectral_average(self):
        if not self.casacore_available:
            return

        msfile = rascil2_data_path("vis/ASKAP_example.ms")

        vis_by_channel = list()
        nchan_ave = 16
        nchan = 192
        for schan in range(0, nchan, nchan_ave):
            max_chan = min(nchan, schan + nchan_ave)
            v = create_visibility_from_ms(
                msfile, range(schan, max_chan), average_channels=True
            )
            vis_by_channel.append(v[0])

        assert len(vis_by_channel) == 12
        for v in vis_by_channel:
            assert v.vis.data.shape[-1] == 4
            assert v.vis.data.shape[-2] == 1
            assert v.visibility_acc.polarisation_frame.type == "linear"
            assert numpy.max(numpy.abs(v.vis)) > 0.0
            assert numpy.max(numpy.abs(v.visibility_acc.flagged_vis)) > 0.0

    def test_read_all(self):
        ms_list = ["vis/3C277.1C.16channels.ms", "vis/ASKAP_example.ms", "vis/xcasa.ms"]

        for ms in ms_list:
            vis_list = create_visibility_from_ms(rascil2_data_path(ms))
            assert isinstance(vis_list[0], Visibility)

    def test_read_not_ms(self):
        with self.assertRaises(RuntimeError):
            ms = "vis/ASKAP_example.fits"
            vis_list = create_visibility_from_ms(rascil2_data_path(ms))
            # assert isinstance(vis_list[0], Visibility)

    def test_write_multi_bvis_ms(self):
        # read in an MS which produces multiple bvis
        bvis_list = create_visibility_from_ms(rascil2_path("data/vis/xcasa.ms"))

        # export the list without any changes
        export_visibility_to_ms("out.ms", bvis_list)
        # verify data
        bvis_list_2 = create_visibility_from_ms("out.ms")
        # there are 14 elements in the list and we test half to decrease the time it takes for the test to run
        for i in range(0, len(bvis_list), 2):
            assert bvis_list[i] == bvis_list_2[i]
        shutil.rmtree("./out.ms", ignore_errors=True)

    def test_export_regularisation_visibility_to_ms(self):
        from rascil2.processing_components.io.regular_ms import (
            export_regularisation_visibility_to_ms,
        )

        bvis_list = create_visibility_from_ms(rascil2_path("data/vis/xcasa.ms"))
        export_regularisation_visibility_to_ms(bvis_list, "testnew.ms")
        bvis_list_2 = create_visibility_from_ms("testnew.ms")
        assert len(bvis_list) == len(bvis_list_2)
        assert len(bvis_list_2) == 14
        for vis1, vis2 in zip(bvis_list, bvis_list_2):
            assert numpy.allclose(vis1["vis"].data, vis2["vis"].data)
            assert numpy.allclose(vis1["uvw"].data, vis2["uvw"].data)
            assert numpy.allclose(vis1.time.data, vis2.time.data)
            assert numpy.allclose(vis1.frequency.data, vis2.frequency.data)
            assert numpy.allclose(vis1.phasecentre.ra.rad, vis2.phasecentre.ra.rad)
            assert numpy.allclose(vis1.phasecentre.dec.rad, vis2.phasecentre.dec.rad)
        shutil.rmtree("./testnew.ms", ignore_errors=True)

    def test_export_regularisation_visibility_to_ms_ovsa_data(self):
        from rascil2.processing_components.io.regular_ms import (
            export_regularisation_visibility_to_ms,
            create_visibility_from_ms_regularisation,
        )

        bvis_list = create_visibility_from_ms_regularisation(
            rascil2_path("data/vis/small_ovsa_IDB20241103_1751-1758XXYY_selfcaled.ms"),
            ifauto=True,
        )
        export_regularisation_visibility_to_ms(bvis_list, "testnew_ovsa.ms")
        bvis_list_2 = create_visibility_from_ms_regularisation(
            "testnew_ovsa.ms", ifauto=True
        )
        assert len(bvis_list) == len(bvis_list_2)
        assert len(bvis_list_2) == 2
        for vis1, vis2 in zip(bvis_list, bvis_list_2):
            assert numpy.allclose(vis1.vis.data, vis2.vis.data, equal_nan=True)
            assert numpy.allclose(vis1.weight.data, vis2.weight.data)
            assert numpy.allclose(vis1.uvw.data, vis2.uvw.data)
            assert numpy.allclose(vis1.time.data, vis2.time.data)
            assert numpy.allclose(vis1.frequency.data, vis2.frequency.data)
            assert numpy.allclose(vis1.phasecentre.ra.rad, vis2.phasecentre.ra.rad)
            assert numpy.allclose(vis1.phasecentre.dec.rad, vis2.phasecentre.dec.rad)
        shutil.rmtree("./testnew_ovsa.ms", ignore_errors=True)

    def test_create_visibility_from_ms_regularisation(self):
        """
        Test creating a visibility from a regularisation visibility
        """
        import pandas
        from rascil2.processing_components.io.regular_ms import (
            create_visibility_from_ms_regularisation,
        )

        def uvw_or_vis_equal(vis1, vis2, key):
            v2baseline = pandas.MultiIndex.from_tuples(vis2.baselines.data)
            for v1row, v1baseline in enumerate(vis1.baselines.data):
                a1, a2 = v1baseline
                try:
                    v2row = v2baseline.get_loc((a1, a2))
                except KeyError:
                    v2row = -1

                if v2row != -1:
                    assert numpy.allclose(
                        vis1[key].data[:, v1row], vis2[key].data[:, v2row]
                    )

        filled_tmp_table_prefix = rascil2_path("data/vis/xcasa.ms").replace(
            "xcasa", ".xcasa_filled_tmp"
        )

        bvis_list = create_visibility_from_ms(
            rascil2_path("data/vis/xcasa.ms"),
        )
        bvis_list_2 = create_visibility_from_ms_regularisation(
            rascil2_path("data/vis/xcasa.ms"),
            filled_tmp_table_prefix=filled_tmp_table_prefix,
        )

        for vis1, vis2 in zip(bvis_list, bvis_list_2):
            uvw_or_vis_equal(vis1, vis2, "uvw")
            uvw_or_vis_equal(vis1, vis2, "vis")
            nants = vis1.configuration.configuration_acc.nants
            assert (
                vis1.visibility_acc.nbaselines - vis2.visibility_acc.nbaselines == nants
            )
            assert numpy.allclose(vis1.time.data, vis2.time.data)
            assert numpy.allclose(vis1.frequency.data, vis2.frequency.data)
            assert numpy.allclose(vis1.phasecentre.ra.rad, vis2.phasecentre.ra.rad)
            assert numpy.allclose(vis1.phasecentre.dec.rad, vis2.phasecentre.dec.rad)
        shutil.rmtree(filled_tmp_table_prefix, ignore_errors=True)

    def test_create_visibility_from_ms_regularisation_weight(self):
        """
        Test creating a visibility from a regularisation visibility
        """
        import pandas
        from rascil2.processing_components.io.regular_ms import (
            create_visibility_from_ms_regularisation,
        )

        def uvw_or_vis_equal(vis1, vis2, key):
            v2baseline = pandas.MultiIndex.from_tuples(vis2.baselines.data)
            for v1row, v1baseline in enumerate(vis1.baselines.data):
                a1, a2 = v1baseline
                try:
                    v2row = v2baseline.get_loc((a1, a2))
                except KeyError:
                    v2row = -1

                if v2row != -1:
                    assert numpy.allclose(
                        vis1[key].data[:, v1row], vis2[key].data[:, v2row]
                    )

        filled_tmp_table_prefix = rascil2_path("data/vis/ASKAP_example.ms").replace(
            "ASKAP_example", ".askap_filled_tmp"
        )

        bvis_list = create_visibility_from_ms(
            rascil2_path("data/vis/ASKAP_example.ms"),
        )
        bvis_list_2 = create_visibility_from_ms_regularisation(
            rascil2_path("data/vis/ASKAP_example.ms"),
            filled_tmp_table_prefix=filled_tmp_table_prefix,
            ifauto=True,
        )

        for vis1, vis2 in zip(bvis_list, bvis_list_2):
            uvw_or_vis_equal(vis1, vis2, "uvw")
            uvw_or_vis_equal(vis1, vis2, "vis")
            nants = vis1.configuration.configuration_acc.nants
            assert vis1.visibility_acc.nbaselines - vis2.visibility_acc.nbaselines == 0
            assert numpy.allclose(vis1.time.data, vis2.time.data)
            assert numpy.allclose(vis1.frequency.data, vis2.frequency.data)
            assert numpy.allclose(vis1.phasecentre.ra.rad, vis2.phasecentre.ra.rad)
            assert numpy.allclose(vis1.phasecentre.dec.rad, vis2.phasecentre.dec.rad)
        shutil.rmtree(filled_tmp_table_prefix, ignore_errors=True)

    def test_create_visibility_from_ms_regularisation_ovsa_data(self):
        """
        Test creating a visibility from a regularisation visibility
        with OVSA data, spw is different, StManIndArray::get/put
        """

        from rascil2.processing_components.io.regular_ms import (
            create_visibility_from_ms_regularisation,
        )

        msname = rascil2_path(
            "data/vis/small_ovsa_IDB20241103_1751-1758XXYY_selfcaled.ms"
        )

        vis = create_visibility_from_ms_regularisation(msname, ifauto=True)

        assert len(vis) == 2
        assert vis[0].vis.data.shape == (874, 136, 12, 1)
        assert vis[1].vis.data.shape == (874, 136, 10, 1)

        assert numpy.isclose(numpy.nansum(numpy.abs(vis[0].vis.data)), 425417870000.0)
        assert numpy.isclose(numpy.nansum(numpy.abs(vis[1].vis.data)), 368787520000.0)

        assert numpy.isclose(
            numpy.nansum(numpy.abs(vis[0].weight.data)), 356591.9787454605
        )
        assert numpy.isclose(
            numpy.nansum(numpy.abs(vis[1].weight.data)), 297159.99531149864
        )

        assert numpy.isclose(
            numpy.nansum(numpy.abs(vis[0].uvw.data)), 68691131.04039447
        )
        assert numpy.isclose(
            numpy.nansum(numpy.abs(vis[1].uvw.data)), 68691131.04039447
        )

    def test_create_visibility_from_ms_ovsa_data(self):
        """
        Test creating a visibility from a regularisation visibility
        with OVSA data, spw is different, StManIndArray::get/put
        """

        msname = rascil2_path(
            "data/vis/small_ovsa_IDB20241103_1751-1758XXYY_selfcaled.ms"
        )

        vis = create_visibility_from_ms(msname)

        assert len(vis) == 2
        assert vis[0].vis.data.shape == (874, 136, 12, 1)
        assert vis[1].vis.data.shape == (874, 136, 10, 1)

        assert numpy.isclose(numpy.nansum(numpy.abs(vis[0].vis.data)), 425417870000.0)
        assert numpy.isclose(numpy.nansum(numpy.abs(vis[1].vis.data)), 368787520000.0)

        assert numpy.isclose(
            numpy.nansum(numpy.abs(vis[0].weight.data)), 356591.9787454605
        )
        assert numpy.isclose(
            numpy.nansum(numpy.abs(vis[1].weight.data)), 297159.99531149864
        )

        assert numpy.isclose(
            numpy.nansum(numpy.abs(vis[0].uvw.data)), 68691131.04039447
        )
        assert numpy.isclose(
            numpy.nansum(numpy.abs(vis[1].uvw.data)), 68691131.04039447
        )


if __name__ == "__main__":
    unittest.main()
