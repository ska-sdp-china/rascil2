# pylint: disable=E1101

"""Unit test for the uvfits module."""

import os
import time
import shutil
import tempfile
import unittest

import numpy

from rascil2.data_models.memory_data_models import Configuration
from rascil2.data_models.polarisation import ReceptorFrame
from rascil2.processing_components.util import ecef_to_enu

from astropy.coordinates import EarthLocation
from astropy import units as u
from astropy.time import Time
from astropy.io import fits as astrofits

from rascil2.processing_components.io.basedata import Stand, Antenna
from rascil2.processing_components.io import uvfits


class uvfits_tests(unittest.TestCase):
    """A unittest.TestCase collection of unit tests for the lsl.writer.measurementset.Ms
    class."""

    testPath = None

    def setUp(self):
        """Turn off all numpy warnings and create the temporary file directory."""

        numpy.seterr(all="ignore")
        self.testPath = tempfile.mkdtemp(prefix="test-uvfits-", suffix=".tmp")

    def __initData_WGS84(self):
        """Private function to generate a random set of data for writing a UVFITS
        file.  The data is returned as a dictionary with keys:
         * freq - frequency array in Hz
         * site - Observatory object
         * stands - array of stand numbers
         * bl - list of baseline pairs in real stand numbers
         * vis - array of visibility data in baseline x freq format
        """
        # Frequency range
        freq = numpy.arange(0, 512) * 20e6 / 512 + 40e6
        channel_width = numpy.full_like(freq, 20e6 / 512.0)

        # Site and stands
        obs = EarthLocation(
            lon=116.76444824 * u.deg, lat=-26.824722084 * u.deg, height=300.0
        )

        mount = numpy.array(
            [
                "equat",
                "equat",
                "equat",
                "equat",
                "equat",
                "equat",
                "equat",
                "equat",
                "equat",
                "equat",
            ]
        )
        names = numpy.array(
            [
                "ak02",
                "ak04",
                "ak05",
                "ak12",
                "ak13",
                "ak14",
                "ak16",
                "ak24",
                "ak28",
                "ak30",
            ]
        )
        diameter = numpy.array(
            [12.0, 12.0, 12.0, 12.0, 12.0, 12.0, 12.0, 12.0, 12.0, 12.0]
        )
        xyz = numpy.array(
            [
                [-2556109.98244348, 5097388.70050131, -2848440.1332423],
                [-2556087.396082, 5097423.589662, -2848396.867933],
                [-2556028.60254059, 5097451.46195695, -2848399.83113161],
                [-2556496.23893101, 5097333.71466669, -2848187.33832738],
                [-2556407.35299627, 5097064.98390756, -2848756.02069474],
                [-2555972.78456557, 5097233.65481756, -2848839.88915184],
                [-2555592.88867802, 5097835.02121109, -2848098.26409648],
                [-2555959.34313275, 5096979.52802882, -2849303.57702486],
                [-2556552.97431815, 5097767.23612874, -2847354.29540396],
                [-2557348.40370367, 5097170.17682775, -2847716.21368966],
            ]
        )

        site_config = Configuration.constructor(
            name="ASKAP",
            location=obs,
            names=names,
            xyz=xyz,
            mount=mount,
            frame="WGS84",
            receptor_frame=ReceptorFrame("linear"),
            diameter=diameter,
        )
        antennas = []
        for i in range(len(names)):
            antennas.append(
                Antenna(i, Stand(names[i], xyz[i, 0], xyz[i, 1], xyz[i, 2]))
            )

        # Set baselines and data
        blList = []
        N = len(antennas)

        antennas2 = antennas

        for i in range(0, N - 1):
            for j in range(i + 1, N):
                blList.append((antennas[i], antennas2[j]))

        visData = numpy.random.rand(len(blList), len(freq))
        visData = visData.astype(numpy.complex64)
        flags = numpy.zeros_like(visData)
        weights = numpy.random.rand(len(blList), len(freq))
        return {
            "freq": freq,
            "channel_width": channel_width,
            "site": site_config,
            "antennas": antennas,
            "bl": blList,
            "vis": visData,
            "flags": flags,
            "weights": weights,
            "xyz": xyz,
            "obs": obs,
        }

    def __initData_ENU(self):
        """Private function to generate a random set of data for writing a Measurements
        file.  The data is returned as a dictionary with keys:
         * freq - frequency array in Hz
         * site - observatory object
         * stands - array of stand numbers
         * bl - list of baseline pairs in real stand numbers
         * vis - array of visibility data in baseline x freq format
        """

        # Frequency range
        freq = numpy.arange(0, 512) * 20e6 / 512 + 40e6
        channel_width = numpy.full_like(freq, 20e6 / 512.0)

        # Site and stands
        obs = EarthLocation(
            lon=+116.6356824 * u.deg, lat=-26.70130064 * u.deg, height=377.0
        )

        names = numpy.array(["A%02d" % i for i in range(36)])
        mount = numpy.array(["equat" for i in range(36)])
        diameter = numpy.array([12 for i in range(36)])
        xyz = numpy.array(
            [
                [-175.233429, +1673.460938, 0.0000],
                [+261.119019, +796.922119, 0.0000],
                [-29.2005200, +744.432068, 0.0000],
                [-289.355286, +586.936035, 0.0000],
                [-157.031570, +815.570068, 0.0000],
                [-521.311646, +754.674927, 0.0000],
                [-1061.114258, +840.541443, 0.0000],
                [-921.829407, +997.627686, 0.0000],
                [-818.293579, +1142.272095, 0.0000],
                [-531.752808, +850.726257, 0.0000],
                [+81.352448, +790.245117, 0.0000],
                [+131.126358, +1208.831909, 0.0000],
                [-1164.709351, +316.779236, 0.0000],
                [-686.248901, +590.285278, 0.0000],
                [-498.987305, +506.338226, 0.0000],
                [-182.249146, +365.113464, 0.0000],
                [+420.841858, +811.081543, 0.0000],
                [+804.107910, +1273.328369, 0.0000],
                [-462.810394, +236.353790, 0.0000],
                [-449.772339, +15.039755, 0.0000],
                [+13.791821, +110.971809, 0.0000],
                [-425.687317, -181.908752, 0.0000],
                [-333.404053, -503.603394, 0.0000],
                [-1495.472412, +1416.063232, 0.0000],
                [-1038.578857, +1128.367920, 0.0000],
                [-207.151749, +956.312561, 0.0000],
                [-389.051880, +482.405670, 0.0000],
                [-434.000000, +510.000000, 0.0000],
                [-398.000000, +462.000000, 0.0000],
                [-425.000000, +475.000000, 0.0000],
                [-400.000000, +3664.000000, 0.0000],
                [+1796.000000, +1468.000000, 0.0000],
                [+2600.000000, -1532.000000, 0.0000],
                [-400.000000, -2336.000000, 0.0000],
                [-3400.00000, -1532.000000, 0.0000],
                [-2596.000000, +1468.000000, 0.0000],
            ]
        )

        site_config = Configuration.constructor(
            name="ASKAP",
            location=obs,
            names=names,
            xyz=xyz,
            mount=mount,
            frame="ENU",
            receptor_frame=ReceptorFrame("linear"),
            diameter=diameter,
        )
        antennas = []
        for i in range(len(names)):
            antennas.append(
                Antenna(i, Stand(names[i], xyz[i, 0], xyz[i, 1], xyz[i, 2]))
            )

        # Set baselines and data
        blList = []
        N = len(antennas)

        antennas2 = antennas

        for i in range(0, N - 1):
            for j in range(i + 1, N):
                blList.append((antennas[i], antennas2[j]))

        visData = numpy.random.rand(len(blList), len(freq))
        visData = visData.astype(numpy.complex64)
        flags = numpy.zeros_like(visData)

        weights = numpy.random.rand(len(blList), len(freq))
        return {
            "freq": freq,
            "channel_width": channel_width,
            "site": site_config,
            "antennas": antennas,
            "bl": blList,
            "vis": visData,
            "flags": flags,
            "weights": weights,
            "xyz": xyz,
            "obs": obs,
        }

    def test_write_tables_ENU(self):
        """Test if the uvfits writes all of the tables."""

        testTime = float(Time("2000-01-01T00:00:00.000", format="isot").unix)
        testFile = os.path.join(self.testPath, "test-W.fits")

        # Get some data
        data = self.__initData_ENU()

        # Start the table
        tbl = uvfits.Uv(testFile, ref_time=testTime, frame=data["site"].attrs["frame"])
        tbl.set_stokes(["xx"])
        tbl.set_frequency(data["freq"], data["channel_width"])
        tbl.set_geometry(data["site"], data["antennas"])
        tbl.add_data_set(testTime, 2.0, data["bl"], data["vis"], data["flags"])

        # Judge if the tbl's antenna is correctly positioned
        antxyz_ecef = numpy.zeros((len(data["antennas"]), 3))
        for i, ant in enumerate(tbl.array[0]["ants"]):
            antxyz_ecef[i][0] = ant.x
            antxyz_ecef[i][1] = ant.y
            antxyz_ecef[i][2] = ant.z

        # convert back to enu
        antxyz_enu = ecef_to_enu(data["obs"], antxyz_ecef)
        assert numpy.allclose(antxyz_enu, data["xyz"])

        tbl.write()
        tbl.close()

        # Make sure everyone is there
        self.assertTrue(os.path.exists(testFile))

    def test_write_tables_WGS84(self):
        """Test if the MeasurementSet writer writes all of the tables."""

        testTime = float(Time("2000-01-01T00:00:00.000", format="isot").unix)
        testFile = os.path.join(self.testPath, "test-WGS.fits")

        # Get some data
        data = self.__initData_WGS84()

        # Start the table
        tbl = uvfits.Uv(testFile, ref_time=testTime, frame=data["site"].attrs["frame"])
        tbl.set_stokes(["xx"])
        tbl.set_frequency(data["freq"], data["channel_width"])
        tbl.set_geometry(data["site"], data["antennas"])
        tbl.add_data_set(testTime, 2.0, data["bl"], data["vis"], data["flags"])

        # Judge if the tbl's antenna is correctly positioned
        antxyz_ecef = numpy.zeros((len(data["antennas"]), 3))
        for i, ant in enumerate(tbl.array[0]["ants"]):
            antxyz_ecef[i][0] = ant.x
            antxyz_ecef[i][1] = ant.y
            antxyz_ecef[i][2] = ant.z

        # the antxyz in table should be same as data['xyz']
        assert numpy.allclose(antxyz_ecef, data["xyz"])

        tbl.write()
        tbl.close()

        # Make sure everyone is there
        self.assertTrue(os.path.exists(testFile))

        # Open the file and examine
        hdulist = astrofits.open(testFile)
        # Check that all of the extensions are there
        extNames = [hdu.name for hdu in hdulist]
        for ext in ["AIPS AN", "AIPS FQ", "AIPS SU"]:
            self.assertTrue(ext in extNames)
        # Check the comments and history
        # self.assertTrue('This is a comment' in str(hdulist[0].header['COMMENT']).split('\n'))
        # self.assertTrue('This is history' in str(hdulist[0].header['HISTORY']).split('\n'))

        ag = hdulist[1].data
        # Correct number of stands
        self.assertEqual(len(data["antennas"]), len(ag.field("NOSTA")))

        # Correct stand names
        names = [ant.stand.id for ant in data["antennas"]]
        for name, anname in zip(names, ag.field("ANNAME")):
            self.assertEqual(name, anname)

        hdulist.close()

        # Open the file and examine
        hdulist = astrofits.open(testFile)
        uv = hdulist[0]

        # Load the mapper
        try:
            mp = hdulist["NOSTA_MAPPER"].data
            nosta = mp.field("NOSTA")
            noact = mp.field("NOACT")
        except KeyError:
            ag = hdulist["AIPS AN"].data
            nosta = ag.field("NOSTA")
            noact = ag.field("NOSTA")
        mapper = {}
        for s, a in zip(nosta, noact):
            mapper[s] = a

        # Correct number of visibilities
        self.assertEqual(len(uv.data), data["vis"].shape[0])

        # Correct number of frequencies
        for row in uv.data:
            vis = row["DATA"][0, 0, :, :, :]
            self.assertEqual(len(vis), len(data["freq"]))

        # Correct values
        for row in uv.data:
            bl = int(row["BASELINE"])
            vis = row["DATA"][0, 0, :, :, :]

            # Unpack the baseline
            if bl >= 65536:
                a1 = int((bl - 65536) / 2048)
                a2 = int((bl - 65536) % 2048)
            else:
                a1 = int(bl / 256)
                a2 = int(bl % 256)

            # Convert mapped stands to real stands
            stand1 = mapper[a1]
            stand2 = mapper[a2]

            # Find out which visibility set in the random data corresponds to the
            # current visibility
            i = 0

            # Extract the data and run the comparison
            visData = numpy.zeros(len(data["freq"]), dtype=numpy.complex64)
            visData.real = vis[:, 0, 0]
            visData.imag = vis[:, 0, 1]
            numpy.testing.assert_allclose(visData, data["vis"][i, :])
            break

        hdulist.close()

    def test_array_geometry(self):
        """Test the 'AIPS AN' table, part 1."""

        testTime = float(Time("2000-01-01T00:00:00.000", format="isot").unix)
        testFile = os.path.join(self.testPath, "test-W.fits")

        # Get some data
        data = self.__initData_ENU()

        # Start the table
        tbl = uvfits.Uv(testFile, ref_time=testTime, frame=data["site"].attrs["frame"])
        tbl.set_stokes(["xx"])
        tbl.set_frequency(data["freq"], data["channel_width"])
        tbl.set_geometry(data["site"], data["antennas"])
        tbl.add_data_set(testTime, 2.0, data["bl"], data["vis"], data["flags"])

        tbl.write()
        tbl.close()

        # Open the file and examine
        hdulist = astrofits.open(testFile)
        ag = hdulist["AIPS AN"].data
        # Correct number of stands
        self.assertEqual(len(data["antennas"]), len(ag.field("NOSTA")))

        # Correct stand names
        names = [ant.stand.id for ant in data["antennas"]]
        for name, anname in zip(names, ag.field("ANNAME")):
            self.assertEqual(name, anname)

        hdulist.close()

    def test_frequency(self):
        """Test the 'AIPS FQ' table."""

        testTime = float(Time("2000-01-01T00:00:00.000", format="isot").unix)
        testFile = os.path.join(self.testPath, "test-W.fits")

        # Get some data
        data = self.__initData_ENU()

        # Start the table
        tbl = uvfits.Uv(testFile, ref_time=testTime, frame=data["site"].attrs["frame"])
        tbl.set_stokes(["xx"])
        tbl.set_frequency(data["freq"], data["channel_width"])
        tbl.set_geometry(data["site"], data["antennas"])
        tbl.add_data_set(testTime, 2.0, data["bl"], data["vis"], data["flags"])

        tbl.write()
        tbl.close()

        # Open the file and examine
        hdulist = astrofits.open(testFile)
        fq = hdulist["AIPS FQ"].data
        # Correct number of IFs
        self.assertEqual(len(fq.field("FRQSEL")), 1)
        # Correct channel width
        self.assertEqual(fq.field("CH WIDTH")[0], data["freq"][1] - data["freq"][0])
        # Correct bandwidth
        self.assertEqual(
            fq.field("TOTAL BANDWIDTH")[0],
            numpy.abs(data["freq"][-1] - data["freq"][0]).astype(numpy.float32),
        )

    def tearDown(self):
        """Remove the test path directory and its contents"""

        shutil.rmtree(self.testPath, ignore_errors=True)


class UvfitsTestSuite(unittest.TestSuite):
    """A unittest.TestSuite class which contains tests."""

    def __init__(self):
        unittest.TestSuite.__init__(self)

        loader = unittest.TestLoader()
        self.addTests(loader.loadTestsFromTestCase(uvfits_tests))


if __name__ == "__main__":
    unittest.main()
