"""Unit tests for image operations"""

import logging
import unittest

import numpy

from rascil2.processing_components.griddata.operations import create_griddata_from_image
from rascil2.processing_components.simulation import create_test_image
from rascil2.processing_components.image.operations import create_image

log = logging.getLogger("rascil2-logger")

log.setLevel(logging.WARNING)


class TestGridData(unittest.TestCase):
    def setUp(self):
        from rascil2.data_models.parameters import rascil2_path

        self.results_dir = rascil2_path("test_results")

        self.m31image = create_test_image(cellsize=0.0001)
        self.cellsize = 180.0 * 0.0001 / numpy.pi
        self.phase_centre = self.m31image.image_acc.phasecentre

    def test_create_griddata_from_image(self):
        m31model_by_image = create_griddata_from_image(self.m31image)
        assert (
            m31model_by_image.griddata_acc.shape[0] == self.m31image.image_acc.shape[0]
        )
        assert (
            m31model_by_image.griddata_acc.shape[1] == self.m31image.image_acc.shape[1]
        )
        assert (
            m31model_by_image.griddata_acc.shape[2] == self.m31image.image_acc.shape[2]
        )
        assert (
            m31model_by_image.griddata_acc.shape[3] == self.m31image.image_acc.shape[3]
        )

    def test_create_griddata_from_image_single_channel(self):
        """
        Test creating GridData from an image with specified
        frequency and only a single channel/band.
        The wcs information should correctly return the
        bandwidth.
        """

        n_pixels = 512
        cell_size = 0.000015
        frequency = 50.0e6
        bandwidth = 300.0e6
        nchan = 1

        test_image = create_image(
            n_pixels,
            cell_size,
            phasecentre=self.phase_centre,
            frequency=[frequency],
            channel_bandwidth=[bandwidth],
            nchan=nchan,
        )

        result = create_griddata_from_image(test_image)

        assert result.griddata_acc.griddata_wcs.wcs.cdelt[3] == bandwidth


if __name__ == "__main__":
    unittest.main()
