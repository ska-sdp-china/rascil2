"""Unit test for the lsl.catalog module."""

import unittest
from rascil2.processing_components.util import catalog


class Test_catalog(unittest.TestCase):
    """A unittest.TestCase collection of unit tests for the lsl.catalog
    module."""

    def test_constructor(self):
        """Test catalog constructors."""

        for name in ("USER", "3C", "4C", "JPL", "PSR", "PKS", "PKS90"):
            with self.subTest(name=name):
                catalog.CatalogFactory.get_catalog(name)

    def test_get_names(self):
        """Test catalog.CatalogFactory.get_names() method."""

        names = catalog.CatalogFactory.get_names()
        self.assertTrue("3C" in names)
        self.assertTrue("NONSENSE" not in names)

    def test_get_catalog(self):
        """Test catalog.CatalogFactory.get_catalog() function."""

        cat = catalog.CatalogFactory.get_catalog("3C")
        self.assertTrue(isinstance(cat, catalog.C3C_Catalog))

        self.assertRaises(ValueError, catalog.CatalogFactory.get_catalog, "NONSENSE")

    def test_lookup(self):
        """Test catalog.Catalog.lookup() method."""

        cat = catalog.CatalogFactory.get_catalog("USER")

        source = cat.lookup("SgrA")
        self.assertEqual(source.name, "SgrA")
        self.assertTrue("W24" in source.alias_list)
        self.assertAlmostEqual(source.source.ra.hours, 17.753, 3)
        self.assertAlmostEqual(source.source.dec.degrees, -28.806, 3)

        source = cat.lookup("CasA")
        self.assertEqual(source.name, "CasA")
        self.assertTrue("NRAO711" in source.alias_list)
        self.assertAlmostEqual(source.source.ra.hours, 23.39, 2)
        self.assertAlmostEqual(source.source.dec.degrees, 58.815, 3)

        source = cat.lookup("CygA")
        self.assertEqual(source.name, "CygA")
        self.assertTrue("3C405" in source.alias_list)
        self.assertAlmostEqual(source.source.ra.hours, 19.991, 3)
        self.assertAlmostEqual(source.source.dec.degrees, 40.734, 3)

        source = cat.lookup("NONSENSE")
        self.assertTrue(source is None)

    def test_jpl_lookup(self):
        """Test catalog.Catalog.lookup() method."""

        cat = catalog.CatalogFactory.get_catalog("JPL")

        source = cat.lookup("Sun")
        self.assertEqual(source.name, "Sun")


class catalog_test_suite(unittest.TestSuite):
    """A unittest.TestSuite class which contains all of the lwa_user.catalog
    module unit tests."""

    def __init__(self):
        unittest.TestSuite.__init__(self)

        loader = unittest.TestLoader()
        self.addTests(loader.loadTestsFromTestCase(Test_catalog))


if __name__ == "__main__":
    unittest.main()
