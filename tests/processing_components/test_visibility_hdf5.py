"""Unit tests for visibility operations"""

import os
import sys
import unittest
import logging
import numpy
import shutil
from rascil2.data_models.parameters import rascil2_path, rascil2_data_path
from rascil2.data_models.polarisation import PolarisationFrame

from rascil2.processing_components.io import (
    create_visibility_from_hdf5,
    save_visibility_to_hdf5,
    convert_visibility_from_ms_to_hdf5,
    create_visibility_from_ms,
)

from rascil2.processing_components.visibility.operations import (
    integrate_visibility_by_channel,
)
from rascil2.processing_components import (
    invert_visibility,
    create_image_from_visibility,
)
from rascil2.processing_components.image.operations import (
    export_image_to_fits,
    qa_image,
)


log = logging.getLogger("rascil2-logger")

log.setLevel(logging.WARNING)
log.addHandler(logging.StreamHandler(sys.stdout))


def assert_vis(vis1, vis2):
    for datavar in vis1.keys():
        assert (vis1[datavar].data == vis2[datavar].data).all()


class TestCreateHDF5(unittest.TestCase):
    def setUp(self):
        self.results_dir = rascil2_path("test_results")

        self.persist = os.getenv("RASCIL_PERSIST", False)

        self.doplot = False

        self.msfile = rascil2_data_path("vis/ASKAP_example.ms")
        self.hdf5file = rascil2_data_path("vis/ASKAP_example.hdf5")
        self.irregularly_msfile = rascil2_data_path("vis/xcasa.ms")

        return

    def test_convert_visibility_from_ms_to_hdf5(self):
        test_hdf5_file = "%s/test_vis2hdf5.hdf5" % self.results_dir
        convert_visibility_from_ms_to_hdf5(
            msname=self.msfile, filename=test_hdf5_file, ifauto=True
        )
        hdf5_vis = create_visibility_from_hdf5(test_hdf5_file)
        ms_vis = create_visibility_from_ms(self.msfile)[0]

        assert_vis(hdf5_vis, ms_vis)

        try:
            os.remove(test_hdf5_file)
        except:
            pass

    def test_convert_visibility_from_ms_to_hdf5_irregularly_ms(self):
        import pandas

        def uvw_or_vis_equal(vis1, vis2, key):
            v2baseline = pandas.MultiIndex.from_tuples(vis2.baselines.data)
            for v1row, v1baseline in enumerate(vis1.baselines.data):
                a1, a2 = v1baseline
                try:
                    v2row = v2baseline.get_loc((a1, a2))
                except KeyError:
                    v2row = -1

                if v2row != -1:
                    assert numpy.allclose(
                        vis1[key].data[:, v1row], vis2[key].data[:, v2row]
                    )

        test_hdf5_file = "%s/test_vis2hdf5_irregularly.hdf5" % self.results_dir
        filled_tmp_table_prefix = "%s/test_filled_tmp_table" % self.results_dir
        convert_visibility_from_ms_to_hdf5(
            msname=self.irregularly_msfile,
            filename=test_hdf5_file,
            ifauto=False,
            ifregularize_table=True,
            filled_tmp_table_prefix=filled_tmp_table_prefix,
        )
        hdf5_vis_list = create_visibility_from_hdf5(test_hdf5_file)
        ms_vis_list = create_visibility_from_ms(self.irregularly_msfile)

        for vis1, vis2 in zip(ms_vis_list, hdf5_vis_list):
            uvw_or_vis_equal(vis1, vis2, "uvw")
            uvw_or_vis_equal(vis1, vis2, "vis")
            nants = vis1.configuration.configuration_acc.nants
            assert (
                vis1.visibility_acc.nbaselines - vis2.visibility_acc.nbaselines == nants
            )
            assert numpy.allclose(vis1.time.data, vis2.time.data)
            assert numpy.allclose(vis1.frequency.data, vis2.frequency.data)
            assert numpy.allclose(vis1.phasecentre.ra.rad, vis2.phasecentre.ra.rad)
            assert numpy.allclose(vis1.phasecentre.dec.rad, vis2.phasecentre.dec.rad)
        shutil.rmtree(filled_tmp_table_prefix, ignore_errors=True)

    def test_create_list_spectral(self):
        vis_by_channel = list()
        nchan_ave = 16
        nchan = 192
        for schan in range(0, nchan, nchan_ave):
            max_chan = min(nchan, schan + nchan_ave)
            v = create_visibility_from_hdf5(
                self.hdf5file, chanslice=slice(schan, max_chan)
            )
            vis_by_channel.append(v)

        assert len(vis_by_channel) == 12
        for v in vis_by_channel:
            assert v.vis.data.shape[-1] == 4
            assert v.visibility_acc.polarisation_frame.type == "linear"

    def test_create_list_spectral_average(self):
        vis_by_channel = list()
        nchan_ave = 16
        nchan = 192
        for schan in range(0, nchan, nchan_ave):
            max_chan = min(nchan, schan + nchan_ave)
            v = create_visibility_from_hdf5(
                self.hdf5file, chanslice=slice(schan, max_chan)
            )
            vis_by_channel.append(integrate_visibility_by_channel(v))

        assert len(vis_by_channel) == 12
        for v in vis_by_channel:
            assert v.vis.data.shape[-1] == 4
            assert v.vis.data.shape[-2] == 1
            assert v.visibility_acc.polarisation_frame.type == "linear"

    def test_invert(self):
        nchan_ave = 32
        nchan = 192
        for schan in range(0, nchan, nchan_ave):
            max_chan = min(nchan, schan + nchan_ave)
            vis = create_visibility_from_hdf5(
                self.hdf5file, chanslice=slice(schan, max_chan)
            )

            model = create_image_from_visibility(
                vis, npixel=256, polarisation_frame=PolarisationFrame("stokesI")
            )
            dirty, sumwt = invert_visibility(vis, model, context="2d")
            assert (numpy.max(numpy.abs(dirty["pixels"].data))) > 0.0
            assert dirty["pixels"].shape == (nchan_ave, 1, 256, 256)
            if self.doplot:
                import matplotlib.pyplot as plt
                from rascil2.processing_components.image.operations import show_image

                show_image(dirty)
                plt.show(block=False)
            if self.persist:
                export_image_to_fits(
                    dirty, "%s/test_visibility_uvfits_dirty.fits" % self.results_dir
                )

            if schan == 0:
                qa = qa_image(dirty)
                numpy.testing.assert_allclose(
                    qa.data["max"], 1.8952600368889259, atol=1e-7, err_msg=f"{qa}"
                )
                numpy.testing.assert_allclose(
                    qa.data["min"], -1.1593944910648, atol=1e-7, err_msg=f"{qa}"
                )

    def test_same_as_ms(self):
        msfile = rascil2_data_path("vis/ASKAP_example.ms")
        nchan_ave = 32
        nchan = 192
        for schan in range(0, nchan, nchan_ave):
            max_chan = min(nchan, schan + nchan_ave)
            vis = create_visibility_from_hdf5(
                self.hdf5file, chanslice=slice(schan, max_chan)
            )
            vis2 = create_visibility_from_ms(
                msfile, start_chan=schan, end_chan=max_chan - 1
            )[0]
            assert_vis(vis, vis2)

    def test_save_visibility_to_hdf5(self):
        vis = create_visibility_from_hdf5(self.hdf5file)
        hdf5_export = "%s/test_vishdf5_exported.hdf5" % self.results_dir
        save_visibility_to_hdf5(vis, hdf5_export)
        vis2 = create_visibility_from_hdf5(hdf5_export)
        assert_vis(vis, vis2)
        try:
            os.remove(hdf5_export)
        except:
            pass


if __name__ == "__main__":
    unittest.main()
