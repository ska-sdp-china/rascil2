"""Test functions to test the msgpack encode/decode functions."""

# pylint: disable=duplicate-code

import xarray
import numpy
import pytest
from astropy import units
from astropy.coordinates import EarthLocation, SkyCoord


from rascil2.processing_components.util import lla_to_ecef
from rascil2.processing_components.io import decode, encode
from rascil2.processing_components.visibility.base import create_visibility
from rascil2.data_models import Configuration


TIMES = (numpy.pi / 43200.0) * numpy.arange(0.0, 300.0, 30.0)
FREQUENCY = numpy.linspace(0.8e8, 1.2e8, 5)
CHANNEL_BANDWIDTH = numpy.array([1e7, 1e7, 1e7, 1e7, 1e7])


@pytest.fixture(scope="package", name="phase_centre")
def phase_centre_fixture():
    """
    PhaseCentre fixture
    """
    return SkyCoord(
        ra=+180.0 * units.deg,
        dec=-35.0 * units.deg,
        frame="icrs",
        equinox="J2000",
    )


@pytest.fixture(scope="package", name="low_aa05_config")
def config_fixture_low():
    """
    Configuration object fixture
    """
    location = EarthLocation(
        lon=116.69345390 * units.deg,
        lat=-26.86371635 * units.deg,
        height=300.0,
    )

    nants = 6
    aa05_low_coords = numpy.array(
        [
            [116.69345390, -26.86371635],
            [116.69365770, -26.86334071],
            [116.72963910, -26.85615287],
            [116.73007800, -26.85612864],
            [116.74788540, -26.88080530],
            [116.74733280, -26.88062234],
        ]
    )
    lon, lat = aa05_low_coords[:, 0], aa05_low_coords[:, 1]

    altitude = 300.0
    diameter = 38.0

    # pylint: disable=duplicate-code
    names = [
        "S008-1",
        "S008-2",
        "S009-1",
        "S009-2",
        "S010-1",
        "S010-2",
    ]
    mount = "XY"
    x_coord, y_coord, z_coord = lla_to_ecef(lat * units.deg, lon * units.deg, altitude)
    ant_xyz = numpy.stack((x_coord, y_coord, z_coord), axis=1)

    config = Configuration.constructor(
        name="LOW-AA0.5",
        location=location,
        names=names,
        mount=numpy.repeat(mount, nants),
        xyz=ant_xyz,
        vp_type=numpy.repeat("LOW", nants),
        diameter=diameter * numpy.ones(nants),
    )
    return config


def test_encode_decode(low_aa05_config, phase_centre):
    """Test both encode and decode, but only that they work."""
    vis = create_visibility(
        low_aa05_config,
        TIMES,
        FREQUENCY,
        channel_bandwidth=CHANNEL_BANDWIDTH,
        phasecentre=phase_centre,
    )

    encoded = encode(vis)
    assert isinstance(encoded, bytes)

    decoded = decode(encoded)
    assert isinstance(decoded, xarray.Dataset)
