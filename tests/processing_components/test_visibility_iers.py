import numpy
import pandas
from astropy.coordinates import SkyCoord, EarthLocation
from rascil2.data_models.polarisation import PolarisationFrame

import astropy.units as u
from rascil2.processing_components.visibility.visibility_iers import (
    create_visibility_iers,
    calculate_azel,
    calculate_hadec,
    calculate_ENU2UVW_matrix,
    calculate_ECEF2UVW_matrix,
    times_to_mjd,
    calculate_MATRIX_uvw,
    calculate_uvw,
    build_basic_object,
    calculate_ENU_AZEL_uvw,
    calculate_ECEF_HADEC_uvw,
)

from rascil2.processing_components.util.coordinate_support import (
    enu_to_ecef,
)

from rascil2.processing_components.util.catalog import CatalogFactory

from rascil2.processing_components.simulation import create_named_configuration
from rascil2.processing_components.visibility.base import generate_baselines

import datetime


# NOTE: The apparent RA is not a straight line,
#       so if the number is too large, the u,v will be inaccurate, but w is always accurate,
#       if the number is too small, the u,v,w will large difference with different uvw_model
#       the default value is 0.03 degrees according to the katpoint, there may not be a best value
OFFSETNUMBER = 3

RA_HOURS = 19
DEC_DEGREES = 50
LATITUDE_DEGREES = 42.211833333  # latitude in degrees; north positive.
LONGITUDE_DEGREES = 115.2505  # longitude in degrees; east positive.

ELEVATION_M = 1365  # elevation in meters
DEC_OFFSET = -OFFSETNUMBER
OFFSET_SIGN = -1.0

DATATIME_LIST = [
    datetime.datetime(2024, 6, 29, 12, 2, tzinfo=datetime.timezone.utc),
    datetime.datetime(2024, 6, 29, 12, 44, 51, 428571, tzinfo=datetime.timezone.utc),
    datetime.datetime(2024, 6, 29, 13, 27, 42, 857143, tzinfo=datetime.timezone.utc),
    datetime.datetime(2024, 6, 29, 14, 10, 34, 285714, tzinfo=datetime.timezone.utc),
    datetime.datetime(2024, 6, 29, 14, 53, 25, 714286, tzinfo=datetime.timezone.utc),
    datetime.datetime(2024, 6, 29, 15, 36, 17, 142857, tzinfo=datetime.timezone.utc),
    datetime.datetime(2024, 6, 29, 16, 19, 8, 571429, tzinfo=datetime.timezone.utc),
    datetime.datetime(2024, 6, 29, 17, 2, tzinfo=datetime.timezone.utc),
    datetime.datetime(2024, 6, 29, 17, 44, 51, 428571, tzinfo=datetime.timezone.utc),
    datetime.datetime(2024, 6, 29, 18, 27, 42, 857143, tzinfo=datetime.timezone.utc),
    datetime.datetime(2024, 6, 29, 19, 10, 34, 285714, tzinfo=datetime.timezone.utc),
    datetime.datetime(2024, 6, 29, 19, 53, 25, 714286, tzinfo=datetime.timezone.utc),
    datetime.datetime(2024, 6, 29, 20, 36, 17, 142857, tzinfo=datetime.timezone.utc),
    datetime.datetime(2024, 6, 29, 21, 19, 8, 571429, tzinfo=datetime.timezone.utc),
    datetime.datetime(2024, 6, 29, 22, 2, tzinfo=datetime.timezone.utc),
]


COMMON_OBJ = {
    pos_pkg: build_basic_object(
        pos_pkg,
        LATITUDE_DEGREES,
        LONGITUDE_DEGREES,
        ELEVATION_M,
        RA_HOURS,
        DEC_DEGREES,
        DEC_OFFSET,
        DATATIME_LIST,
    )
    for pos_pkg in ["native", "skyfield", "ephem", "astropy"]
}


ANT1_ENU = numpy.array([60000.0, 100.0, 1.0])
ANT2_ENU = numpy.array([6300.0, 60000.0, 0.0])

LOCATION = EarthLocation.from_geodetic(LONGITUDE_DEGREES, LATITUDE_DEGREES, ELEVATION_M)

ANT1_ECEF = enu_to_ecef(LOCATION, ANT1_ENU)
ANT2_ECEF = enu_to_ecef(LOCATION, ANT2_ENU)

CONFIGURATION = create_named_configuration("MID")


def test_build_basic_object():
    for pos_pkg in ["native", "skyfield", "ephem", "astropy"]:
        obser, star, star_offset, obs_time_list = COMMON_OBJ[pos_pkg]


def test_calculate_azel():
    pos_pkg_to_az_el_time_0 = {
        "native": (0.9576525706602466, 0.7318157430443712),
        "skyfield": (0.9560960254308888, 0.730260128399916),
        "ephem": (0.9560957551002502, 0.7302611470222473),
        "astropy": (0.9560960256421801, 0.7302601220589692),
    }
    for pos_pkg in ["native", "skyfield", "ephem", "astropy"]:
        obser, star, star_offset, obs_time_list = COMMON_OBJ[pos_pkg]
        azel = calculate_azel(obser, star, obs_time_list[0], pos_pkg)
        assert numpy.isclose(azel[0], pos_pkg_to_az_el_time_0[pos_pkg][0])
        assert numpy.isclose(azel[1], pos_pkg_to_az_el_time_0[pos_pkg][1])


def test_calculate_hadec():
    pos_pkg_to_ha_dec_time_0 = {
        "native": (-1.2424329664576939, 0.8726646259971648),
        "skyfield": (-1.245318253260232, 0.8732351516063277),
        "ephem": (-1.2453168482684496, 0.8732357049542602),
        "astropy": (-1.2453182624767563, 0.8732351493296706),
    }

    for pos_pkg in ["native", "skyfield", "ephem", "astropy"]:
        obser, star, star_offset, obs_time_list = COMMON_OBJ[pos_pkg]
        hadec = calculate_hadec(obser, star, obs_time_list[0], pos_pkg)
        assert numpy.isclose(hadec[0], pos_pkg_to_ha_dec_time_0[pos_pkg][0])
        assert numpy.isclose(hadec[1], pos_pkg_to_ha_dec_time_0[pos_pkg][1])


def test_calculate_ENU2UVW_matrix():
    pos_pkg = "skyfield"
    obser, star, star_offset, obs_time_list = COMMON_OBJ[pos_pkg]
    az, el = calculate_azel(obser, star, obs_time_list[0], pos_pkg)
    az_offset, el_offset = calculate_azel(obser, star_offset, obs_time_list[0], pos_pkg)

    enu2uvw = calculate_ENU2UVW_matrix(az, el, az_offset, el_offset, OFFSET_SIGN)
    true = numpy.array(
        [
            [0.31724226, 0.63881534, -0.70090821],
            [-0.72727714, 0.6382115, 0.25249562],
            [0.60862575, 0.42965224, 0.66706345],
        ]
    )
    assert numpy.allclose(enu2uvw, true)


def test_calculate_ECEF2UVW_matrix():
    pos_pkg = "skyfield"
    obser, star, star_offset, obs_time_list = COMMON_OBJ[pos_pkg]
    ha, dec = calculate_hadec(obser, star, obs_time_list[0], pos_pkg)
    ha_offset, dec_offset = calculate_hadec(
        obser, star_offset, obs_time_list[0], pos_pkg
    )

    ecef2uvw = calculate_ECEF2UVW_matrix(
        numpy.deg2rad(LONGITUDE_DEGREES) - ha,
        dec,
        numpy.deg2rad(LONGITUDE_DEGREES) - ha_offset,
        dec_offset,
        OFFSET_SIGN,
    )
    true = numpy.array(
        [
            [0.11761028, -0.99305733, 0.002227],
            [0.76092564, 0.09155885, 0.6423466],
            [-0.6380909, -0.07385198, 0.76641105],
        ]
    )
    assert numpy.allclose(ecef2uvw, true)


def test_skyfile_single_uvw():
    pos_pkg = "skyfield"
    obser, star, star_offset, obs_time_list = COMMON_OBJ[pos_pkg]

    for obs_time in obs_time_list:
        ha, dec = calculate_hadec(obser, star, obs_time, pos_pkg)
        ha_offset, dec_offset = calculate_hadec(obser, star_offset, obs_time, pos_pkg)
        ecef2uvw = calculate_ECEF2UVW_matrix(
            numpy.deg2rad(LONGITUDE_DEGREES) - ha,
            dec,
            numpy.deg2rad(LONGITUDE_DEGREES) - ha_offset,
            dec_offset,
            OFFSET_SIGN,
        )
        uvw_1 = ecef2uvw @ ANT2_ECEF.T - ecef2uvw @ ANT1_ECEF.T

        az, el = calculate_azel(obser, star, obs_time, pos_pkg)
        az_offset, el_offset = calculate_azel(obser, star_offset, obs_time, pos_pkg)
        enu2uvw = calculate_ENU2UVW_matrix(az, el, az_offset, el_offset, OFFSET_SIGN)
        uvw_2 = enu2uvw @ ANT2_ENU.T - enu2uvw @ ANT1_ENU.T

        error = uvw_1 - uvw_2
        error_max = numpy.max(numpy.abs(error))

        assert error_max < 1e-8


def test_skyfield_calculate_uvw_split():
    pos_pkg = "skyfield"
    config = CONFIGURATION
    ra_hours = 19
    dec_degrees = 50
    phasecentre = SkyCoord(ra=ra_hours * u.hour, dec=dec_degrees * u.deg, frame="icrs")
    times = DATATIME_LIST
    dec_offset = OFFSETNUMBER if phasecentre.dec.deg < 0.0 else -OFFSETNUMBER
    offset_sign = 1.0 if phasecentre.dec.deg < 0.0 else -1.0

    obser, star, star_offset, obs_time_list = build_basic_object(
        pos_pkg,
        config.location.geodetic[1].to("deg").value,
        config.location.geodetic[0].to("deg").value,
        config.location.geodetic[2].to("meter").value,
        phasecentre.ra.hour,
        phasecentre.dec.deg,
        dec_offset,
        times,
    )

    ants_xyz = config["xyz"].data
    nants = len(config["names"].data)
    baselines = pandas.MultiIndex.from_tuples(
        generate_baselines(nants), names=("antenna1", "antenna2")
    )

    for obs_time in obs_time_list:
        ant_uvw_1 = calculate_ENU_AZEL_uvw(
            obser,
            star,
            obs_time,
            pos_pkg,
            star_offset,
            ants_xyz,
            config,
            None,
            None,
            offset_sign,
        )

        ant_uvw_2 = calculate_ECEF_HADEC_uvw(
            obser, star, obs_time, pos_pkg, star_offset, ants_xyz, config, offset_sign
        )

        ant_uvw_3 = calculate_MATRIX_uvw(
            obser, star, obs_time, True, star_offset, ants_xyz, config, offset_sign
        )

        ant_uvw_4 = calculate_MATRIX_uvw(
            obser, star, obs_time, False, star_offset, ants_xyz, config, offset_sign
        )

        for _, (a1, a2) in enumerate(baselines):
            if a1 != a2:
                ruvw_1 = ant_uvw_1[a2, :] - ant_uvw_1[a1, :]
                ruvw_2 = ant_uvw_2[a2, :] - ant_uvw_2[a1, :]
                ruvw_3 = ant_uvw_3[a2, :] - ant_uvw_3[a1, :]
                ruvw_4 = ant_uvw_4[a2, :] - ant_uvw_4[a1, :]
                error_max1 = numpy.max(numpy.abs(ruvw_1 - ruvw_2))
                error_max2 = numpy.max(numpy.abs(ruvw_1 - ruvw_3))
                error_max3 = numpy.max(numpy.abs(ruvw_2 - ruvw_3))
                error_max4 = numpy.max(numpy.abs(ruvw_1 - ruvw_4))

                assert error_max1 < 1e-8
                assert error_max2 < 1e-8
                assert error_max3 < 1e-8
                assert error_max4 < 20


def test_calculate_uvw_same_pos_pkg():
    for pos_pkg in ["native", "skyfield", "ephem", "astropy"]:
        config = CONFIGURATION
        ra_hours = 19
        dec_degrees = 50
        phasecentre = SkyCoord(
            ra=ra_hours * u.hour, dec=dec_degrees * u.deg, frame="icrs"
        )
        times = DATATIME_LIST
        dec_offset = OFFSETNUMBER if phasecentre.dec.deg < 0.0 else -OFFSETNUMBER
        offset_sign = 1.0 if phasecentre.dec.deg < 0.0 else -1.0

        obser, star, star_offset, obs_time_list = build_basic_object(
            pos_pkg,
            config.location.geodetic[1].to("deg").value,
            config.location.geodetic[0].to("deg").value,
            config.location.geodetic[2].to("meter").value,
            phasecentre.ra.hour,
            phasecentre.dec.deg,
            dec_offset,
            times,
        )

        ants_xyz = config["xyz"].data
        nants = len(config["names"].data)
        baselines = pandas.MultiIndex.from_tuples(
            generate_baselines(nants), names=("antenna1", "antenna2")
        )

        for obs_time in obs_time_list:
            ants_uvw_enu_azel = calculate_uvw(
                obser,
                star,
                obs_time,
                star_offset,
                ants_xyz,
                config,
                "ENU+AZEL",
                pos_pkg,
                None,
                None,
                offset_sign,
            )
            ants_uvw = calculate_uvw(
                obser,
                star,
                obs_time,
                star_offset,
                ants_xyz,
                config,
                "ECEF+HADEC",
                pos_pkg,
                None,
                None,
                offset_sign,
            )

            # TODO: in ephem HADEC and AZEL is not the same
            if pos_pkg == "ephem":
                tol = 0.5
            else:
                tol = 1e-8
            _check_uvw(ants_uvw_enu_azel, ants_uvw, baselines, tol)

            if pos_pkg == "skyfield":
                ants_uvw = calculate_uvw(
                    obser,
                    star,
                    obs_time,
                    star_offset,
                    ants_xyz,
                    config,
                    "MATRIX-APPPOS",
                    pos_pkg,
                    None,
                    None,
                    offset_sign,
                )
                _check_uvw(ants_uvw_enu_azel, ants_uvw, baselines)

                ants_uvw = calculate_uvw(
                    obser,
                    star,
                    obs_time,
                    star_offset,
                    ants_xyz,
                    config,
                    "MATRIX",
                    pos_pkg,
                    None,
                    None,
                    offset_sign,
                )
                # NOTE: MATRIX max error is 20
                _check_uvw(ants_uvw_enu_azel, ants_uvw, baselines, 20)


def _prepare_data(pos_pkg):
    config = CONFIGURATION
    ra_hours = 19
    dec_degrees = 50
    phasecentre = SkyCoord(ra=ra_hours * u.hour, dec=dec_degrees * u.deg, frame="icrs")
    times = DATATIME_LIST
    dec_offset = OFFSETNUMBER if phasecentre.dec.deg < 0.0 else -OFFSETNUMBER
    offset_sign = 1.0 if phasecentre.dec.deg < 0.0 else -1.0

    obser, star, star_offset, obs_time_list = build_basic_object(
        pos_pkg,
        config.location.geodetic[1].to("deg").value,
        config.location.geodetic[0].to("deg").value,
        config.location.geodetic[2].to("meter").value,
        phasecentre.ra.hour,
        phasecentre.dec.deg,
        dec_offset,
        times,
    )

    ants_xyz = config["xyz"].data
    nants = len(config["names"].data)
    baselines = pandas.MultiIndex.from_tuples(
        generate_baselines(nants), names=("antenna1", "antenna2")
    )

    return (
        obser,
        star,
        star_offset,
        obs_time_list,
        ants_xyz,
        config,
        baselines,
        offset_sign,
    )


def test_calculate_uvw_same_uvw_model():
    uvw_model = "ENU+AZEL"

    test_uvw_dict = {
        pos_pkg: [] for pos_pkg in ["native", "skyfield", "ephem", "astropy"]
    }

    for pos_pkg in ["native", "skyfield", "ephem", "astropy"]:
        (
            obser,
            star,
            star_offset,
            obs_time_list,
            ants_xyz,
            config,
            baselines,
            offset_sign,
        ) = _prepare_data(pos_pkg)

        for obs_time in obs_time_list:
            ants_uvw = calculate_uvw(
                obser,
                star,
                obs_time,
                star_offset,
                ants_xyz,
                config,
                uvw_model,
                pos_pkg,
                None,
                None,
                offset_sign,
            )
            for _, (a1, a2) in enumerate(baselines):
                if a1 != a2:
                    ruvw = ants_uvw[a2, :] - ants_uvw[a1, :]
                    test_uvw_dict[pos_pkg].append(ruvw)

    standard_ruvw = test_uvw_dict["skyfield"]

    for pos_pkg in ["native", "ephem", "astropy"]:
        # NOTE: error_max_tol of different pos_pkg
        if pos_pkg == "ephem":
            error_max_tol = 0.45
        if pos_pkg == "native":
            error_max_tol = 859.5
        if pos_pkg == "astropy":
            error_max_tol = 4.4e-4

        (
            obser,
            star,
            star_offset,
            obs_time_list,
            ants_xyz,
            config,
            baselines,
            offset_sign,
        ) = _prepare_data(pos_pkg)

        idx = 0
        for obs_time in obs_time_list:
            ants_uvw = calculate_uvw(
                obser,
                star,
                obs_time,
                star_offset,
                ants_xyz,
                config,
                uvw_model,
                pos_pkg,
                None,
                None,
                offset_sign,
            )
            for _, (a1, a2) in enumerate(baselines):
                if a1 != a2:
                    ruvw = ants_uvw[a2, :] - ants_uvw[a1, :]
                    error_max = numpy.max(numpy.abs(ruvw - standard_ruvw[idx]))
                    idx += 1
                    assert error_max < error_max_tol


def _check_uvw(ants_uvw_enu_azel, ants_uvw, baselines, tol=1e-8):
    for _, (a1, a2) in enumerate(baselines):
        if a1 != a2:
            ruvw_0 = ants_uvw_enu_azel[a2, :] - ants_uvw_enu_azel[a1, :]
            ruvw_2 = ants_uvw[a2, :] - ants_uvw[a1, :]
            error_max1 = numpy.max(numpy.abs(ruvw_0 - ruvw_2))

            assert error_max1 < tol
            # if error_max1 > tol:
            #     # print("Error")
            #     print(ruvw_0)
            #     print(ruvw_2)
            #     print(error_max1)
            #     break


def test_times_to_mjd():
    true = [
        5.22637932e09,
        5.22638189e09,
        5.22638446e09,
        5.22638703e09,
        5.22638961e09,
        5.22639218e09,
        5.22639475e09,
        5.22639732e09,
        5.22639989e09,
        5.22640246e09,
        5.22640503e09,
        5.22640761e09,
        5.22641018e09,
        5.22641275e09,
        5.22641532e09,
    ]
    for pos_pkg in ["native", "skyfield", "ephem", "astropy"]:
        obser, star, star_offset, obs_time_list = COMMON_OBJ[pos_pkg]
        mjd = times_to_mjd(obs_time_list, pos_pkg)
        assert numpy.allclose(mjd, true)


def test_create_visibility_iers():
    pos_pkg = "skyfield"
    config = CONFIGURATION
    ra_hours = 19
    dec_degrees = 50
    phasecentre = SkyCoord(ra=ra_hours * u.hour, dec=dec_degrees * u.deg, frame="icrs")
    times = DATATIME_LIST

    frequency = numpy.array([1.4e9])
    channel_bandwidth = numpy.array([1e6])

    vis_list = []

    for uvw_model in ["ENU+AZEL", "ECEF+HADEC", "MATRIX-APPPOS"]:
        vis = create_visibility_iers(
            config=config,
            times=times,
            frequency=frequency,
            phasecentre=phasecentre,
            weight=1.0,
            polarisation_frame=PolarisationFrame("stokesI"),
            integration_time=1.0,
            channel_bandwidth=channel_bandwidth,
            source="test",
            meta={"test": "test"},
            uvw_model=uvw_model,
            pos_pkg=pos_pkg,
            offset_value=OFFSETNUMBER,
        )
        vis_list.append(vis)

    standard_vis = vis_list[0]

    nants = len(config["names"].data)

    assert standard_vis.time.shape == (len(times),)
    assert standard_vis.frequency.shape == (len(frequency),)
    assert standard_vis.baselines.shape == (nants * (nants + 1) // 2,)

    for i in range(1, len(vis_list)):
        vis = vis_list[i]

        error_uvw = numpy.max(numpy.abs(vis.uvw.data - standard_vis.uvw.data))

        assert error_uvw < 1e-8


def test_offset_value():
    all_uvw = []

    for offset_value in [0.01, 0.03]:
        pos_pkg = "skyfield"
        config = CONFIGURATION
        ra_hours = 19
        dec_degrees = 50
        phasecentre = SkyCoord(
            ra=ra_hours * u.hour, dec=dec_degrees * u.deg, frame="icrs"
        )
        times = DATATIME_LIST
        dec_offset = offset_value if phasecentre.dec.deg < 0.0 else -offset_value
        offset_sign = 1.0 if phasecentre.dec.deg < 0.0 else -1.0

        obser, star, star_offset, obs_time_list = build_basic_object(
            pos_pkg,
            config.location.geodetic[1].to("deg").value,
            config.location.geodetic[0].to("deg").value,
            config.location.geodetic[2].to("meter").value,
            phasecentre.ra.hour,
            phasecentre.dec.deg,
            dec_offset,
            times,
        )

        ants_xyz = config["xyz"].data
        nants = len(config["names"].data)
        baselines = pandas.MultiIndex.from_tuples(
            generate_baselines(nants), names=("antenna1", "antenna2")
        )

        uvw_list = []
        for obs_time in obs_time_list:
            ants_uvw_enu_azel = calculate_uvw(
                obser,
                star,
                obs_time,
                star_offset,
                ants_xyz,
                config,
                "ECEF+HADEC",
                pos_pkg,
                None,
                None,
                offset_sign,
            )
            for _, (a1, a2) in enumerate(baselines):
                if a1 != a2:
                    ruvw = ants_uvw_enu_azel[a2, :] - ants_uvw_enu_azel[a1, :]
                    uvw_list.append(ruvw)

        all_uvw.append(uvw_list)

    all_uvw = numpy.array(all_uvw)
    error_max_u = numpy.max(numpy.abs(all_uvw[0][:, 0] - all_uvw[1][:, 0]))
    error_max_v = numpy.max(numpy.abs(all_uvw[0][:, 1] - all_uvw[1][:, 1]))
    error_max_w = numpy.max(numpy.abs(all_uvw[0][:, 2] - all_uvw[1][:, 2]))
    assert error_max_u < 0.003
    assert error_max_v < 0.003
    assert error_max_w < 1e-12


def test_build_basic_object_with_CatalogFactory():
    cat = CatalogFactory.get_catalog("USER")
    source = cat.lookup("SgrA").source

    obser, star, star_offset, obs_time_list = build_basic_object(
        "skyfield",
        LATITUDE_DEGREES,
        LONGITUDE_DEGREES,
        ELEVATION_M,
        None,
        None,
        3,
        DATATIME_LIST,
        source,
    )

    assert star.dec.degrees == source.dec.degrees
    assert star_offset.dec.degrees == source.dec.degrees + 3


def test_create_visibility_iers_with_CatalogFactory():
    cat = CatalogFactory.get_catalog("USER")
    source = cat.lookup("SgrA").source

    vis = create_visibility_iers(
        config=CONFIGURATION,
        times=DATATIME_LIST,
        frequency=numpy.array([1.4e9]),
        phasecentre=None,
        weight=1.0,
        polarisation_frame=PolarisationFrame("stokesI"),
        integration_time=1.0,
        channel_bandwidth=numpy.array([1e6]),
        source=source,
    )
