.. _rascil_structure:

.. toctree::
   :maxdepth: 2

Structure
*********

Those familiar with other calibration and imaging packages will find the following information useful in navigating the
RASCIL2. Not all functions are listed here but are contained in the API.

The long form of the name is given for all entries but all function names are unique so a given function can be
accessed using the very top level import::

   import rascil2.data_models
   import rascil2.processing_components
   import rascil2.workflows
   import rascil2.apps


.. toctree::
   :maxdepth: 2

   RASCIL_data
   RASCIL_functions
   RASCIL_workflows
   RASCIL_apps
   RASCIL_polarisation_handling
   RASCIL_Fourier_processing
   RASCIL_dask
   RASCIL_xarray
   RASCIL_wagg

