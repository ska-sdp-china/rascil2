.. _rascil_macos_install:

Installation of RASCIL2 on macos
===============================

RASCIL2 is well-suited to running under macos. Installation should be straightforward. Although the pip approach can
be used, we recommend use of Anaconda https://www.anaconda.com It is necessary to
install python-casacore in a separate step. The steps required are::

    conda create -n rascil_env python=3.9
    conda activate rascil_env
    conda install -c conda-forge python-casacore
    git clone https://gitlab.astrolab.cn/ska-sdp-china/rascil2.git
    cd rascil2
    make install_requirements

We have specified python 3.9 since this currently is the preferred and supported version.

Finally, put the following definitions in your .bashrc::

    export RASCIL2=/path/to/rascil2
    export PYTHONPATH=$RASCIL2:$PYTHONPATH


.. _feedback: mailto:realtimcornwell@gmail.com
