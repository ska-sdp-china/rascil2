.. _rascil_P3_install:

Installation of RASCIL2 on P3
============================

RASCIL2 is well-suited to running on P3. Installation should be straightforward. We strongly recommend the use of a python virtual environment. Follow the generic installation steps.

