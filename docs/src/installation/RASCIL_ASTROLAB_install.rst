.. _rascil_ASTROLAB_install:

Installation of RASCIL2 on ASTROLAB Cluster
==============================

RASCIL2 is well-suited to running on AstroLAB in Guangzhou Univeristy. Installation should be straightforward. We strongly recommend the use of a python virtual environment. Follow the generic installation steps.

We recommend that RASCIL2 be installed on one of the preferred storage systems e.g. /rds/user/hpccorn1/hpc-work/rascil2

Installation of Git LFS::

    pip install git-lfs
    git lfs install
    git-lfs pull



.. _feedback: mailto:cnwangfeng@gmail.com
