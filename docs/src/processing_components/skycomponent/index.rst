.. _rascil_processing_components_skycomponent:

.. py:currentmodule:: rascil2.processing_components.skycomponent

Sky components
**************


.. automodapi::    rascil2.processing_components.skycomponent.base
   :no-inheritance-diagram:

.. automodapi::    rascil2.processing_components.skycomponent.operations
   :no-inheritance-diagram:
