.. _rascil_processing_components_flagging:

.. py:currentmodule:: rascil2.processing_components.flagging

********
Flagging
********

.. automodapi::    rascil2.processing_components.flagging.base
   :no-inheritance-diagram:
