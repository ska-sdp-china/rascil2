.. _rascil_processing_components_arrays:

.. py:currentmodule:: rascil2.processing_components.arrays

.. toctree::
   :maxdepth: 3

Arrays
******

.. automodapi::    rascil2.processing_components.arrays.cleaners
   :no-inheritance-diagram:

