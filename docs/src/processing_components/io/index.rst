.. _rascil_processing_components_io:

.. py:currentmodule:: rascil2.processing_components.io

Input/Output
**********

.. toctree::
   :maxdepth: 3

.. automodapi::    rascil2.processing_components.io.base
   :no-inheritance-diagram:

.. automodapi::    rascil2.processing_components.io.operations
   :no-inheritance-diagram:

.. automodapi::    rascil2.processing_components.io.basedata
   :no-inheritance-diagram:

.. automodapi::    rascil2.processing_components.io.msv2
   :no-inheritance-diagram:

.. automodapi::    rascil2.processing_components.io.uvfits
   :no-inheritance-diagram:

.. automodapi::    rascil2.processing_components.io.hdf5vis
   :no-inheritance-diagram:

.. automodapi::    rascil2.processing_components.io.regular_ms
   :no-inheritance-diagram:

.. automodapi::    rascil2.processing_components.io.casatable
   :no-inheritance-diagram:

.. automodapi::    rascil2.processing_components.io.serialize
   :no-inheritance-diagram:



