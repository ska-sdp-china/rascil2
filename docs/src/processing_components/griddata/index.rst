.. _rascil_processing_components_griddata:

.. py:currentmodule:: rascil2.processing_components.griddata

*************
Gridding Data
*************

.. toctree::
   :maxdepth: 3

.. automodapi::    rascil2.processing_components.griddata.convolution_functions
   :no-inheritance-diagram:

.. automodapi::    rascil2.processing_components.griddata.gridding
   :no-inheritance-diagram:

.. automodapi::    rascil2.processing_components.griddata.kernels
   :no-inheritance-diagram:

.. automodapi::    rascil2.processing_components.griddata.operations
   :no-inheritance-diagram:

