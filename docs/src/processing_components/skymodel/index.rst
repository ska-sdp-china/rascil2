.. _rascil_processing_components_skymodel:

.. py:currentmodule:: rascil2.processing_components.skymodel

**********
Sky models
**********

.. toctree::
   :maxdepth: 3

.. automodapi::    rascil2.processing_components.skymodel.skymodel_imaging
   :no-inheritance-diagram:

.. automodapi::    rascil2.processing_components.skymodel.operations
   :no-inheritance-diagram:

