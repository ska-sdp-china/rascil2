.. _rascil_data_models_polarisation_lower:

.. py:currentmodule:: rascil2.data_models.polarisation

.. toctree::
   :maxdepth: 3

============
Polarisation
============

.. automodapi::    rascil2.data_models.polarisation
   :no-inheritance-diagram:

