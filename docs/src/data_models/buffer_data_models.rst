.. _rascil_data_models_buffer_data_models:

.. py:currentmodule:: rascil2.data_models.buffer_data_models

.. toctree::
   :maxdepth: 3

==================
Buffer data models
==================

.. automodapi::    rascil2.data_models.buffer_data_models
   :no-inheritance-diagram:

