.. _rascil_data_models_parameters_lower:

.. py:currentmodule:: rascil2.data_models.parameters

.. toctree::
   :maxdepth: 3

==========
Parameters
==========

.. automodapi::    rascil2.data_models.parameters
   :no-inheritance-diagram:

