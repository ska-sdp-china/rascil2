.. _rascil_development:

******************
RASCIL2 development
******************

RASCIL2 is part of the SKA telescope organisation on GitLab https://gitlab.astrolab.cn/ska-sdp-china/rascil2.git and development
is ongoing. We welcome merge requests submitted via GitLab. Guidelines and instructions for contributing to code and
documentation can be found here.


.. toctree::
   :maxdepth: 2

   RASCIL_developing
   RASCIL_documenting
   RASCIL_release_process
   RASCIL_requirements_management
   RASCIL_background

* :ref:`genindex`
* :ref:`modindex`

.. _feedback: mailto:realtimcornwell@gmail.com
