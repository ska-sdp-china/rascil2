.. _rascil_api:

API
===

Here is a quick guide to the layout of the package:

 - rascil2.data_models.memory_data_models: Data models such as Image, Visibility, GainTable
 - rascil2.processing_components: Processing functions used in algorithms
 - rascil2.workflows: Distributed processing workflows
 - rascil2.apps: CLI apps
 - examples: Example scripts and notebooks
 - tests: Unit and regression tests
 - docs: Complete documentation. Includes non-interactive output of examples.
 - rascil2.data: Data used for simulations

The API is specified in the rascil2 directory.

.. toctree::
   :maxdepth: 1

   data_models/index.rst
   processing_components/index.rst
   workflows/index.rst
   apps/index.rst

* :ref:`genindex`
* :ref:`modindex`

.. _feedback: mailto:realtimcornwell@gmail.com
